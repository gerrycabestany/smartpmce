<?php

use Illuminate\Database\Seeder;
use App\Model\Unidades;
use App\Model\Empresas;

class UnidadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
        ->value('id');
 
         Unidades::create([
            'empresa_id'=>$empresa_id,
            'unidad'=>'Kg',
            'descripcion'=>'Medida de peso'
         ]);
 
         Unidades::create([
             'empresa_id'=>$empresa_id,
             'unidad'=>'Metros',
             'descripcion'=>'Medida de distancia'
          ]);
 
          Unidades::create([
             'empresa_id'=>$empresa_id,
             'unidad'=>'Toneladas',
             'descripcion'=>'Medida de fuerza'
          ]);
    }
}
