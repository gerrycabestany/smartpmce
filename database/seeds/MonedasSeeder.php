<?php

use Illuminate\Database\Seeder;
use App\Model\Monedas;

class MonedasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Monedas::create([
            'moneda'=>'MXN',
            'descripcion'=>'Peso Mexicano',
        ]);

        Monedas::create([
            'moneda'=>'USD',
            'descripcion'=>'Dolar Americano',
        ]);

        Monedas::create([
            'moneda'=>'CAD',
            'descripcion'=>'Dolar Canadiense',
        ]);
    }
}
