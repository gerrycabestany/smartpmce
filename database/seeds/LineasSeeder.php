<?php

use Illuminate\Database\Seeder;
use App\Model\Lineas;
use App\Model\Empresas;

class LineasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
       ->value('id');

        Lineas::create([
           'empresa_id'=>$empresa_id,
           'linea'=>'Construcción'
        ]);

        Lineas::create([
            'empresa_id'=>$empresa_id,
            'linea'=>'Albañilería'
         ]);

         Lineas::create([
            'empresa_id'=>$empresa_id,
            'linea'=>'Química'
         ]);
    }
}
