<?php

use Illuminate\Database\Seeder;
use App\Model\Clientes;
use App\Model\Empresas;
use App\Model\Industrias;
use App\Model\Creditos;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
        ->value('id');

        $industria_id = Industrias::where(['industria' => 'Motriz'])
        ->value('id');

        $credito_id = Creditos::where(['tiempo' => '15 dias'])
        ->value('id');
 
         Clientes::create([
            'empresa_id'=>$empresa_id,
            'cliente'=>'Ulisses',
            'industria_id'=>$industria_id,
            'logo'=>'Ninguno',
            'rfc'=>'ulisses@kan-ek.com',
            'razonSocial'=>'perfil92438png',
            'credito_id'=>$credito_id,
         ]);
    }
}
