<?php

use Illuminate\Database\Seeder;
use App\Model\Empresas;
use App\Model\Marcas;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
       ->value('id');

        Marcas::create([
           'empresa_id'=>$empresa_id,
           'marca'=>'Ulisses'
        ]);

        Marcas::create([
            'empresa_id'=>$empresa_id,
            'marca'=>'Genomax'
         ]);

         Marcas::create([
            'empresa_id'=>$empresa_id,
            'marca'=>'Ultra Importex'
         ]);
    }
}
