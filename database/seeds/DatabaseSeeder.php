<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(EmpresasSeeder::class);
        $this->call(AdminsSeeder::class);
        $this->call(MarcasSeeder::class);
        $this->call(LineasSeeder::class);
        $this->call(UnidadesSeeder::class);
        $this->call(IndustriasSeeder::class);
        $this->call(CreditosSeeder::class);
        $this->call(MonedasSeeder::class);
        $this->call(ClientesSeeder::class);
    }
}
