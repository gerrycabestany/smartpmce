<?php

use Illuminate\Database\Seeder;
use App\Model\Empresas;

class EmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Empresas::create([
            'razonSocial'=>'Innovaciones Tecnologicas Kumiai SAS de CV',
            'rfc'=>'ITK170202C56',
            'logo'=>'93405neqt.png',
            'persona'=>'Moral',
            'web'=>'innovakumiai.com'
        ]);
    }
}
