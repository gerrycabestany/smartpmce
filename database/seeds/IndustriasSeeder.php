<?php

use Illuminate\Database\Seeder;
use App\Model\Industrias;
use App\Model\Empresas;

class IndustriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
        ->value('id');
 
         Industrias::create([
            'empresa_id'=>$empresa_id,
            'industria'=>'Automotriz',
            'descripcion'=>'Especializacion en motores'
         ]);
 
         Industrias::create([
             'empresa_id'=>$empresa_id,
             'industria'=>'Motriz',
             'descripcion'=>'Partes ortopedicas'
          ]);
 
          Industrias::create([
             'empresa_id'=>$empresa_id,
             'industria'=>'Constructora',
             'descripcion'=>'Condicion de presas'
          ]);
    }
}
