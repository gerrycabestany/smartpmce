<?php

use Illuminate\Database\Seeder;
use App\Model\Empresas;
use App\Model\Admins;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
        ->value('id');
 
         Admins::create([
            'empresa_id'=>$empresa_id,
            'nombre'=>'Ulisses',
            'paterno'=>'Mercado',
            'materno'=>'Alvarez',
            'email'=>'ulisses@kan-ek.com',
            'foto'=>'perfil92438.png',
            'password'=>bcrypt('Rachma20'),
            'perfil'=>'admin',
            'fecha'=>'20 de Julio de 2019'
         ]);
    }
}
