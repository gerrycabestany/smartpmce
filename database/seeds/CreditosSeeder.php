<?php

use Illuminate\Database\Seeder;
use App\Model\Creditos;
use App\Model\Empresas;

class CreditosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
       ->value('id');

        Creditos::create([
           'empresa_id'=>$empresa_id,
           'tiempo'=>'15 días',
           'observaciones'=>'ninguna',
        ]);

        Creditos::create([
            'empresa_id'=>$empresa_id,
            'tiempo'=>'30 días',
            'observaciones'=>'ninguna'
         ]);

         Creditos::create([
            'empresa_id'=>$empresa_id,
            'tiempo'=>'45 días',
            'observaciones'=>'ninguna'
         ]);
    }
}
