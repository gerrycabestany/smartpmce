<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDireccionesClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direccionesClientes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->string('tipoDireccion');
            $table->string('calle');
            $table->string('ext');
            $table->string('int');
            $table->string('colonia');
            $table->string('municipio');
            $table->string('estado');
            $table->string('cp');
            $table->string('pais');
            $table->unsignedInteger('contacto_id')->nullable();
            $table->foreign('contacto_id')->references('id')->on('contactosClientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('direccionesClientes');
    }
}
