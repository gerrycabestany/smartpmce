<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->string('orden');
            $table->integer('version');
            $table->decimal('total', 10, 2);
            $table->string('cotizacion');
            $table->string('fechaProgramadaEntrega');
            $table->string('fechaLLegada');
            $table->text('condicionesPagos');
            $table->string('proveedor');
            $table->unsignedInteger('contactoProveedor_id')->nullable();
            $table->foreign('contactoProveedor_id')->references('id')->on('contactosProveedores');
            $table->unsignedInteger('direccionProveedor_id')->nullable();
            $table->foreign('direccionProveedor_id')->references('id')->on('direccionesProveedores');
            $table->unsignedInteger('emailProveedor_id')->nullable();
            $table->foreign('emailProveedor_id')->references('id')->on('emailsProveedores');
            $table->unsignedInteger('telefonoProveedor_id')->nullable();
            $table->foreign('telefonoProveedor_id')->references('id')->on('telefonosProveedores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordenes');
    }
}
