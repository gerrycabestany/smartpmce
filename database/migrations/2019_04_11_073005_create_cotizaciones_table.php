<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->unsignedInteger('cliente_id')->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes');
            $table->unsignedInteger('contactoCliente_id')->nullable();
            $table->foreign('contactoCliente_id')->references('id')->on('contactosClientes');
            $table->unsignedInteger('direccionCliente_id')->nullable();
            $table->foreign('direccionCliente_id')->references('id')->on('direccionesClientes');
            $table->unsignedInteger('emailCliente_id')->nullable();
            $table->foreign('emailCliente_id')->references('id')->on('emailsClientes');
            $table->unsignedInteger('telefonoCliente_id')->nullable();
            $table->foreign('telefonoCliente_id')->references('id')->on('telefonosClientes');
            $table->string('cotizacion');
            $table->integer('version');
            $table->string('fechaElaboracion');
            $table->unsignedInteger('elaboro_id')->nullable();
            $table->foreign('elaboro_id')->references('id')->on('admins');
            $table->string('fechaAutorizacion');
            $table->unsignedInteger('autorizo_id')->nullable();
            $table->foreign('autorizo_id')->references('id')->on('admins');
            $table->string('fechaCancelacion');
            $table->unsignedInteger('cancelo_id')->nullable();
            $table->foreign('cancelo_id')->references('id')->on('admins');
            $table->string('motivoCancelacion');
            $table->unsignedInteger('status_id')->nullable();
            $table->foreign('status_id')->references('id')->on('statusCotizaciones');
            $table->text('condicionesComerciales');
            $table->string('condicionesPagos');
            $table->text('comentarios');
            $table->string('totalMateriales');
            $table->string('totalServicios');
            $table->decimal('descuentoClienteFrecuente', 10, 2);
            $table->decimal('descuentoClienteEspecial', 10, 2);
            $table->decimal('descuentoEspecial', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cotizaciones');
    }
}
