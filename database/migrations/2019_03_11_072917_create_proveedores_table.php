<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('id');
                $table->unsignedInteger('empresa_id')->nullable();
                $table->foreign('empresa_id')->references('id')->on('empresas');
                $table->string('proveedor');
                $table->unsignedInteger('credito_id')->nullable();
                $table->foreign('credito_id')->references('id')->on('creditos');
                $table->string('logo');
                $table->string('rfc');
                $table->string('razonSocial');
                $table->string('clabe');
                $table->string('banco');
                $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proveedores');
    }
}
