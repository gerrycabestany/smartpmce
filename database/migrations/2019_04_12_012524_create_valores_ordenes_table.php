<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValoresOrdenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('valoresOrdenes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cotizacion');
            $table->string('orden');
            $table->string('version');
            $table->unsignedInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->integer('material');
            $table->integer('servicio');
            $table->string('codigo');
            $table->string('nombre');
            $table->unsignedInteger('marca_id')->nullable();
            $table->foreign('marca_id')->references('id')->on('marcas');
            $table->unsignedInteger('linea_id')->nullable();
            $table->foreign('linea_id')->references('id')->on('lineas');
            $table->decimal('costo', 10, 2);
            $table->integer('cantidad');
            $table->unsignedInteger('unidad_id')->nullable();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->string('proveedor');
            $table->unsignedInteger('moneda_id')->nullable();
            $table->foreign('moneda_id')->references('id')->on('monedas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('valoresOrdenes');
    }
}
