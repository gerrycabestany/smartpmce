<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('empresa_id')->nullable();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->string('cliente');
            $table->unsignedInteger('industria_id')->nullable();
            $table->foreign('industria_id')->references('id')->on('industrias');
            $table->string('logo');
            $table->string('rfc');
            $table->string('razonSocial');
            $table->unsignedInteger('credito_id')->nullable();
            $table->foreign('credito_id')->references('id')->on('creditos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
