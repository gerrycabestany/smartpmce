<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Proveedores;
use App\Model\ContactosProveedores;
use App\Model\DireccionesProveedores;
use App\Model\EmailsProveedores;
use App\Model\TelefonosProveedores;
use App\Model\Empresas;
use App\Model\Industrias;
use App\Model\Creditos;

class ProveedoresController extends Controller
{
    public function proveedores(){
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/proveedores', compact('proveedores'));
    }

    public function nuevoProveedor(){
        $creditos = Creditos::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/nuevoProveedor', ['creditos'=>$creditos]);
    }

    public function crearProveedor(Request $request){

        $data = request()->validate([
            'proveedor'=>['required', 'unique:proveedores'],
            'banco'=>['required'],
            'clabe'=>['required'],
            'logo'=>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'rfc'=>['required', 'unique:proveedores'],
            'razonSocial'=>['required'],
            'credito_id'=>['required'],
        ],[
            'proveedor.required'=>'Inserte un proveedor',
            'proveedor.unique'=>'El proveedor debe ser único',
            'logo.image'=>'El logo debe ser una imagen',
            'logo.mimes'=>'Selecciona una imagen jpeg, png, jpg o gif',
            'rfc.required'=>'Inserta un RFC',
            'razonSocial.required'=>'Inserta una razón social',
            'banco.required'=>'Inserte un proveedor',
            'clabe.required'=>'Inserte un proveedor',
        ]);

        if((request()->logo)!=null){
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('logos_proveedores'), $imageName);
        }else{
            $imageName = "avatar.jpg";
        }

    
        Proveedores::create([
            'proveedor'=>$data['proveedor'],
            'banco'=>$data['banco'],
            'clabe'=>$data['clabe'],
            'logo'=>$imageName,
            'rfc'=>$data['rfc'],
            'razonSocial'=>$data['razonSocial'],
            'credito_id'=>$data['credito_id'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('proveedores'); 
    }

    public function editarProveedor(Proveedores $proveedor){
        $creditos = Creditos::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/editarProveedor', ['proveedor'=> $proveedor, 'creditos'=>$creditos]);
    }

    public function actualizarProveedor(Proveedores $proveedor){
        $data = request()->validate([
            'proveedor'=>['required', 'unique:proveedores,proveedor,'.$proveedor->id],
            'banco'=>['required'],
            'clabe'=>['required'],
            'logo'=>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'rfc'=>['required', 'unique:proveedores'],
            'razonSocial'=>['required'],
            'credito_id'=>['required'],
        ],[
            'proveedor.required'=>'Inserte un proveedor',
            'proveedor.unique'=>'El proveedor debe ser único',
            'logo.image'=>'El logo debe ser una imagen',
            'logo.mimes'=>'Selecciona una imagen jpeg, png, jpg o gif',
            'rfc.required'=>'Inserta un RFC',
            'razonSocial.required'=>'Inserta una razón social',
            'banco.required'=>'Inserte un proveedor',
            'clabe.required'=>'Inserte un proveedor',
        ]);

        $proveedor->update($data);

        return redirect()->route('proveedores'); 
    }

    public function borrarProveedor(Proveedores $proveedor){

        $proveedor->delete();

        return redirect()->route('proveedores'); 
    }

    //Contactos
    public function contactosProveedores(){
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        $direccionesProveedores = DireccionesProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        $emailsProveedores = EmailsProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        $telefonosProveedores = TelefonosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/contactos', [
            'proveedores'=>$proveedores,
            'contactosProveedores'=>$contactosProveedores,
            'direccionesProveedores'=>$direccionesProveedores,
            'emailsProveedores'=>$emailsProveedores,
            'telefonosProveedores'=>$telefonosProveedores]);
    }

    public function nuevoContactoProveedor(){
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/nuevoContacto', ['proveedores'=>$proveedores]);
    }

    public function crearContactoProveedor(Request $request){

        $data = request()->validate([
            'proveedor_id'=>['required'],
            'nombre'=>['required'],
            'paterno'=>['required'],
            'materno'=>['required'],
            'puesto'=>['required'],
        ],[
            'proveedor_id.required'=>'Inserte un proveedor',
            'nombre.required'=>'Inserte un nombre',
            'paterno.required'=>'Inserte un apellido paterno',
            'materno.required'=>'Inserte un apellido materno',
            'puesto.required'=>'Inserte un puesto',
        ]);

        ContactosProveedores::create([
            'proveedor_id'=>$data['proveedor_id'],
            'nombre'=>$data['nombre'],
            'paterno'=>$data['paterno'],
            'materno'=>$data['materno'],
            'puesto'=>$data['puesto'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosProveedores'); 
    }

    public function editarContactoProveedor(ContactosProveedores $contactoProveedor){
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/editarContacto', ['proveedores'=> $proveedores, 'contactoProveedor'=>$contactoProveedor]);
    }

    public function borrarContactoProveedor(ContactosProveedores $contactoProveedor){

        $contactoProveedor->delete();

        return redirect()->route('contactosProveedores'); 
    }

    public function actualizarContactoProveedor(ContactosProveedores $contactoProveedor){
        $data = request()->validate([
            'proveedor_id'=>['required'],
            'nombre'=>['required'],
            'paterno'=>['required'],
            'materno'=>['required'],
            'puesto'=>['required'],
        ],[
            'proveedor_id.required'=>'Inserte un proveedor',
            'nombre.required'=>'Inserte un nombre',
            'paterno.required'=>'Inserte un apellido paterno',
            'materno.required'=>'Inserte un apellido materno',
            'puesto.required'=>'Inserte un puesto',
        ]);

        $contactoProveedor->update($data);

        return redirect()->route('contactosProveedores'); 
    }

    //Direcciones
    public function nuevaDireccionProveedor(){
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/nuevaDireccion', ['contactosProveedores'=>$contactosProveedores]);
    }

    public function crearDireccionProveedor(Request $request){

        $data = request()->validate([
            'int'=>['required'],
            'contacto_id'=>['required'],
            'tipoDireccion'=>['required'],
            'calle'=>['required'],
            'ext'=>['required'],
            'colonia'=>['required'],
            'municipio'=>['required'],
            'estado'=>['required'],
            'cp'=>['required'],
            'pais'=>['required'],
        ],[
            'int.required'=>'Inserte un numero interior, si no tiene coloca s/n',
            'contacto_id.required'=>'Inserte un proveedor',
            'tipoDireccion.required'=>'Inserte un tipo de direccion',
            'calle.required'=>'Inserte una calle',
            'ext.required'=>'Inserte un no. ext',
            'colonia.required'=>'Inserte una colonia',
            'municipio.required'=>'Inserte un municipio',
            'estado.required'=>'Inserte un estado',
            'cp.required'=>'Inserte un cp',
            'pais.required'=>'Inserte un pais',
        ]);

        DireccionesProveedores::create([
            'contacto_id'=>$data['contacto_id'],
            'tipoDireccion'=>$data['tipoDireccion'],
            'calle'=>$data['calle'],
            'ext'=>$data['ext'],
            'int'=>$data['int'],
            'colonia'=>$data['colonia'],
            'municipio'=>$data['municipio'],
            'estado'=>$data['estado'],
            'cp'=>$data['cp'],
            'pais'=>$data['pais'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosProveedores'); 
    }

    public function editarDireccionProveedor(DireccionesProveedores $direccionProveedor){
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/editarDireccion', ['contactosProveedores'=> $contactosProveedores, 'direccionProveedor'=>$direccionProveedor]);
    }

    public function borrarDireccionProveedor(DireccionesProveedores $direccionProveedor){

        $direccionProveedor->delete();

        return redirect()->route('contactosProveedores'); 
    }

    public function actualizarDireccionProveedor(DireccionesProveedores $direccionProveedor){
        $data = request()->validate([
            'int'=>['required'],
            'contacto_id'=>['required'],
            'tipoDireccion'=>['required'],
            'calle'=>['required'],
            'ext'=>['required'],
            'colonia'=>['required'],
            'municipio'=>['required'],
            'estado'=>['required'],
            'cp'=>['required'],
            'pais'=>['required'],
        ],[
            'int.required'=>'Inserte un numero interior, si no tiene coloca s/n',
            'contacto_id.required'=>'Inserte un proveedor',
            'tipoDireccion.required'=>'Inserte un tipo de direccion',
            'calle.required'=>'Inserte una calle',
            'ext.required'=>'Inserte un no. ext',
            'colonia.required'=>'Inserte una colonia',
            'municipio.required'=>'Inserte un municipio',
            'estado.required'=>'Inserte un estado',
            'cp.required'=>'Inserte un cp',
            'pais.required'=>'Inserte un pais',
        ]);

        $direccionProveedor->update($data);

        return redirect()->route('contactosProveedores'); 
    }

    //Emails
    public function nuevoEmailProveedor(){
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/nuevoEmail', ['contactosProveedores'=>$contactosProveedores]);
    }

    public function crearEmailProveedor(Request $request){

        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoEmail'=>['required'],
            'email'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un proveedor',
            'tipoDireccion.required'=>'Inserte un tipo de Email',
            'email.required'=>'Inserte un email',
        ]);

        EmailsProveedores::create([
            'contacto_id'=>$data['contacto_id'],
            'tipoEmail'=>$data['tipoEmail'],
            'email'=>$data['email'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosProveedores'); 
    }

    public function editarEmailProveedor(EmailsProveedores $emailProveedor){
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/editarEmail', ['contactosProveedores'=> $contactosProveedores, 'emailProveedor'=>$emailProveedor]);
    }

    public function borrarEmailProveedor(EmailsProveedores $emailProveedor){

        $emailProveedor->delete();

        return redirect()->route('contactosProveedores'); 
    }

    public function actualizarEmailProveedor(EmailsProveedores $emailProveedor){
        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoEmail'=>['required'],
            'email'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un proveedor',
            'tipoDireccion.required'=>'Inserte un tipo de Email',
            'email.required'=>'Inserte un email',
        ]);

        $emailProveedor->update($data);

        return redirect()->route('contactosProveedores'); 
    }

    //Telefonos
    public function nuevoTelefonoProveedor(){
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/nuevoTelefono', ['contactosProveedores'=>$contactosProveedores]);
    }

    public function crearTelefonoProveedor(Request $request){

        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoTelefono'=>['required'],
            'pais'=>['required', 'int'],
            'area'=>['required'],
            'telefono'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un proveedor',
            'tipoTelefono.required'=>'Inserte un tipo de Teléfono',
            'pais.required'=>'Inserte un pais',
            'pais.int'=>'Inserte el código de área de un pais',
            'area.required'=>'Inserte un area',
            'telefono.required'=>'Inserte un telefono',
        ]);

        TelefonosProveedores::create([
            'contacto_id'=>$data['contacto_id'],
            'tipoTelefono'=>$data['tipoTelefono'],
            'pais'=>$data['pais'],
            'area'=>$data['area'],
            'telefono'=>$data['telefono'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosProveedores'); 
    }

    public function editarTelefonoProveedor(TelefonosProveedores $telefonoProveedor){
        $contactosProveedores = ContactosProveedores::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/proveedores/editarTelefono', ['contactosProveedores'=> $contactosProveedores, 'telefonoProveedor'=>$telefonoProveedor]);
    }

    public function borrarTelefonoProveedor(TelefonosProveedores $telefonoProveedor){

        $telefonoProveedor->delete();

        return redirect()->route('contactosProveedores'); 
    }

    public function actualizarTelefonoProveedor(TelefonosProveedores $telefonoProveedor){
        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoTelefono'=>['required'],
            'pais'=>['required', 'int'],
            'area'=>['required'],
            'telefono'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un proveedor',
            'tipoTelefono.required'=>'Inserte un tipo de Teléfono',
            'pais.required'=>'Inserte un pais',
            'pais.int'=>'Inserte el código de área de un pais',
            'area.required'=>'Inserte un area',
            'telefono.required'=>'Inserte un telefono',
        ]);

        $telefonoProveedor->update($data);

        return redirect()->route('contactosProveedores'); 
    }

}
