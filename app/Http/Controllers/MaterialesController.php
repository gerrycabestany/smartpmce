<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Materiales;
use App\Model\Marcas;
use App\Model\Lineas;
use App\Model\Monedas;
use App\Model\Unidades;
use App\Model\Empresas;
use App\Model\Proveedores;

class MaterialesController extends Controller
{
    public function materiales(Request $request){

        if ($request->get('texto')){
            $texto = explode(" ", $request->get('texto'));

            $materiales= Materiales::query();

            foreach ($texto as $filtro){
                $materiales->orWhere('nombre', 'LIKE', '%'.$filtro.'%');
                $materiales->orWhere('codigo', 'LIKE', '%'.$filtro.'%');
            }

            $materiales = $materiales->distinct()->paginate(20);

        }else{
            $materiales = Materiales::orderBy('id', 'DESC')->paginate(20);
        }

        return view('crm/materiales/materiales', compact('materiales'));
    }

    public function nuevoMaterial(){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        
        //numero de proveedores
        $numeroProveedores=array();
        foreach($proveedores as $proveedor){
            array_push($numeroProveedores, $proveedor->id);
        }

        return view('crm/materiales/nuevoMaterial', ['marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function crearMaterial(Request $request){

        $data = request()->validate([
            'nombre'=>['required'],
            'codigo'=>['required'],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
        ],[
            'nombre.required'=>'Inserte un nombre del material',
            'codigo.required'=>'Inserta un codigo',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
        ]);

        Materiales::create([
            'empresa_id'=>session('empresa_id'),
            'codigo'=>$data['codigo'],
            'nombre'=> $data['nombre'],
            'marca_id'=>$data['marca_id'],
            'linea_id'=>$data['linea_id'],
            'costo'=> $data['costo'],
            'unidad_id'=>$data['unidad_id'],
            'proveedor'=>$data['proveedor'],
            'moneda_id'=> $data['moneda_id'],
        ]);

        return redirect()->route('materiales'); 
    }

    public function editarMaterial(Materiales $material){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();

        //numero de proveedores
        $numeroProveedores=array();
        $proveedorArray= explode(',', $material->proveedor);
        foreach($proveedorArray as $proveedor){
            array_push($numeroProveedores, (int)$proveedor);
        }

        return view('crm/materiales/editarMaterial', ['material'=>$material, 
        'marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function actualizarMaterial(Materiales $material){
        $data = request()->validate([
            'nombre'=>['required'],
            'codigo'=>['required'],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
        ],[
            'nombre.required'=>'Inserte un nombre del material',
            'codigo.required'=>'Inserta un codigo',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
        ]);

        $material->update($data);

        return redirect()->route('materiales'); 
    }

    public function borrarMaterial(Materiales $material){

        $material->delete();

        return redirect()->route('materiales'); 
    }
}
