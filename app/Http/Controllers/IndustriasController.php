<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Empresas;
use App\Model\Industrias;

class IndustriasController extends Controller
{
    public function industrias(){
        $industrias = industrias::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/industrias/industrias', compact('industrias'));
    }

    public function nuevaIndustria(){
        return view('crm/industrias/nuevaIndustria');
    }

    public function crearIndustria(Request $request){

        $data = request()->validate([
            'industria'=>['required', 'unique:industrias'],
            'descripcion'=>['required']
        ],[
            'industria.required'=>'Inserte una industria',
            'industria.unique'=>'Esta industria ya existe',
            'descripcion.required'=>'Inserta una descripción'
        ]);

        Industrias::create([
              'industria' => $data['industria'],
              'descripcion'=>$data['descripcion'],
              'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('industrias'); 
    }

    public function editarIndustria(Industrias $industria){
        return view('crm/industrias/editarIndustria', ['industria'=> $industria]);
    }

    public function actualizarIndustria(Industrias $industria){
        $data = request()->validate([
            'industria'=>['required', 'unique:industrias,industria,'.$industria->id],
            'descripcion'=>['required']
        ],[
            'industria.required'=>'Inserte una industria',
            'industria.unique'=>'Esta industria ya existe.',
            'descripcion.required'=>'Inserta una descripción'
        ]);


        $industria->update($data);

        return redirect()->route('industrias'); 
    }

    public function borrarIndustria(Industrias $industria){

        $industria->delete();

        return redirect()->route('industrias'); 
    }
}
