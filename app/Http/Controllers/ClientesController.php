<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Clientes;
use App\Model\ContactosClientes;
use App\Model\DireccionesClientes;
use App\Model\EmailsClientes;
use App\Model\TelefonosClientes;
use App\Model\Empresas;
use App\Model\Industrias;
use App\Model\Creditos;

class ClientesController extends Controller
{
    public function clientes(){
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/clientes', compact('clientes'));
    }

    public function nuevoCliente(){
        $creditos = Creditos::where(['empresa_id'=>session('empresa_id')])->get();
        $industrias = Industrias::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/nuevoCliente', ['industrias'=>$industrias, 'creditos'=>$creditos]);
    }

    public function crearCliente(Request $request){

        $data = request()->validate([
            'cliente'=>['required', 'unique:clientes'],
            'industria_id'=>['required'],
            'logo'=>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'rfc'=>['required', 'unique:clientes'],
            'razonSocial'=>['required'],
            'credito_id'=>['required'],
        ],[
            'cliente.required'=>'Inserte un cliente',
            'cliente.unique'=>'El cliente debe ser único',
            'industria_id.required'=>'Escoge una industria',
            'logo.image'=>'El logo debe ser una imagen',
            'logo.mimes'=>'Selecciona una imagen jpeg, png, jpg o gif',
            'rfc.required'=>'Inserta un RFC',
            'razonSocial.required'=>'Inserta una razón social',
        ]);

        if((request()->logo)!=null){
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('logos_clientes'), $imageName);
        }else{
            $imageName = "avatar.jpg";
        }
        

        Clientes::create([
            'cliente'=>$data['cliente'],
            'industria_id'=>$data['industria_id'],
            'logo'=>$imageName,
            'rfc'=>$data['rfc'],
            'razonSocial'=>$data['razonSocial'],
            'credito_id'=>$data['credito_id'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('clientes'); 
    }

    public function editarCliente(Clientes $cliente){
        $creditos = Creditos::where(['empresa_id'=>session('empresa_id')])->get();
        $industrias = Industrias::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/editarCliente', ['cliente'=> $cliente, 'creditos'=>$creditos, 'industrias'=>$industrias]);
    }

    public function actualizarCliente(Clientes $cliente){
        $data = request()->validate([
            'cliente'=>['required', 'unique:clientes,cliente,'.$cliente->id],
            'industria_id'=>['required'],
            'rfc'=>['required'],
            'razonSocial'=>['required'],
            'credito_id'=>['required'],
        ],[
            'cliente.required'=>'Inserte un cliente',
            'cliente.unique'=>'El cliente debe ser único',
            'industria_id.required'=>'Escoge una industria',
            'rfc.required'=>'Inserta un RFC',
            'razonSocial.required'=>'Inserta una razón social',
            'credito_id'=>'Escoge un crédito'
        ]);

        $cliente->update($data);

        return redirect()->route('clientes'); 
    }

    public function borrarCliente(Clientes $cliente){

        $cliente->delete();

        return redirect()->route('clientes'); 
    }

    //Contactos
    public function contactosClientes(){
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        $direccionesClientes = DireccionesClientes::where(['empresa_id'=>session('empresa_id')])->get();
        $emailsClientes = EmailsClientes::where(['empresa_id'=>session('empresa_id')])->get();
        $telefonosClientes = TelefonosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/contactos', [
            'clientes'=>$clientes,
            'contactosClientes'=>$contactosClientes,
            'direccionesClientes'=>$direccionesClientes,
            'emailsClientes'=>$emailsClientes,
            'telefonosClientes'=>$telefonosClientes]);
    }

    public function nuevoContactoCliente(){
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/nuevoContacto', ['clientes'=>$clientes]);
    }

    public function crearContactoCliente(Request $request){

        $data = request()->validate([
            'cliente_id'=>['required'],
            'nombre'=>['required'],
            'paterno'=>['required'],
            'materno'=>['required'],
            'puesto'=>['required'],
        ],[
            'cliente_id.required'=>'Inserte un cliente',
            'nombre.required'=>'Inserte un nombre',
            'paterno.required'=>'Inserte un apellido paterno',
            'materno.required'=>'Inserte un apellido materno',
            'puesto.required'=>'Inserte un puesto',
        ]);

        ContactosClientes::create([
            'cliente_id'=>$data['cliente_id'],
            'nombre'=>$data['nombre'],
            'paterno'=>$data['paterno'],
            'materno'=>$data['materno'],
            'puesto'=>$data['puesto'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosClientes'); 
    }

    public function editarContactoCliente(ContactosClientes $contactoCliente){
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/editarContacto', ['clientes'=> $clientes, 'contactoCliente'=>$contactoCliente]);
    }

    public function borrarContactoCliente(ContactosClientes $contactoCliente){

        $contactoCliente->delete();

        return redirect()->route('contactosClientes'); 
    }

    public function actualizarContactoCliente(ContactosClientes $contactoCliente){
        $data = request()->validate([
            'cliente_id'=>['required'],
            'nombre'=>['required'],
            'paterno'=>['required'],
            'materno'=>['required'],
            'puesto'=>['required'],
        ],[
            'cliente_id.required'=>'Inserte un cliente',
            'nombre.required'=>'Inserte un nombre',
            'paterno.required'=>'Inserte un apellido paterno',
            'materno.required'=>'Inserte un apellido materno',
            'puesto.required'=>'Inserte un puesto',
        ]);

        $contactoCliente->update($data);

        return redirect()->route('contactosClientes'); 
    }

    //Direcciones
    public function nuevaDireccionCliente(){
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/nuevaDireccion', ['contactosClientes'=>$contactosClientes]);
    }

    public function crearDireccionCliente(Request $request){

        $data = request()->validate([
            'int'=>['required'],
            'contacto_id'=>['required'],
            'tipoDireccion'=>['required'],
            'calle'=>['required'],
            'ext'=>['required'],
            'colonia'=>['required'],
            'municipio'=>['required'],
            'estado'=>['required'],
            'cp'=>['required'],
            'pais'=>['required'],
        ],[
            'int.required'=>'Inserte un numero interior, si no tiene coloca s/n',
            'contacto_id.required'=>'Inserte un cliente',
            'tipoDireccion.required'=>'Inserte un tipo de direccion',
            'calle.required'=>'Inserte una calle',
            'ext.required'=>'Inserte un no. ext',
            'colonia.required'=>'Inserte una colonia',
            'municipio.required'=>'Inserte un municipio',
            'estado.required'=>'Inserte un estado',
            'cp.required'=>'Inserte un cp',
            'pais.required'=>'Inserte un pais',
        ]);

        DireccionesClientes::create([
            'contacto_id'=>$data['contacto_id'],
            'tipoDireccion'=>$data['tipoDireccion'],
            'calle'=>$data['calle'],
            'ext'=>$data['ext'],
            'int'=>$data['int'],
            'colonia'=>$data['colonia'],
            'municipio'=>$data['municipio'],
            'estado'=>$data['estado'],
            'cp'=>$data['cp'],
            'pais'=>$data['pais'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosClientes'); 
    }

    public function editarDireccionCliente(DireccionesClientes $direccionCliente){
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/editarDireccion', ['contactosClientes'=> $contactosClientes, 'direccionCliente'=>$direccionCliente]);
    }

    public function borrarDireccionCliente(DireccionesClientes $direccionCliente){

        $direccionCliente->delete();

        return redirect()->route('contactosClientes'); 
    }

    public function actualizarDireccionCliente(DireccionesClientes $direccionCliente){
        $data = request()->validate([
            'int'=>['required'],
            'contacto_id'=>['required'],
            'tipoDireccion'=>['required'],
            'calle'=>['required'],
            'ext'=>['required'],
            'colonia'=>['required'],
            'municipio'=>['required'],
            'estado'=>['required'],
            'cp'=>['required'],
            'pais'=>['required'],
        ],[
            'int.required'=>'Inserte un numero interior, si no tiene coloca s/n',
            'contacto_id.required'=>'Inserte un cliente',
            'tipoDireccion.required'=>'Inserte un tipo de direccion',
            'calle.required'=>'Inserte una calle',
            'ext.required'=>'Inserte un no. ext',
            'colonia.required'=>'Inserte una colonia',
            'municipio.required'=>'Inserte un municipio',
            'estado.required'=>'Inserte un estado',
            'cp.required'=>'Inserte un cp',
            'pais.required'=>'Inserte un pais',
        ]);

        $direccionCliente->update($data);

        return redirect()->route('contactosClientes'); 
    }

    //Emails
    public function nuevoEmailCliente(){
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/nuevoEmail', ['contactosClientes'=>$contactosClientes]);
    }

    public function crearEmailCliente(Request $request){

        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoEmail'=>['required'],
            'email'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un cliente',
            'tipoDireccion.required'=>'Inserte un tipo de Email',
            'email.required'=>'Inserte un email',
        ]);

        EmailsClientes::create([
            'contacto_id'=>$data['contacto_id'],
            'tipoEmail'=>$data['tipoEmail'],
            'email'=>$data['email'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosClientes'); 
    }

    public function editarEmailCliente(EmailsClientes $emailCliente){
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/editarEmail', ['contactosClientes'=> $contactosClientes, 'emailCliente'=>$emailCliente]);
    }

    public function borrarEmailCliente(EmailsClientes $emailCliente){

        $emailCliente->delete();

        return redirect()->route('contactosClientes'); 
    }

    public function actualizarEmailCliente(EmailsClientes $emailCliente){
        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoEmail'=>['required'],
            'email'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un cliente',
            'tipoDireccion.required'=>'Inserte un tipo de Email',
            'email.required'=>'Inserte un email',
        ]);

        $emailCliente->update($data);

        return redirect()->route('contactosClientes'); 
    }

    //Telefonos
    public function nuevoTelefonoCliente(){
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/nuevoTelefono', ['contactosClientes'=>$contactosClientes]);
    }

    public function crearTelefonoCliente(Request $request){

        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoTelefono'=>['required'],
            'pais'=>['required', 'int'],
            'area'=>['required'],
            'telefono'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un cliente',
            'tipoTelefono.required'=>'Inserte un tipo de Teléfono',
            'pais.required'=>'Inserte un pais',
            'pais.int'=>'Inserte el código de área de un pais',
            'area.required'=>'Inserte un area',
            'telefono.required'=>'Inserte un telefono',
        ]);

        TelefonosClientes::create([
            'contacto_id'=>$data['contacto_id'],
            'tipoTelefono'=>$data['tipoTelefono'],
            'pais'=>$data['pais'],
            'area'=>$data['area'],
            'telefono'=>$data['telefono'],
            'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('contactosClientes'); 
    }

    public function editarTelefonoCliente(TelefonosClientes $telefonoCliente){
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/clientes/editarTelefono', ['contactosClientes'=> $contactosClientes, 'telefonoCliente'=>$telefonoCliente]);
    }

    public function borrarTelefonoCliente(TelefonosClientes $telefonoCliente){

        $telefonoCliente->delete();

        return redirect()->route('contactosClientes'); 
    }

    public function actualizarTelefonoCliente(TelefonosClientes $telefonoCliente){
        $data = request()->validate([
            'contacto_id'=>['required'],
            'tipoTelefono'=>['required'],
            'pais'=>['required', 'int'],
            'area'=>['required'],
            'telefono'=>['required'],
        ],[
            'contacto_id.required'=>'Inserte un cliente',
            'tipoTelefono.required'=>'Inserte un tipo de Teléfono',
            'pais.required'=>'Inserte un pais',
            'pais.int'=>'Inserte el código de área de un pais',
            'area.required'=>'Inserte un area',
            'telefono.required'=>'Inserte un telefono',
        ]);

        $telefonoCliente->update($data);

        return redirect()->route('contactosClientes'); 
    }

}
