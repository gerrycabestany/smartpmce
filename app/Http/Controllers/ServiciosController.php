<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Servicios;
use App\Model\Marcas;
use App\Model\Lineas;
use App\Model\Monedas;
use App\Model\Unidades;
use App\Model\Empresas;
use App\Model\Proveedores;


class ServiciosController extends Controller
{
    public function servicios(Request $request){

        if ($request->get('texto')){
            $texto = explode(" ", $request->get('texto'));

            $servicios= Servicios::query();

            foreach ($texto as $filtro){
                $servicios->orWhere('nombre', 'LIKE', '%'.$filtro.'%');
                $servicios->orWhere('codigo', 'LIKE', '%'.$filtro.'%');
            }

            $servicios = $servicios->distinct()->paginate(20);

        }else{
            $servicios = Servicios::orderBy('id', 'DESC')->paginate(20);
        }

        return view('crm/servicios/servicios', compact('servicios'));
    }

    public function nuevoServicio(){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        
        //numero de proveedores
        $numeroProveedores=array();
        foreach($proveedores as $proveedor){
            array_push($numeroProveedores, $proveedor->id);
        }

        return view('crm/servicios/nuevoServicio', ['marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function crearServicio(Request $request){

        $data = request()->validate([
            'nombre'=>['required'],
            'codigo'=>['required'],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
        ],[
            'nombre.required'=>'Inserte un nombre del servicio',
            'codigo.required'=>'Inserta un codigo',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
        ]);

        Servicios::create([
            'empresa_id'=>session('empresa_id'),
            'codigo'=>$data['codigo'],
            'nombre'=> $data['nombre'],
            'marca_id'=>$data['marca_id'],
            'linea_id'=>$data['linea_id'],
            'costo'=> $data['costo'],
            'unidad_id'=>$data['unidad_id'],
            'proveedor'=>$data['proveedor'],
            'moneda_id'=> $data['moneda_id'],
        ]);

        return redirect()->route('servicios'); 
    }

    public function editarServicio(Servicios $servicio){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        
        //numero de proveedores
        $numeroProveedores=array();
        $proveedorArray= explode(',', $servicio->proveedor);
        foreach($proveedorArray as $proveedor){
            array_push($numeroProveedores, (int)$proveedor);
        }

        return view('crm/servicios/editarServicio', ['servicio'=>$servicio, 
        'marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function actualizarServicio(Servicios $servicio){
        $data = request()->validate([
            'nombre'=>['required'],
            'codigo'=>['required'],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
        ],[
            'nombre.required'=>'Inserte un nombre del servicio',
            'codigo.required'=>'Inserta un codigo',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
        ]);

        $servicio->update($data);

        return redirect()->route('servicios'); 
    }

    public function borrarServicio(Servicios $servicio){

        $servicio->delete();

        return redirect()->route('servicios'); 
    }
}
