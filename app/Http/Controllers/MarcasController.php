<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Marcas;
use App\Model\Empresas;

class MarcasController extends Controller
{
    public function marcas(){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/marcas/marcas', compact('marcas'));
    }

    public function nuevaMarca(){
        return view('crm/marcas/nuevaMarca');
    }

    public function crearMarca(Request $request){

        $data = request()->validate([
            'marca'=>['required', 'unique:marcas'],
        ],[
            'marca.required'=>'Inserte una marca',
            'marca.unique'=>'Esta marca ya existe'
        ]);

        Marcas::create([
              'marca' => $data['marca'],
              'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('marcas'); 
    }

    public function editarMarca(Marcas $marca){
        return view('crm/marcas/editarMarca', ['marca'=> $marca]);
    }

    public function actualizarMarca(Marcas $marca){
        $data = request()->validate([
            'marca'=>['required', 'unique:marcas,marca,'.$marca->id],
        ],[
            'marca.required'=>'Inserte una marca',
            'marca.unique'=>'Esta marca ya existe'
        ]);

        $marca->update($data);

        return redirect()->route('marcas'); 
    }

    public function borrarMarca(Marcas $marca){

        $marca->delete();

        return redirect()->route('marcas'); 
    }
}
