<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Model\Admins;
use App\Model\Empresas;

class LoginController extends Controller
{
    public function login(){
        if (session('sesion')!=null){
            return redirect()->route('inicio');
        }else{
            return view('login');
        }
    }

    public function inicio(){
        return view('crm/inicio');
    }

    public function authLogin(){
        $data = request()->validate([
            'email'=>['required', 'email'],
            'password'=> ['required']
        ],[
            'email.required'=>'Ingresa un email',
            'email.email'=>'El dato introducido no es un email',
            'password.required'=>'Ingresa tu contraseña'
        ]);

        $admin = Admins::where('email', $data['email'])->first();

        if($admin){
            if (Hash::check($data['password'], $admin->password)) {
                session(['id'=>$admin->id, 
                        'empresa_id'=>$admin->empresa_id, 
                        'nombre'=>$admin->nombre, 
                        'paterno'=>$admin->paterno, 
                        'materno'=>$admin->materno, 
                        'email'=>$admin->email, 
                        'foto'=>$admin->foto, 
                        'perfil'=>$admin->perfil,
                        'razonSocial'=>$admin->empresas->razonSocial,
                        'rfc'=>$admin->empresas->rfc,
                        'logo'=>$admin->empresas->logo,
                        'persona'=>$admin->empresas->persona,
                        'web'=>$admin->empresas->web,
                        'sesion'=>"Open"]);
                return redirect()->route('inicio');
            }else{
                return redirect()->route('loginCrm')->withErrors('La contraseña es incorrecta');
            }
        }else {
            return redirect()->route('loginCrm')->withErrors('El email no existe en la db');
        }
    }

    public function logout(){
        session()->flush();
        return redirect()->route('loginCrm');

    }
}
