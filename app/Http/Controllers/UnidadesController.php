<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Empresas;
use App\Model\Unidades;

class UnidadesController extends Controller
{
    public function unidades(){
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/unidades/unidades', compact('unidades'));
    }

    public function nuevaUnidad(){
        return view('crm/unidades/nuevaUnidad');
    }

    public function crearUnidad(Request $request){

        $data = request()->validate([
            'unidad'=>['required', 'unique:unidades'],
            'descripcion'=>['required']
        ],[
            'unidad.required'=>'Inserte una unidad',
            'unidad.unique'=>'Esta unidad ya existe',
            'descripcion.required'=>'Inserta una descripción'
        ]);

        Unidades::create([
              'unidad' => $data['unidad'],
              'descripcion'=>$data['descripcion'],
              'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('unidades'); 
    }

    public function editarUnidad(Unidades $unidad){
        return view('crm/unidades/editarUnidad', ['unidad'=> $unidad]);
    }

    public function actualizarUnidad(Unidades $unidad){
        $data = request()->validate([
            'unidad'=>['required', 'unique:unidades,unidad,'.$unidad->id],
            'descripcion'=>['required']
        ],[
            'unidad.required'=>'Inserte una unidad',
            'descripcion.required'=>'Inserta una descripción'
        ]);

        $unidad->update($data);

        return redirect()->route('unidades'); 
    }

    public function borrarUnidad(Unidades $unidad){

        $unidad->delete();

        return redirect()->route('unidades'); 
    }
}
