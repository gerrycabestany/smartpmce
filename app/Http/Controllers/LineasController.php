<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Lineas;
use App\Model\Empresas;

class LineasController extends Controller
{
    public function lineas(){
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/lineas/lineas', compact('lineas'));
    }

    public function nuevaLinea(){
        return view('crm/lineas/nuevaLinea');
    }

    public function crearLinea(Request $request){

        $data = request()->validate([
            'linea'=>['required', 'unique:lineas'],
        ],[
            'lineas.required'=>'Inserte una línea',
            'linea.unique'=>'Esta línea ya existe',
        ]);

        Lineas::create([
              'linea' => $data['linea'],
              'empresa_id'=> session('empresa_id'),
        ]);

        return redirect()->route('lineas'); 
    }

    public function editarLinea(Lineas $linea){
        return view('crm/lineas/editarLinea', ['linea'=> $linea]);
    }

    public function actualizarLinea(Lineas $linea){
        $data = request()->validate([
            'linea'=>['required', 'unique:lineas,linea,'.$linea->id],
        ],[
            'linea.required'=>'Inserte una línea',
            'linea.unique'=>'Esta línea ya existe',
        ]);

        $linea->update($data);

        return redirect()->route('lineas'); 
    }

    public function borrarLinea(Lineas $linea){

        $linea->delete();

        return redirect()->route('lineas'); 
    }
}
