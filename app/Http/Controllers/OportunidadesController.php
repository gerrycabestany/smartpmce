<?php

namespace App\Http\Controllers;

use App\Model\Oportunidades;
use App\Model\Clientes;
use App\Model\Monedas;
use App\Model\Empresas;
use Illuminate\Http\Request;
use Mail;

class OportunidadesController extends Controller
{
    public function oportunidades(){
        $oportunidades = Oportunidades::where(['empresa_id'=>session('empresa_id')])->get();
        return view('crm/oportunidades/oportunidades', compact('oportunidades'));
    }

    public function nuevaOportunidad(){
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        return view('crm/oportunidades/nuevaOportunidad', ['clientes'=>$clientes, 'monedas'=>$monedas]);
    }

    public function crearOportunidad(Request $request){

        $data = request()->validate([
            'oportunidad'=>['required'],
            'fecha'=>['required'],
            'fechaCierre'=>['required'],
            'valor'=>['required'],
            'moneda_id'=>[''],
            'cliente_id'=>[''],
            'emails'=>['required'],
        ],[
            'oportunidad.required'=>'Inserte una oportunidad',
            'fecha.required'=>'Inserta una fecha',
            'fechaCierre.required'=>'Inserta una fecha de cierre',
            'valor.required'=>'Inserta el valor',
            'emails.required'=>'Inserta el valor',
        ]);

        $empresa_id = Empresas::where(['rfc' => 'ITK170202C56'])
       ->value('id');

        Oportunidades::create([
              'moneda_id'=>$data['moneda_id'],
              'cliente_id'=>$data['cliente_id'],
              'oportunidad' => $data['oportunidad'],
              'empresa_id'=> $empresa_id,
              'fecha'=>$data['fecha'],
              'fechaCierre'=>$data['fechaCierre'],
              'valor'=>$data['valor'],
              'emails'=>$data['emails'],
              'empresa_id'=> session('empresa_id'),
        ]);

        $cliente=Clientes::where(['empresa_id'=>session('empresa_id'), 'id'=>$data['cliente_id']])->get();
        $moneda=Monedas::where(['id'=>$data['moneda_id']])->get();

        $data['cliente']=$cliente->last()->cliente;
        $data['moneda']=$moneda->last()->moneda;

        $correos=array();
        $correosArray= explode(',', str_replace(" ", "", $data['emails']));
        foreach($correosArray as $correo){
            if($correo!=""){
                array_push($correos, $correo);
            }
        }

        Mail::send('crm.email.oportunidad', $data, function ($message) use($correos) {
            $message->from('no-reply@smartpmce.com', 'SmartPmce');
            $message->to($correos);
            $message->subject('Oportunidad ');
        });
        
        return redirect()->route('oportunidades'); 
    }

    public function editarOportunidad(Oportunidades $oportunidad){
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        return view('crm/oportunidades/editarOportunidad', ['oportunidad'=> $oportunidad, 'clientes'=>$clientes, 'monedas'=>$monedas]);
    }

    public function actualizarOportunidad(Oportunidades $oportunidad){
        $data = request()->validate([
            'oportunidad'=>['required'],
            'fecha'=>['required'],
            'fechaCierre'=>['required'],
            'valor'=>['required'],
            'moneda_id'=>[''],
            'cliente_id'=>[''],
            'emails'=>['required'],
        ],[
            'oportunidad.required'=>'Inserte una oportunidad',
            'fecha.required'=>'Inserta una fecha',
            'fechaCierre.required'=>'Inserta una fecha de cierre',
            'valor.required'=>'Inserta el valor',
            'emails.required'=>'Inserta el valor',
        ]);

        $oportunidad->update($data);

        return redirect()->route('oportunidades'); 
    }

    public function borrarOportunidad(Oportunidades $oportunidad){

        $oportunidad->delete();

        return redirect()->route('oportunidades'); 
    }
}
