<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Ordenes;
use App\Model\Cotizaciones;
use App\Model\Proveedores;
use App\Http\Controllers\Controller;
use App\Model\ValoresCotizaciones;
use App\Model\ValoresOrdenes;
use Mail;
use App;
use PDF;

class OrdenesController extends Controller
{
    public function ordenes(){
        //Regresar cotizaciones en Venta
        $cotizacionesVenta= Cotizaciones::where(['status_id' => 4, 'empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        
        for($i=0; $i<count($cotizacionesVenta); $i++){
            //consultar valores de la cotizacion
            $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacionesVenta[$i]->cotizacion])->get();
            
            //Calcular el total de materiales
            $tieneMat=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if(($valorCotizacion->material)==1){
                    $tieneMat += $valorCotizacion->restanteOrdenes;
                }
            }
            $tieneServ=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if(($valorCotizacion->servicio)==1){
                    $tieneServ += $valorCotizacion->restanteOrdenes;
                }
            }

            $cotizacionesVenta[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesVenta[$i]->cotizacion);
            $cotizacionesVenta[$i]->tieneMat= $tieneMat;
            $cotizacionesVenta[$i]->tieneServ = $tieneServ;

        }

        $ordenes = Ordenes::where(['empresa_id'=>session('empresa_id')])->get();
        for($i=0; $i<count($ordenes); $i++){
            //Verifica si es de materiales y servicios
            $valoresOrdenes=ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$ordenes[$i]->cotizacion, 'orden'=>$ordenes[$i]->orden])->get();
            for($j=0; $j<count($valoresOrdenes); $j++){
                if($valoresOrdenes[$j]->material==1){
                    $ordenes[$i]->material=1; 
                    $ordenes[$i]->servicio=0; 
                }else{
                    $ordenes[$i]->servicio=1; 
                    $ordenes[$i]->material=0; 
                }
            }
            $ordenes[$i]->orden = OrdenesController::formatoP($ordenes[$i]->orden);
            $proveedor=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>(int)$ordenes[$i]->proveedor])->get();
            $ordenes[$i]->proveedor=$proveedor->last()->proveedor;
            
        }


        return view('crm/ordenes/ordenes', ['cotizacionesVenta'=>$cotizacionesVenta, 'ordenes'=>$ordenes]);
    }

    //Materiales
    public function ordenesMaterialesCompra(Cotizaciones $cotizacion){
        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion->cotizacion, 'version'=>$cotizacion->version])->get();
        $valoresOrdenes = ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion->cotizacion, 'orden'=>'proceso'])->get();

        for($i=0; $i<count($valoresCotizaciones); $i++){
            $proveedores=array();
            $proveedoresArray= explode(',', $valoresCotizaciones[$i]->proveedor);
            foreach($proveedoresArray as $proveedor){
                $proveedorQuery=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>$proveedor])->get();
                array_push($proveedores, array('id'=>($proveedorQuery->first()->id), 'proveedor'=>($proveedorQuery->first()->proveedor)));
            }
            $valoresCotizaciones[$i]->proveedor=$proveedores;
        }

        for($i=0; $i<count($valoresOrdenes); $i++){
            $proveedorQuery=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>$valoresOrdenes[$i]->proveedor])->get();
            $valoresOrdenes[$i]->proveedor=$proveedorQuery->first()->proveedor;
        }

        return view('crm/ordenes/ordenesMaterialesCompra', ['valoresCotizaciones'=>$valoresCotizaciones, 'valoresOrdenes'=>$valoresOrdenes]);
    }

    public function subirMaterialOrdenes(ValoresCotizaciones $valorCotizacion){
        $data=request();

        if($data['cantidad']==null){
            return back()->with('error','Ingresa una cantidad');
        }
        if($data['cantidad']>$valorCotizacion->restanteOrdenes){
            return back()->with('error','La cantidad no puede ser mayor que los restantes');
        }

        //Checar si un elemento ya existe en el proceso tanto con nombre, codigo, cotizacion, proveedor, material y servicio

        $checarValorOrdenes=ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 
        'cotizacion'=>$valorCotizacion->cotizacion,
        'proveedor'=>$data['proveedor'],
        'nombre'=>$valorCotizacion->nombre,
        'codigo'=>$valorCotizacion->codigo,
        'material'=>$valorCotizacion->material,
        'servicio'=>$valorCotizacion->servicio])->get();

        if(count($checarValorOrdenes)>0){
            $actualizarValoresOrdenes['cantidad']=($checarValorOrdenes->last()->cantidad)+$data['cantidad'];
            ValoresOrdenes::find($checarValorOrdenes)->first()->fill($actualizarValoresOrdenes)->save();
        }else{
            ValoresOrdenes::create([
                'cotizacion'=> $valorCotizacion->cotizacion,
                'orden'=> "proceso",
                'version'=> 0,
                'empresa_id'=> session('empresa_id'),
                'material'=>1,
                'servicio'=>0,
                'codigo'=>$valorCotizacion->codigo,
                'nombre'=>$valorCotizacion->nombre,
                'marca_id'=>$valorCotizacion->marca_id,
                'linea_id'=>$valorCotizacion->linea_id,
                'costo'=>$valorCotizacion->costo,
                'cantidad'=>$data['cantidad'],
                'unidad_id'=>$valorCotizacion->unidad_id,
                'proveedor'=>$data['proveedor'],
                'moneda_id'=>$valorCotizacion->moneda_id]);
        }

        $actualizarValoresCotizaciones['restanteOrdenes']=($valorCotizacion->restanteOrdenes)-$data['cantidad'];

        $valorCotizacion->update($actualizarValoresCotizaciones);

        return back()->with('success','Articulo agregado con exito');
    }

    //Materiales
    public function ordenesServiciosCompra(Cotizaciones $cotizacion){
        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion->cotizacion, 'version'=>$cotizacion->version])->get();
        $valoresOrdenes = ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion->cotizacion, 'orden'=>'proceso'])->get();

        for($i=0; $i<count($valoresCotizaciones); $i++){
            $proveedores=array();
            $proveedoresArray= explode(',', $valoresCotizaciones[$i]->proveedor);
            foreach($proveedoresArray as $proveedor){
                $proveedorQuery=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>$proveedor])->get();
                array_push($proveedores, array('id'=>($proveedorQuery->first()->id), 'proveedor'=>($proveedorQuery->first()->proveedor)));
            }
            $valoresCotizaciones[$i]->proveedor=$proveedores;
        }

        for($i=0; $i<count($valoresOrdenes); $i++){
            $proveedorQuery=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>$valoresOrdenes[$i]->proveedor])->get();
            $valoresOrdenes[$i]->proveedor=$proveedorQuery->first()->proveedor;
        }

        return view('crm/ordenes/ordenesServiciosCompra', ['valoresCotizaciones'=>$valoresCotizaciones, 'valoresOrdenes'=>$valoresOrdenes]);
    }

    public function subirServicioOrdenes(ValoresCotizaciones $valorCotizacion){
        $data=request();

        if($data['cantidad']==null){
            return back()->with('error','Ingresa una cantidad');
        }
        if($data['cantidad']>$valorCotizacion->restanteOrdenes){
            return back()->with('error','La cantidad no puede ser mayor que los restantes');
        }

        //Checar si un elemento ya existe en el proceso tanto con nombre, codigo, cotizacion, proveedor, material y servicio

        $checarValorOrdenes=ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 
        'cotizacion'=>$valorCotizacion->cotizacion,
        'proveedor'=>$data['proveedor'],
        'nombre'=>$valorCotizacion->nombre,
        'codigo'=>$valorCotizacion->codigo,
        'material'=>$valorCotizacion->material,
        'servicio'=>$valorCotizacion->servicio])->get();

        if(count($checarValorOrdenes)>0){
            $actualizarValoresOrdenes['cantidad']=($checarValorOrdenes->last()->cantidad)+$data['cantidad'];
            ValoresOrdenes::find($checarValorOrdenes)->first()->fill($actualizarValoresOrdenes)->save();
        }else{
            ValoresOrdenes::create([
                'cotizacion'=> $valorCotizacion->cotizacion,
                'orden'=> "proceso",
                'version'=> 0,
                'empresa_id'=> session('empresa_id'),
                'material'=>0,
                'servicio'=>1,
                'codigo'=>$valorCotizacion->codigo,
                'nombre'=>$valorCotizacion->nombre,
                'marca_id'=>$valorCotizacion->marca_id,
                'linea_id'=>$valorCotizacion->linea_id,
                'costo'=>$valorCotizacion->costo,
                'cantidad'=>$data['cantidad'],
                'unidad_id'=>$valorCotizacion->unidad_id,
                'proveedor'=>$data['proveedor'],
                'moneda_id'=>$valorCotizacion->moneda_id]);
        }

        $actualizarValoresCotizaciones['restanteOrdenes']=($valorCotizacion->restanteOrdenes)-$data['cantidad'];

        $valorCotizacion->update($actualizarValoresCotizaciones);

        return back()->with('success','Articulo agregado con exito');
    }

    public function generarMaterialesOrdenes($cotizacion){
        $valoresOrdenes = ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion])->get();
        
        $proveedores=array();
        foreach($valoresOrdenes as $valorOrden){
            if(!in_array($valorOrden->proveedor, $proveedores)){
                array_push($proveedores, $valorOrden->proveedor);
            }  
        }

        foreach($proveedores as $proveedor){
            $valoresOrdenes = ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion, 'proveedor'=>$proveedor])->get();
            //aqui va otro foreach para valores ordenes y que genera las PO

            //Calcular el total de materiales
            $total=0;
            foreach ($valoresOrdenes as $valorOrden){
                if (($valorOrden->material)==1){
                    $total += ($valorOrden->costo)*($valorOrden->cantidad);
                }
            }

            //En caso de que no exista ninguna PO
            $ordenes=Ordenes::where(['empresa_id'=>session('empresa_id')])->get();
            
            $orden=0;
            if (($ordenes->last())==null){
                $orden=1;
                Ordenes::create([
                    'empresa_id'=> session('empresa_id'),
                    'orden'=>$orden, 
                    'version'=>0, 
                    'total'=>$total, 
                    'cotizacion'=>$cotizacion, 
                    'fechaProgramadaEntrega'=>"ninguna", 
                    'fechaLLegada'=>"ninguna", 
                    'proveedor'=>$proveedor,
                    'direccionProveedor_id'=>null, 
                    'emailProveedor_id'=>null, 
                    'telefonoProveedor_id'=>null, 
                    'condicionesPagos'=>'ninguno',]);
            }else{ 
                $orden = ($ordenes->last()->orden)+1;
                Ordenes::create([
                    'empresa_id'=> session('empresa_id'),
                    'orden'=>$orden, 
                    'version'=>0, 
                    'total'=>$total, 
                    'cotizacion'=>$cotizacion, 
                    'fechaProgramadaEntrega'=>"ninguna", 
                    'fechaLLegada'=>"ninguna", 
                    'proveedor'=>$proveedor,
                    'direccionProveedor_id'=>null, 
                    'emailProveedor_id'=>null, 
                    'telefonoProveedor_id'=>null, 
                    'condicionesPagos'=>"ninguna",]);
            }

            //foreach valores ordenes actualizar orden
            $actualizarValorOrden['orden']=$orden;
            foreach($valoresOrdenes as $valorOrden){
                ValoresOrdenes::find($valorOrden->id)->fill($actualizarValorOrden)->save();
            }
        }

        return redirect()->route('ordenes');

    }

    public function generarServiciosOrdenes($cotizacion){
        $valoresOrdenes = ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion])->get();
        
        $proveedores=array();
        foreach($valoresOrdenes as $valorOrden){
            if(!in_array($valorOrden->proveedor, $proveedores)){
                array_push($proveedores, $valorOrden->proveedor);
            }  
        }

        foreach($proveedores as $proveedor){
            $valoresOrdenes = ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion, 'proveedor'=>$proveedor])->get();
            //aqui va otro foreach para valores ordenes y que genera las PO

            //Calcular el total de materiales
            $total=0;
            foreach ($valoresOrdenes as $valorOrden){
                if (($valorOrden->servicio)==1){
                    $total += ($valorOrden->costo)*($valorOrden->cantidad);
                }
            }

            //En caso de que no exista ninguna PO
            $ordenes=Ordenes::where(['empresa_id'=>session('empresa_id')])->get();
            
            $orden=0;
            if (($ordenes->last())==null){
                $orden=1;
                Ordenes::create([
                    'empresa_id'=> session('empresa_id'),
                    'orden'=>$orden, 
                    'version'=>0, 
                    'total'=>$total, 
                    'cotizacion'=>$cotizacion, 
                    'fechaProgramadaEntrega'=>"ninguna", 
                    'fechaLLegada'=>"ninguna", 
                    'proveedor'=>$proveedor,
                    'direccionProveedor_id'=>null, 
                    'emailProveedor_id'=>null, 
                    'telefonoProveedor_id'=>null, 
                    'condicionesPagos'=>'ninguno',]);
            }else{ 
                $orden = ($ordenes->last()->orden)+1;
                Ordenes::create([
                    'empresa_id'=> session('empresa_id'),
                    'orden'=>$orden, 
                    'version'=>0, 
                    'total'=>$total, 
                    'cotizacion'=>$cotizacion, 
                    'fechaProgramadaEntrega'=>"ninguna", 
                    'fechaLLegada'=>"ninguna", 
                    'proveedor'=>$proveedor,
                    'direccionProveedor_id'=>null, 
                    'emailProveedor_id'=>null, 
                    'telefonoProveedor_id'=>null, 
                    'condicionesPagos'=>"ninguna",]);
            }

            //foreach valores ordenes actualizar orden
            $actualizarValorOrden['orden']=$orden;
            foreach($valoresOrdenes as $valorOrden){
                ValoresOrdenes::find($valorOrden->id)->fill($actualizarValorOrden)->save();
            }
        }

        return redirect()->route('ordenes');

    }

    //pdf
    public function pdfOrdenMaterial(Ordenes $orden){
        $proveedor=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>(int)$orden->proveedor])->get();
        $valoresOrdenes=ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$orden->cotizacion, 'orden'=>$orden->orden, 'material'=>1])->get();

        $pdf = PDF::loadView('pdf/ordenes', [
            'tipoOrden'=>"Materiales",
            'orden'=>$orden,
            'noOrden'=>OrdenesController::formatoP($orden->orden),
            'proveedor'=>$proveedor->last()->proveedor,
            'valoresOrdenes'=>$valoresOrdenes,
            ]);
            return $pdf->stream();
    }

    public function pdfOrdenServicio(Ordenes $orden){
        $proveedor=Proveedores::where(['empresa_id'=>session('empresa_id'), 'id'=>(int)$orden->proveedor])->get();
        $valoresOrdenes=ValoresOrdenes::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$orden->cotizacion, 'orden'=>$orden->orden, 'servicio'=>1])->get();
        
        $pdf = PDF::loadView('pdf/ordenes', [
        'tipoOrden'=>"Materiales",
        'orden'=>$orden,
        'noOrden'=>OrdenesController::formatoP($orden->orden),
        'proveedor'=>$proveedor->last()->proveedor,
        'valoresOrdenes'=>$valoresOrdenes,
        ]);
        return $pdf->stream();
    }

    //funcion para formato de las ordenes
    public static function formatoP($valor){
        if ($valor<10){
            $noCotizacion = 'PO 0000'.$valor;
        }else if  ($valor<100){
            $noCotizacion = 'PO 000'.$valor;
        }else if  ($valor<1000){
            $noCotizacion = 'PO 00'.$valor;
        }else if  ($valor<10000){
            $noCotizacion = 'PO 0'.$valor;
        }elseif  ($valor<100000){
            $noCotizacion = 'PO '.$valor;
        }else{
            $noCotizacion = 'error';
        }
        return $noCotizacion;
    } 
    
}
