<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use App;
use PDF;
use App\Model\Cotizaciones;
use App\Model\ValoresCotizaciones;
use App\Model\Materiales;
use App\Model\Servicios;
use App\Model\Empresas;
use App\Model\Clientes;
use App\Model\Admins;
use App\Model\Marcas;
use App\Model\Lineas;
use App\Model\Monedas;
use App\Model\Unidades;
use App\Model\Proveedores;
use App\Model\CondicionesComerciales;
use App\Model\ContactosClientes;
use App\Model\DireccionesClientes;
use App\Model\EmailsClientes;
use App\Model\TelefonosClientes;
use Carbon\Carbon;

class CotizacionesController extends Controller
{
    public function regresarEditarCotizacionActual($noCotizacion){
        $cotizacion = Cotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$noCotizacion])->first();
        return redirect()->route('editarCotizacion', $cotizacion);
    }

    public function cancelarCotizacion(Cotizaciones $cotizacion){
        return view('crm/cotizaciones/cancelarCotizacion', ['cotizacion'=>$cotizacion]);
    }

    public function finalizarCancelacionCotizacion(Cotizaciones $cotizacion){
        $data = request()->validate([
            'motivo'=>['required'],
        ],[
            'motivo.required'=>'Ingresa un Motivo',
        ]);

        //Tiempo
        $mytime = Carbon::now();

        //Status a cancelacion
        $data['motivoCancelacion']=$data['motivo'];
        $data['fechaCancelacion']=$mytime->toDateTimeString();
        $data['cancelo_id']=session('id');
        $data['status_id']=5;

        $cotizacion->update($data);
        return redirect()->route('cotizaciones');
    }

    public function Cotizaciones(){
        //todas las cotizaciones
        /*$cotizaciones = Cotizaciones::where(['empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizaciones); $i++){
            $cotizaciones[$i]->cotizacion = CotizacionesController::formatoQ($cotizaciones[$i]->cotizacion);
        }*/

        //Regresar cotizaciones sin finalizar
        $cotizacionesPendientes = Cotizaciones::where(['status_id' => 1, 'empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizacionesPendientes); $i++){
            $cotizacionesPendientes[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesPendientes[$i]->cotizacion);
        }

        //Regresar cotizaciones en Proceso
        $cotizacionesProceso= Cotizaciones::where(['status_id' => 2, 'empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizacionesProceso); $i++){
            $cotizacionesProceso[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesProceso[$i]->cotizacion);
        }

        //Regresar cotizaciones Autorizadas
        $cotizacionesAutorizadas= Cotizaciones::where(['status_id' => 3, 'empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizacionesAutorizadas); $i++){
            $cotizacionesAutorizadas[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesAutorizadas[$i]->cotizacion);
        }

        //Regresar cotizaciones en Venta
        $cotizacionesVenta= Cotizaciones::where(['status_id' => 4, 'empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizacionesVenta); $i++){
            $cotizacionesVenta[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesVenta[$i]->cotizacion);
        }

        //Regresar cotizaciones Canceladas
        $cotizacionesCanceladas= Cotizaciones::where(['status_id' => 5, 'empresa_id'=>session('empresa_id')])
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizacionesCanceladas); $i++){
            $cotizacionesCanceladas[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesCanceladas[$i]->cotizacion);
        }

        //Regresar cotizaciones Versionadas
        $cotizacionesVersionadas= Cotizaciones::where(['empresa_id'=>session('empresa_id')])
            ->where('version', '!=' , 0)
            ->where('status_id', '!=' , 5)
            ->orderBy('id', 'DESC')->get();
        for($i=0; $i<count($cotizacionesVersionadas); $i++){
            $cotizacionesVersionadas[$i]->cotizacion = CotizacionesController::formatoQ($cotizacionesVersionadas[$i]->cotizacion);
        }

        //dd($cotizacionesPendientes);
        return view('crm/cotizaciones/cotizaciones', ['cotizacionesPendientes'=>$cotizacionesPendientes,
                                                    'cotizacionesCanceladas'=>$cotizacionesCanceladas,
                                                    'cotizacionesProceso'=>$cotizacionesProceso,
                                                    'cotizacionesAutorizadas'=>$cotizacionesAutorizadas,
                                                    'cotizacionesVenta'=>$cotizacionesVenta,
                                                    'cotizacionesVersionadas'=>$cotizacionesVersionadas]);
    }

    public function nuevaCotizacion(){
        //olvidar ultima cotizacion
        if (session('noCotizacion')!=null){
            session()->forget('noCotizacion');
            session()->forget('versionCotizacion');
        }

        //consulta de clientes y admins
        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $admins = Admins::where(['empresa_id'=>session('empresa_id')])->get();
        $condicionesComerciales = CondicionesComerciales::where(['empresa_id'=>session('empresa_id'), 'tipo'=>'venta'])->get();
        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id')])->first()->get();

        $cotizacionQuery = Cotizaciones::where(['empresa_id'=>session('empresa_id')])->get();

        //Tiempo
        $mytime = Carbon::now();

        if (($cotizacionQuery->last())==null){
            $cotizacion=1;
            Cotizaciones::create([
                'empresa_id'=> session('empresa_id'),
                'cotizacion'=> 1,
                'version'=> 0,
                'fechaElaboracion'=>$mytime->toDateTimeString(),
                'fechaAutorizacion'=>"00-00-00 00:00:00",
                'fechaCancelacion'=>"00-00-00 00:00:00",
                'motivoCancelacion'=>"Ninguno",
                'status_id'=>1,
                'condicionesComerciales'=>"Ninguna",
                'condicionesPagos'=>"Ninguna",
                'comentarios'=>"Ninguno",
                'totalMateriales'=>0.00,
                'totalServicios'=>0.00,
                'descuentoClienteFrecuente'=>0.00,
                'descuentoClienteEspecial'=>0.00,
                'descuentoEspecial'=>0.00,
                'cliente_id'=>1,
                'elaboro_id'=>1,
                'contactoCliente_id'=>null,
                'direccionCliente_id'=>null,
                'emailCliente_id'=>null,
                'telefonoCliente_id'=>null]);
        }else{
            $cotizacion = ($cotizacionQuery->last()->cotizacion)+1;
            Cotizaciones::create([
                'empresa_id'=> session('empresa_id'),
                'cotizacion'=> $cotizacion,
                'version'=> 0,
                'fechaElaboracion'=>$mytime->toDateTimeString(),
                'fechaAutorizacion'=>"00-00-00 00:00:00",
                'fechaCancelacion'=>"00-00-00 00:00:00",
                'motivoCancelacion'=>"Ninguno",
                'status_id'=>1,
                'condicionesComerciales'=>"Ninguna",
                'condicionesPagos'=>"Ninguna",
                'comentarios'=>"Ninguno",
                'totalMateriales'=>0.00,
                'totalServicios'=>0.00,
                'descuentoClienteFrecuente'=>0.00,
                'descuentoClienteEspecial'=>0.00,
                'descuentoEspecial'=>0.00,
                'cliente_id'=>1,
                'elaboro_id'=>1,
                'contactoCliente_id'=>null,
                'direccionCliente_id'=>null,
                'emailCliente_id'=>null,
                'telefonoCliente_id'=>null]);
        }

        //Volver a consultar tabla
        $cotizacionLastQuery = Cotizaciones::where(['cotizacion'=>$cotizacion, 'empresa_id'=>session('empresa_id')])->first();

        //Asignar a session el noCotizacion actual
        session(['noCotizacion'=>$cotizacion]);
        session(['versionCotizacion'=>0]);

        //consultar valores de la cotizacion
        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>session('noCotizacion')])->get();

        //Separar por moneda
        $monedasArray = array();
        foreach ($valoresCotizaciones as $valorCotizacion){
            if(!in_array(($valorCotizacion->moneda_id), $monedasArray)){
                array_push($monedasArray, ($valorCotizacion->monedas->moneda));
            }
        }

        if(sizeof($monedasArray)==0){
            //Calcular el total de materiales
            $totalMateriales=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->material)==1){
                    $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalMaterialestexto=$totalMateriales;

            //Calcular el total de servicios
            $totalServicios=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->servicio)==1){
                    $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalServiciostexto=$totalServicios;

        }else {
            //Calcular el total de materiales
            $totalMaterialestexto="";
            foreach($monedasArray as $eachMoneda){
                $totalMateriales=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->material)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalMaterialestexto.=" $ ".number_format($totalMateriales, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }

             //Calcular el total de servicios
            $totalServiciostexto="";
            foreach($monedasArray as $eachMoneda){
                $totalServicios=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->servicio)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalServiciostexto.=" $ ".number_format($totalServicios, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }
        }

        //numero de condiciones comerciales
        $condiciones=array();
        foreach($condicionesComerciales as $condicionComercial){
            array_push($condiciones, $condicionComercial->orden);
        }

        return view('crm/cotizaciones/editarCotizacion', [
            'valoresCotizaciones'=>$valoresCotizaciones,
            'cotizacion'=>$cotizacionLastQuery,
            'noCotizacion'=>CotizacionesController::formatoQ($cotizacion),
            'clientes'=>$clientes,
            'admins'=>$admins,
            'totalMateriales'=>$totalMaterialestexto,
            'totalServicios'=>$totalServiciostexto,
            'condicionesComerciales'=>$condicionesComerciales,
            'condiciones'=>$condiciones]);
    }

    public function editarCotizacion(Cotizaciones $cotizacion){
        session(['noCotizacion'=>$cotizacion->cotizacion]);
        session(['versionCotizacion'=>$cotizacion->version]);

        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $admins = Admins::where(['empresa_id'=>session('empresa_id')])->get();
        $condicionesComerciales = CondicionesComerciales::where(['empresa_id'=>session('empresa_id'), 'tipo'=>'venta'])->get();
        if (($cotizacion->condicionesComerciales)=='Ninguna'){
            //numero de condiciones comerciales
            $condiciones=array();
            foreach($condicionesComerciales as $condicionComercial){
            array_push($condiciones, $condicionComercial->orden);
            }
        }else {
            $condiciones=array();
            $condicionesArray= explode(',', $cotizacion->condicionesComerciales);
            foreach($condicionesArray as $condicion){
                array_push($condiciones, (int)$condicion);
            }
        }

        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>session('noCotizacion'), 'version'=>$cotizacion->version])->get();

        //Separar por moneda
        $monedasArray = array();
        foreach ($valoresCotizaciones as $valorCotizacion){
            if(!in_array(($valorCotizacion->monedas->moneda), $monedasArray)){
                array_push($monedasArray, ($valorCotizacion->monedas->moneda));
            }
        }

        if(sizeof($monedasArray)==0){
            //Calcular el total de materiales
            $totalMateriales=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->material)==1){
                    $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalMaterialestexto=$totalMateriales;

            //Calcular el total de servicios
            $totalServicios=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->servicio)==1){
                    $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalServiciostexto=$totalServicios;

        }else {
            //Calcular el total de materiales
            $totalMaterialestexto="";
            foreach($monedasArray as $eachMoneda){
                $totalMateriales=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->material)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalMaterialestexto.=" $ ".number_format($totalMateriales, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }

             //Calcular el total de servicios
            $totalServiciostexto="";
            foreach($monedasArray as $eachMoneda){
                $totalServicios=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->servicio)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalServiciostexto.=" $ ".number_format($totalServicios, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }
        }


        return view('crm/cotizaciones/editarCotizacion', [
            'cotizacion'=>$cotizacion,
            'noCotizacion'=>CotizacionesController::formatoQ($cotizacion->cotizacion),
            'clientes'=>$clientes,
            'admins'=>$admins,
            'valoresCotizaciones'=>$valoresCotizaciones,
            'totalMateriales'=>$totalMaterialestexto,
            'totalServicios'=>$totalServiciostexto,
            'condicionesComerciales'=>$condicionesComerciales,
            'condiciones'=>$condiciones]);
    }

    public function actualizarCotizacion(Cotizaciones $cotizacion){
        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>session('noCotizacion'), 'version'=>$cotizacion->version])->get();

        $data = request()->validate([
            'cliente_id'=>['required'],
            'autorizo_id'=>['required'],
            'elaboro_id'=>['required'],
            'condicionesComerciales'=>['required'],
            'condicionesPagos'=>['required'],
            'comentarios'=>['required'],
        ]);

        //Separar por moneda
        $monedasArray = array();
        foreach ($valoresCotizaciones as $valorCotizacion){
            if(!in_array(($valorCotizacion->monedas->moneda), $monedasArray)){
                array_push($monedasArray, ($valorCotizacion->monedas->moneda));
            }
        }

        if(sizeof($monedasArray)==0){
            //Calcular el total de materiales
            $totalMateriales=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->material)==1){
                    $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalMaterialestexto=$totalMateriales;

            //Calcular el total de servicios
            $totalServicios=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->servicio)==1){
                    $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalServiciostexto=$totalServicios;

        }else {
            //Calcular el total de materiales
            $totalMaterialestexto="";
            foreach($monedasArray as $eachMoneda){
                $totalMateriales=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->material)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalMaterialestexto.=" $ ".number_format($totalMateriales, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }

             //Calcular el total de servicios
            $totalServiciostexto="";
            foreach($monedasArray as $eachMoneda){
                $totalServicios=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->servicio)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalServiciostexto.=" $ ".number_format($totalServicios, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }
        }

        //otros datos
        $data['version']=($cotizacion->version)+1;
        $data['status_id']=2;
        $data['totalMateriales']=$totalMaterialestexto;
        $data['totalServicios']=$totalServiciostexto;

        $cotizacion->update($data);

        //Aqui se duplican los valores de cotizacion para la nueva version
        foreach($valoresCotizaciones as $valorCotizacion){
            ValoresCotizaciones::create([
                'cotizacion'=> $valorCotizacion->cotizacion,
                'version'=> $cotizacion->version,
                'empresa_id'=>$valorCotizacion->empresa_id,
                'material'=>$valorCotizacion->material,
                'servicio'=>$valorCotizacion->servicio,
                'codigo'=>$valorCotizacion->codigo,
                'nombre'=>$valorCotizacion->nombre,
                'marca_id'=>$valorCotizacion->marca_id,
                'linea_id'=>$valorCotizacion->linea_id,
                'costo'=>$valorCotizacion->costo,
                'margen'=>$valorCotizacion->margen,
                'cantidad'=>$valorCotizacion->cantidad,
                'restanteOrdenes'=>$valorCotizacion->cantidad,
                'unidad_id'=>$valorCotizacion->unidad_id,
                'proveedor'=>$valorCotizacion->proveedor,
                'moneda_id'=>$valorCotizacion->moneda_id,
            ]);
        }

        //Tiempo
        $mytime = Carbon::now();

        //Mas datos para el correo
        $data['cotizacion']=$cotizacion->id;
        $data['noCotizacion']=CotizacionesController::formatoQ($cotizacion->cotizacion);
        $data['cliente']=$cotizacion->clientes->cliente;
        $data['fecha']=$mytime->toDateTimeString();


        Mail::send('crm.email.cotizacion', $data, function ($message) use($cotizacion) {
            $message->from('no-reply@smartpmce.com', 'SmartPmce');
            $message->to($cotizacion->autorizo->email, $cotizacion->autorizo->nombre.' '.$cotizacion->autorizo->materno.' '.$cotizacion->autorizo->paterno);
            $message->subject('Cotizacion ');
        });

        
        //Olvidar variables
        session()->forget('noCotizacion');
        session()->forget('versionCotizacion');

        return redirect()->route('cotizaciones');
    }

    public function procesarCotizacion(Cotizaciones $cotizacion){
        $data['status_id']=2;
        $cotizacion->update($data);

        return redirect()->route('cotizaciones');
    }

    public function autorizarCotizacion(Cotizaciones $cotizacion){
        //Tiempo
        $mytime = Carbon::now();
        $data['fechaAutorizacion']=$mytime->toDateTimeString();
        $data['status_id']=3;
        $cotizacion->update($data);

        return redirect()->route('cotizaciones');
    }

    //pendiente
    public function procesarVentaCotizacion(Cotizaciones $cotizacion){

        $contactosClientes = ContactosClientes::where(['empresa_id'=>session('empresa_id'), 'cliente_id'=>$cotizacion->cliente_id])->get();
        $direccionesClientes = DireccionesClientes::where(['empresa_id'=>session('empresa_id'), 'contacto_id'=>$contactosClientes->first()->id])->get();
        $emailsClientes = EmailsClientes::where(['empresa_id'=>session('empresa_id'), 'contacto_id'=>$contactosClientes->first()->id])->get();
        $telefonosClientes = TelefonosClientes::where(['empresa_id'=>session('empresa_id'), 'contacto_id'=>$contactosClientes->first()->id])->get();

        return view('crm/cotizaciones/generarVenta', ['cotizacion'=>$cotizacion, 
        'contactosClientes'=>$contactosClientes,
        'direccionesClientes'=>$direccionesClientes,
        'emailsClientes'=>$emailsClientes,
        'telefonosClientes'=>$telefonosClientes]);
    }

    public function datosVenta(Request $request){
        $direccionesClientes = DireccionesClientes::where(['empresa_id'=>session('empresa_id'), 'contacto_id'=>$request->contacto_id])->orderBy('id', 'DESC')->get();
        $emailsClientes = EmailsClientes::where(['empresa_id'=>session('empresa_id'), 'contacto_id'=>$request->contacto_id])->orderBy('id', 'DESC')->get();
        $telefonosClientes = TelefonosClientes::where(['empresa_id'=>session('empresa_id'), 'contacto_id'=>$request->contacto_id])->orderBy('id', 'DESC')->get();
        
        return response()->json(array('direcciones'=> $direccionesClientes, 'emails'=>$emailsClientes->all(), 'telefonos'=>$telefonosClientes->all()), 200);
    }

    public function finalizarVentaCotizacion(Cotizaciones $cotizacion){
        $data = request()->validate([
            'contactoCliente_id'=>['required'],
            'direccionCliente_id'=>['required'],
            'emailCliente_id'=>['required'],
            'telefonoCliente_id'=>['required'],
        ]);
        if($data['direccionCliente_id']=="ninguno"){
            $data['direccionCliente_id']=null;
        }
        if($data['emailCliente_id']=="ninguno"){
            $data['emailCliente_id']=null;
        }
        if($data['telefonoCliente_id']=="ninguno"){
            $data['telefonoCliente_id']=null;
        }
        $data['status_id']=4;

        $cotizacion->update($data);
        return redirect()->route('cotizaciones');

    }

    //Genera el pdf de la cotizacion
    public function pdfCotizacion(Cotizaciones $cotizacion){

        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $admins = Admins::where(['empresa_id'=>session('empresa_id')])->get();
        $condicionesComerciales = CondicionesComerciales::where(['empresa_id'=>session('empresa_id'), 'tipo'=>'venta'])->get();
        if (($cotizacion->condicionesComerciales)=='Ninguna'){
            //numero de condiciones comerciales
            $condiciones=array();
            foreach($condicionesComerciales as $condicionComercial){
            array_push($condiciones, $condicionComercial->orden);
            }
        }else {
            $condiciones=array();
            $condicionesArray= explode(',', $cotizacion->condicionesComerciales);
            foreach($condicionesArray as $condicion){
                array_push($condiciones, (int)$condicion);
            }
        }
        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion->cotizacion, 'version'=>$cotizacion->version])->get();

        //Separar por moneda
        $monedasArray = array();
        foreach ($valoresCotizaciones as $valorCotizacion){
            if(!in_array(($valorCotizacion->monedas->moneda), $monedasArray)){
                array_push($monedasArray, ($valorCotizacion->monedas->moneda));
            }
        }

        if(sizeof($monedasArray)==0){
            //Calcular el total de materiales
            $totalMateriales=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->material)==1){
                    $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalMaterialestexto=$totalMateriales;

            //Calcular el total de servicios
            $totalServicios=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->servicio)==1){
                    $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalServiciostexto=$totalServicios;

        }else {
            //Calcular el total de materiales
            $totalMaterialestexto="";
            foreach($monedasArray as $eachMoneda){
                $totalMateriales=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->material)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalMaterialestexto.=" $ ".number_format($totalMateriales, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }

             //Calcular el total de servicios
            $totalServiciostexto="";
            foreach($monedasArray as $eachMoneda){
                $totalServicios=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->servicio)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalServiciostexto.=" $ ".number_format($totalServicios, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }
        }

        $version=$cotizacion->version;


        $pdf = PDF::loadView('pdf/cotizaciones', [
            'cotizacion'=>$cotizacion,
            'noCotizacion'=>CotizacionesController::formatoQ($cotizacion->cotizacion),
            'version'=>$version,
            'clientes'=>$clientes,
            'admins'=>$admins,
            'valoresCotizaciones'=>$valoresCotizaciones,
            'totalMateriales'=>$totalMaterialestexto,
            'totalServicios'=>$totalServiciostexto,
            'condicionesComerciales'=>$condicionesComerciales,
            'condiciones'=>$condiciones]);
        return $pdf->stream();
    }

    //Genera el pdf de la cotizacion
    public function pdfVersionCotizacion(Cotizaciones $cotizacion, $version){

        $clientes = Clientes::where(['empresa_id'=>session('empresa_id')])->get();
        $admins = Admins::where(['empresa_id'=>session('empresa_id')])->get();
        $condicionesComerciales = CondicionesComerciales::where(['empresa_id'=>session('empresa_id'), 'tipo'=>'venta'])->get();
        if (($cotizacion->condicionesComerciales)=='Ninguna'){
            //numero de condiciones comerciales
            $condiciones=array();
            foreach($condicionesComerciales as $condicionComercial){
            array_push($condiciones, $condicionComercial->orden);
            }
        }else {
            $condiciones=array();
            $condicionesArray= explode(',', $cotizacion->condicionesComerciales);
            foreach($condicionesArray as $condicion){
                array_push($condiciones, (int)$condicion);
            }
        }
        $valoresCotizaciones = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>$cotizacion->cotizacion, 'version'=>($version-1)])->get();

        //Separar por moneda
        $monedasArray = array();
        foreach ($valoresCotizaciones as $valorCotizacion){
            if(!in_array(($valorCotizacion->monedas->moneda), $monedasArray)){
                array_push($monedasArray, ($valorCotizacion->monedas->moneda));
            }
        }

        if(sizeof($monedasArray)==0){
            //Calcular el total de materiales
            $totalMateriales=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->material)==1){
                    $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalMaterialestexto=$totalMateriales;

            //Calcular el total de servicios
            $totalServicios=0;
            foreach ($valoresCotizaciones as $valorCotizacion){
                if (($valorCotizacion->servicio)==1){
                    $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                }
            }
            $totalServiciostexto=$totalServicios;

        }else {
            //Calcular el total de materiales
            $totalMaterialestexto="";
            foreach($monedasArray as $eachMoneda){
                $totalMateriales=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->material)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalMateriales += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalMaterialestexto.=" $ ".number_format($totalMateriales, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }

             //Calcular el total de servicios
            $totalServiciostexto="";
            foreach($monedasArray as $eachMoneda){
                $totalServicios=0;
                foreach ($valoresCotizaciones as $valorCotizacion){
                    if (($valorCotizacion->servicio)==1){
                        if ($eachMoneda==$valorCotizacion->monedas->moneda){
                            $totalServicios += (($valorCotizacion->costo/(1-($valorCotizacion->margen)/100)))*($valorCotizacion->cantidad);
                        }
                    }
                }
                $totalServiciostexto.=" $ ".number_format($totalServicios, 2, ".", ",")." ".$eachMoneda."<br>" ;
            }
        }


        $pdf = PDF::loadView('pdf/cotizaciones', [
            'cotizacion'=>$cotizacion,
            'noCotizacion'=>CotizacionesController::formatoQ($cotizacion->cotizacion),
            'version'=>$version,
            'clientes'=>$clientes,
            'admins'=>$admins,
            'valoresCotizaciones'=>$valoresCotizaciones,
            'totalMateriales'=>$totalMaterialestexto,
            'totalServicios'=>$totalServiciostexto,
            'condicionesComerciales'=>$condicionesComerciales,
            'condiciones'=>$condiciones]);
        return $pdf->stream();
    }

    //Materiales
    
    public function agregarMaterialCotizacion(Request $request){

        //Checar materiales que ya estan en la cotizacion
        $materialesEnCotizacion = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>session('noCotizacion')]);

        if ($request->get('texto')){
            $texto = explode(" ", $request->get('texto'));

            $materiales= Materiales::query();

            foreach ($texto as $filtro){
                $materiales->orWhere('nombre', 'LIKE', '%'.$filtro.'%');
                $materiales->orWhere('codigo', 'LIKE', '%'.$filtro.'%');
            }

            $materiales = $materiales->distinct()->paginate(20);

        }else{
            $materiales = Materiales::orderBy('id', 'DESC')->paginate(20);
        }

        return view('crm/cotizaciones/agregarMaterialCotizacion', compact('materiales'));
    }

    public function subirMaterialCotizacion(Materiales $material){
        $data = request()->validate([
            'cantidad'=>['required', 'integer', 'not_in:0'],
            'margen'=>['required', 'integer', 'not_in:0'],
        ],[
            'cantidad.required'=>'Inserte una cantidad',
            'cantidad.integer'=>'Inserta solo enteros',
            'cantidad.not_in'=>'La cantidad no puede ser 0',
            'margen.required'=>'Inserte un margen',
            'margen.integer'=>'Inserta solo enteros',
            'margen.not_in'=>'El margen no puede ser 0',
        ]);

        $precio = ($material->costo)/(1-($data['margen']));

        ValoresCotizaciones::create([
            'cotizacion'=> session('noCotizacion'),
            'version'=> session('versionCotizacion'),
            'empresa_id'=>session('empresa_id'),
            'material'=>1,
            'servicio'=>0,
            'codigo'=>$material->codigo,
            'nombre'=>$material->nombre,
            'marca_id'=>$material->marca_id,
            'linea_id'=>$material->linea_id,
            'costo'=>$material->costo,
            'margen'=>$data['margen'],
            'cantidad'=>$data['cantidad'],
            'restanteOrdenes'=>$data['cantidad'],
            'unidad_id'=>$material->unidad_id,
            'proveedor'=>$material->proveedor,
            'moneda_id'=>$material->moneda_id,
        ]);

        return redirect()->route('agregarMaterialCotizacion'); 
    }

    public function crearMaterialFueraCatalogo(Request $request){
        $data = request()->validate([
            'nombre'=>['required', 'unique:valoresCotizaciones,nombre'],
            'codigo'=>['required', 'unique:valoresCotizaciones,codigo'],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
            'cantidad'=>['required', 'integer', 'not_in:0'],
            'margen'=>['required', 'integer', 'not_in:0'],
        ],[
            'nombre.required'=>'Inserte un nombre del material',
            'nombre.unique'=>'El nombre ya existe en la cotizacion',
            'codigo.required'=>'Inserta un codigo',
            'codigo.unique'=>'El código ya existe en la cotizacion',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
            'cantidad.required'=>'Inserte una cantidad',
            'cantidad.integer'=>'Inserta solo enteros',
            'cantidad.not_in'=>'La cantidad no puede ser 0',
            'margen.required'=>'Inserte un margen',
            'margen.integer'=>'Inserta solo enteros',
            'margen.not_in'=>'El margen no puede ser 0',
        ]);

        ValoresCotizaciones::create([
            'cotizacion'=> session('noCotizacion'),
            'version'=> session('versionCotizacion'),
            'empresa_id'=>session('empresa_id'),
            'material'=>1,
            'servicio'=>0,
            'codigo'=>$data['codigo'],
            'nombre'=>$data['nombre'],
            'marca_id'=>$data['marca_id'],
            'linea_id'=>$data['linea_id'],
            'costo'=>$data['costo'],
            'margen'=>$data['margen'],
            'cantidad'=>$data['cantidad'],
            'restanteOrdenes'=>$data['cantidad'],
            'unidad_id'=>$data['unidad_id'],
            'proveedor'=>$data['proveedor'],
            'moneda_id'=>$data['moneda_id'],
        ]);

        return redirect()->route('materialFueraCatalogo'); 

    }

    public function materialFueraCatalogo(){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        
        //numero de proveedores
        $numeroProveedores=array();
        foreach($proveedores as $proveedor){
            array_push($numeroProveedores, $proveedor->id);
        }

        return view('crm/cotizaciones/materialFueraCatalogo', ['marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function editarMaterial(ValoresCotizaciones $valorCotizacion){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();

        //numero de proveedores
        $numeroProveedores=array();
        $proveedorArray= explode(',', $valorCotizacion->proveedor);
        foreach($proveedorArray as $proveedor){
            array_push($numeroProveedores, (int)$proveedor);
        }

        return view('crm/cotizaciones/editarMaterial', ['valorCotizacion'=>$valorCotizacion, 
        'marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function actualizarMaterial(ValoresCotizaciones $valorCotizacion){
        $data = request()->validate([
            'nombre'=>['required', 'unique:valoresCotizaciones,nombre,'.$valorCotizacion->id],
            'codigo'=>['required', 'unique:valoresCotizaciones,codigo,'.$valorCotizacion->id],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
            'cantidad'=>['required', 'integer', 'not_in:0'],
            'margen'=>['required', 'integer', 'not_in:0'],
        ],[
            'nombre.required'=>'Inserte un nombre del material',
            'nombre.unique'=>'El nombre ya existe en la cotizacion',
            'codigo.required'=>'Inserta un codigo',
            'codigo.unique'=>'El código ya existe en la cotizacion',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
            'cantidad.required'=>'Inserte una cantidad',
            'cantidad.integer'=>'Inserta solo enteros',
            'cantidad.not_in'=>'La cantidad no puede ser 0',
            'margen.required'=>'Inserte un margen',
            'margen.integer'=>'Inserta solo enteros',
            'margen.not_in'=>'El margen no puede ser 0',
        ]);

        $data['version']=session('versionCotizacion');

        $valorCotizacion->update($data);

        return redirect()->route('regresarEditarCotizacionActual', ['cotizacion'=>session('noCotizacion')]);
    }

    public function borrarMaterial(ValoresCotizaciones $valorCotizacion){

        $valorCotizacion->delete();

        return redirect()->route('regresarEditarCotizacionActual', ['cotizacion'=>session('noCotizacion')]);
    }

    //Servicios

    public function agregarServicioCotizacion(Request $request){

        //Checar servicios que ya estan en la cotizacion
        $serviciosEnCotizacion = ValoresCotizaciones::where(['empresa_id'=>session('empresa_id'), 'cotizacion'=>session('noCotizacion')]);

        if ($request->get('texto')){
            $texto = explode(" ", $request->get('texto'));

            $servicios= Servicios::query();

            foreach ($texto as $filtro){
                $servicios->orWhere('nombre', 'LIKE', '%'.$filtro.'%');
                $servicios->orWhere('codigo', 'LIKE', '%'.$filtro.'%');
            }

            $servicios = $servicios->distinct()->paginate(20);

        }else{
            $servicios = Servicios::orderBy('id', 'DESC')->paginate(20);
        }

        return view('crm/cotizaciones/agregarServicioCotizacion', compact('servicios'));
    }

    public function subirServicioCotizacion(Servicios $servicio){
        $data = request()->validate([
            'cantidad'=>['required', 'integer', 'not_in:0'],
            'margen'=>['required', 'integer', 'not_in:0'],
        ],[
            'cantidad.required'=>'Inserte una cantidad',
            'cantidad.integer'=>'Inserta solo enteros',
            'cantidad.not_in'=>'La cantidad no puede ser 0',
            'margen.required'=>'Inserte un margen',
            'margen.integer'=>'Inserta solo enteros',
            'margen.not_in'=>'El margen no puede ser 0',
        ]);

        ValoresCotizaciones::create([
            'cotizacion'=> session('noCotizacion'),
            'version'=> session('versionCotizacion'),
            'empresa_id'=>session('empresa_id'),
            'material'=>0,
            'servicio'=>1,
            'codigo'=>$servicio->codigo,
            'nombre'=>$servicio->nombre,
            'marca_id'=>$servicio->marca_id,
            'linea_id'=>$servicio->linea_id,
            'costo'=>$servicio->costo,
            'margen'=>$data['margen'],
            'cantidad'=>$data['cantidad'],
            'restanteOrdenes'=>$data['cantidad'],
            'unidad_id'=>$servicio->unidad_id,
            'proveedor'=>$servicio->proveedor,
            'moneda_id'=>$servicio->moneda_id,
        ]);

        return redirect()->route('agregarServicioCotizacion'); 
    }

    public function crearServicioFueraCatalogo(Request $request){
        $data = request()->validate([
            'nombre'=>['required', 'unique:valoresCotizaciones,nombre'],
            'codigo'=>['required', 'unique:valoresCotizaciones,codigo'],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
            'cantidad'=>['required', 'integer', 'not_in:0'],
            'margen'=>['required', 'integer', 'not_in:0'],
        ],[
            'nombre.required'=>'Inserte un nombre del servicio',
            'nombre.unique'=>'El nombre ya existe en la cotizacion',
            'codigo.required'=>'Inserta un codigo',
            'codigo.unique'=>'El código ya existe en la cotizacion',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
            'cantidad.required'=>'Inserte una cantidad',
            'cantidad.integer'=>'Inserta solo enteros',
            'cantidad.not_in'=>'La cantidad no puede ser 0',
            'margen.required'=>'Inserte un margen',
            'margen.integer'=>'Inserta solo enteros',
            'margen.not_in'=>'El margen no puede ser 0',
        ]);

        ValoresCotizaciones::create([
            'cotizacion'=> session('noCotizacion'),
            'version'=> session('versionCotizacion'),
            'empresa_id'=>session('empresa_id'),
            'material'=>0,
            'servicio'=>1,
            'codigo'=>$data['codigo'],
            'nombre'=>$data['nombre'],
            'marca_id'=>$data['marca_id'],
            'linea_id'=>$data['linea_id'],
            'costo'=>$data['costo'],
            'margen'=>$data['margen'],
            'cantidad'=>$data['cantidad'],
            'restanteOrdenes'=>$data['cantidad'],
            'unidad_id'=>$data['unidad_id'],
            'proveedor'=>$data['proveedor'],
            'moneda_id'=>$data['moneda_id'],
        ]);

        return redirect()->route('servicioFueraCatalogo'); 

    }

    public function servicioFueraCatalogo(){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();
        
        //numero de proveedores
        $numeroProveedores=array();
        foreach($proveedores as $proveedor){
            array_push($numeroProveedores, $proveedor->id);
        }

        return view('crm/cotizaciones/servicioFueraCatalogo', ['marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function editarServicio(ValoresCotizaciones $valorCotizacion){
        $marcas = Marcas::where(['empresa_id'=>session('empresa_id')])->get();
        $lineas = Lineas::where(['empresa_id'=>session('empresa_id')])->get();
        $monedas = Monedas::all();
        $unidades = Unidades::where(['empresa_id'=>session('empresa_id')])->get();
        $proveedores = Proveedores::where(['empresa_id'=>session('empresa_id')])->get();

        //numero de proveedores
        $numeroProveedores=array();
        $proveedorArray= explode(',', $valorCotizacion->proveedor);
        foreach($proveedorArray as $proveedor){
            array_push($numeroProveedores, (int)$proveedor);
        }

        return view('crm/cotizaciones/editarServicio', ['valorCotizacion'=>$valorCotizacion, 
        'marcas'=>$marcas, 
        'lineas'=>$lineas, 
        'monedas'=>$monedas, 
        'unidades'=>$unidades, 
        'proveedores'=>$proveedores,
        'numeroProveedores'=>$numeroProveedores]);
    }

    public function actualizarServicio(ValoresCotizaciones $valorCotizacion){
        $data = request()->validate([
            'nombre'=>['required', 'unique:valoresCotizaciones,nombre,'.$valorCotizacion->id],
            'codigo'=>['required', 'unique:valoresCotizaciones,codigo,'.$valorCotizacion->id],
            'marca_id'=>['required'],
            'linea_id'=>['required'],
            'costo'=>['required'],
            'unidad_id'=>['required'],
            'proveedor'=>['required'],
            'moneda_id'=>['required'],
            'cantidad'=>['required', 'integer', 'not_in:0'],
            'margen'=>['required', 'integer', 'not_in:0'],
        ],[
            'nombre.required'=>'Inserte un nombre del servicio',
            'nombre.unique'=>'El nombre ya existe en la cotizacion',
            'codigo.required'=>'Inserta un codigo',
            'codigo.unique'=>'El código ya existe en la cotizacion',
            'costo.required'=>'Inserta un costo',
            'logo.image'=>'El logo debe ser una imagen',
            'proveedor'=>'Inserta un proveedor',
            'cantidad.required'=>'Inserte una cantidad',
            'cantidad.integer'=>'Inserta solo enteros',
            'cantidad.not_in'=>'La cantidad no puede ser 0',
            'margen.required'=>'Inserte un margen',
            'margen.integer'=>'Inserta solo enteros',
            'margen.not_in'=>'El margen no puede ser 0',
        ]);

        $data['version']=session('versionCotizacion');

        $valorCotizacion->update($data);

        return redirect()->route('regresarEditarCotizacionActual', ['cotizacion'=>session('noCotizacion')]);
    }

    public function borrarServicio(ValoresCotizaciones $valorCotizacion){

        $valorCotizacion->delete();

        return redirect()->route('regresarEditarCotizacionActual', ['cotizacion'=>session('noCotizacion')]);
    }

   //funciones Extra

    public static function formatoQ($valor){
        if ($valor<10){
            $noCotizacion = 'Q 0000'.$valor;
        }else if  ($valor<100){
            $noCotizacion = 'Q 000'.$valor;
        }else if  ($valor<1000){
            $noCotizacion = 'Q 00'.$valor;
        }else if  ($valor<10000){
            $noCotizacion = 'Q 0'.$valor;
        }elseif  ($valor<100000){
            $noCotizacion = 'Q '.$valor;
        }else{
            $noCotizacion = 'error';
        }
        return $noCotizacion;
    } 

}
