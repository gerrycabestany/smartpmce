<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Oportunidades extends Model
{
    protected $fillable = [
        'cliente_id', 
        'moneda_id', 
        'oportunidad', 
        'valor', 
        'fecha', 
        'fechaCierre', 
        'emails',
        'empresa_id',
    ];

    public function clientes(){
        return $this->belongsTo('App\Model\Clientes', 'cliente_id');
    }
    
    public function monedas(){
        return $this->belongsTo('App\Model\Monedas', 'moneda_id');
    }
}
