<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ordenes extends Model
{
    protected $fillable = [
        'empresa_id', 
        'orden', 
        'version', 
        'total', 
        'cotizacion', 
        'fechaProgramadaEntrega', 
        'fechaLLegada',
        'proveedor',
        'condicionesPagos',
        'direccionProveedor_id', 
        'emailProveedor_id', 
        'telefonoProveedor_id',  
    ];
    
}
