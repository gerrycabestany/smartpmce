<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ValoresOrdenes extends Model
{
    protected $table = 'valoresOrdenes';
    protected $fillable = [
        'version',
        'orden', 
        'cotizacion',
        'empresa_id', 
        'material', 
        'servicio', 
        'codigo',
        'nombre',
        'marca_id',
        'linea_id',
        'costo',
        'cantidad',
        'unidad_id',
        'proveedor',
        'moneda_id'];

    public function materiales(){
        return $this->belongsTo('App\Model\Materiales', 'material_id');
    }

    public function servicios(){
        return $this->belongsTo('App\Model\Servicios', 'servicio_id');
    }

    public function unidades(){
        return $this->belongsTo('App\Model\Unidades', 'unidad_id');
    }

    public function marcas(){
        return $this->belongsTo('App\Model\Marcas', 'marca_id');
    }

    public function monedas(){
        return $this->belongsTo('App\Model\Monedas', 'moneda_id');
    }
}
