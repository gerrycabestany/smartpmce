<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DireccionesProveedores extends Model
{
    protected $table = 'direccionesProveedores';
    protected $fillable = [
        'empresa_id', 
        'tipoDireccion', 
        'calle', 
        'ext', 
        'int', 
        'colonia',
        'municipio', 
        'estado', 
        'cp', 
        'pais', 
        'contacto_id'];
}
