<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailsClientes extends Model
{
    protected $table = 'emailsClientes';
    protected $fillable = [
        'empresa_id', 
        'tipoEmail', 
        'email', 
        'contacto_id'];
}
