<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cotizaciones extends Model
{
    protected $fillable = [
        'cotizacion', 
        'empresa_id', 
        'version',
        'fechaElaboracion',
        'fechaAutorizacion',
        'fechaCancelacion',
        'motivoCancelacion',
        'status_id',
        'cancelo_id',
        'autorizo_id',
        'condicionesComerciales',
        'condicionesPagos',
        'comentarios',
        'materiales',
        'servicios',
        'totalMateriales',
        'totalServicios',
        'descuentoClienteFrecuente',
        'descuentoClienteEspecial',
        'descuentoEspecial',
        'cliente_id',
        'elaboro_id',
        'contactoCliente_id',
        'direccionCliente_id',
        'emailCliente_id',
        'telefonoCliente_id'];

        public function elaboro(){
            return $this->belongsTo('App\Model\Admins', 'elaboro_id');
        }

        public function autorizo(){
            return $this->belongsTo('App\Model\Admins', 'autorizo_id');
        }

        public function cancelo(){
            return $this->belongsTo('App\Model\Admins', 'cancelo_id');
        }
        
        public function clientes(){
            return $this->belongsTo('App\Model\Clientes', 'cliente_id');
        }

        public function contactos(){
            return $this->belongsTo('App\Model\ContactosClientes', 'contactoCliente_id');
        }

        public function direcciones(){
            return $this->belongsTo('App\Model\DireccionesClientes', 'direccionCliente_id');
        }

        public function emails(){
            return $this->belongsTo('App\Model\EmailsClientes', 'emailCliente_id');
        }

        public function telefonos(){
            return $this->belongsTo('App\Model\TelefonosClientes', 'telefonoCliente_id');
        }
}
