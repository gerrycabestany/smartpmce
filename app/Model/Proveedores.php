<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Proveedores extends Model
{
    protected $fillable = [
        'empresa_id', 
        'proveedor', 
        'industria_id', 
        'logo', 
        'rfc', 
        'razonSocial', 
        'credito_id',
        'banco',
        'clabe',
    ];

    public function industrias(){
        return $this->belongsTo('App\Model\Industrias', 'industria_id');
    }
    
    public function creditos(){
        return $this->belongsTo('App\Model\Creditos', 'credito_id');
    }
}