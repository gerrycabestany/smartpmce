<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Admins extends Model
{
    public function empresas(){
        return $this->belongsTo('App\Model\Empresas', 'empresa_id');
    }
}
