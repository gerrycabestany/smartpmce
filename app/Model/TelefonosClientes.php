<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TelefonosClientes extends Model
{
    protected $table = 'telefonosClientes';
    protected $fillable = [
        'empresa_id', 
        'tipoTelefono', 
        'pais',
        'area',
        'telefono', 
        'contacto_id'];
}
