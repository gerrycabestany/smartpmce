<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContactosProveedores extends Model
{
    protected $table = 'contactosProveedores';
    protected $fillable = [
        'empresa_id', 
        'proveedor_id', 
        'nombre', 
        'paterno', 
        'materno', 
        'puesto'];
}
