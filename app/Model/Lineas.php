<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lineas extends Model
{
    protected $fillable = ['linea', 'empresa_id'];
}
