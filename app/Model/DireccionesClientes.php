<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DireccionesClientes extends Model
{
    protected $table = 'direccionesClientes';
    protected $fillable = [
        'empresa_id', 
        'tipoDireccion', 
        'calle', 
        'ext', 
        'int', 
        'colonia',
        'municipio', 
        'estado', 
        'cp', 
        'pais', 
        'contacto_id'];
}
