<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Marcas extends Model
{
    protected $fillable = ['marca', 'empresa_id'];
}
