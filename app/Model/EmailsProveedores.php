<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailsProveedores extends Model
{
    protected $table = 'emailsProveedores';
    protected $fillable = [
        'empresa_id', 
        'tipoEmail', 
        'email', 
        'contacto_id'];
}
