<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Industrias extends Model
{
    protected $fillable = ['industria', 'empresa_id', 'descripcion'];
}
