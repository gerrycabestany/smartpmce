<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TelefonosProveedores extends Model
{
    protected $table = 'telefonosProveedores';
    protected $fillable = [
        'empresa_id', 
        'tipoTelefono', 
        'pais',
        'area',
        'telefono', 
        'contacto_id'];
}
