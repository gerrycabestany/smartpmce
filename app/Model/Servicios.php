<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Servicios extends Model
{
    protected $fillable = [
        'codigo',
        'empresa_id', 
        'nombre', 
        'marca_id', 
        'linea_id', 
        'costo', 
        'unidad_id', 
        'proveedor',
        'moneda_id',
    ];

    public function marcas(){
        return $this->belongsTo('App\Model\Marcas', 'marca_id');
    }

    public function lineas(){
        return $this->belongsTo('App\Model\Lineas', 'linea_id');
    }
    
    public function creditos(){
        return $this->belongsTo('App\Model\Creditos', 'credito_id');
    }

    public function monedas(){
        return $this->belongsTo('App\Model\Monedas', 'moneda_id');
    }

    public function unidades(){
        return $this->belongsTo('App\Model\Unidades', 'unidad_id');
    }
}
