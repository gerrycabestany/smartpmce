<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContactosClientes extends Model
{
    protected $table = 'contactosClientes';

    protected $fillable = [
        'empresa_id', 
        'cliente_id', 
        'nombre', 
        'paterno', 
        'materno', 
        'puesto'];
}
