<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Unidades extends Model
{
    protected $fillable = ['unidad', 'empresa_id', 'descripcion'];
}
