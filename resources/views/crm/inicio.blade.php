@extends('layouts.template')

@section('title', 'Inicio')

@section('user', 'Ulisses')

@section('breadcrumb')
    @parent
@endsection

@section('menuInicio') @parent @endsection
@section('menuCatalogo') @parent @endsection
@section('menuClientes') @parent @endsection
@section('menuProveedores') @parent @endsection
@section('menuSKU') @parent @endsection
@section('menuCotizaciones') @parent @endsection
@section('menuOrdenes') @parent @endsection
@section('menuOportunidades') @parent @endsection
