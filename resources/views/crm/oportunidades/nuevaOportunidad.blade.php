@extends('layouts.template')

@section('title', 'Nueva Oportunidad')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Oportunidades</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('oportunidades')}}">Oportunidades</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Nueva oportunidad</strong></b>
    </li>
</ol>
    
@endsection


@section('menuOportunidades') 
<li class="active">
    <a href="{{route('oportunidades')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Oportunidades</span></a>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Agregar neva oportunidad a la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('crearOportunidad')}}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Oportunidad:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="oportunidad" value="{{old('oportunidad')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Valor:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="valor" value="{{old('valor')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Emails:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="emails" value="{{old('emails')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Fecha:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="fecha" value="{{old('fecha')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Fecha cierre:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="fechaCierre" value="{{old('fechaCierre')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Cliente:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="cliente_id">
                                @foreach ($clientes as $cliente)
                                    <option value="{{$cliente->id}}">{{$cliente->cliente}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Moneda:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="moneda_id">
                                @foreach ($monedas as $moneda)
                                    <option value="{{$moneda->id}}">{{$moneda->moneda}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-w-m btn-primary">Agregar</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('oportunidades')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection