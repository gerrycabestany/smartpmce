@extends('layouts.template')

@section('title', 'Oportunidades')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Oportunidades</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('oportunidades')}}"><strong>Oportunidades</strong></a>
     </li>
</ol>
@endsection

@section('menuOportunidades') 
<li class="active">
    <a href="{{route('oportunidades')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Oportunidades</span></a>
</li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-1">
            <div class="text-center">
                <button class="btn btn-primary dim btn-large-dim" type="button" onclick="location.href='{{route('nuevaOportunidad')}}'"><i class="fa">+</i></button>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach ($oportunidades as $oportunidad)
            <div class="col-lg-3">
                <div class="ibox">
                    <div class="ibox-content">
                        <h2 class="text-navy">
                            {{$oportunidad->oportunidad}}
                        </h2>
                        <h3 class="text-navy">
                            {{$oportunidad->valor}}  {{$oportunidad->monedas->moneda}} 
                        </h3>
                        <h3 class="text-navy">
                            Fecha:<br>
                            Inicio: {{$oportunidad->fecha}} Cierre: {{$oportunidad->fechaCierre}}
                        </h3>
                        <p>{{$oportunidad->clientes->cliente}} </p>
                        <div class="d-flex flex-row-reverse">
                            <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarOportunidad', ['oportunidad'=>$oportunidad->id])}}'"><i class="fa fa-edit"></i></button>
                            &nbsp;  
                            <form action="{{route('borrarOportunidad', ['oportunidad'=>$oportunidad->id])}}" method="POST">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> 
        @endforeach
    </div>
</div>

@endsection