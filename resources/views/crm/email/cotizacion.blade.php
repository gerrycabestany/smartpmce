<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml">
 
<head>
 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	 
		<title>Mensaje de Smart PMCE +</title>
	 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
 
</head>
 
<body style="margin: 0; padding: 0;">
 
	<table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

<!-- CABECERA -->
		<tr>
			<td align="center" bgcolor="#3156C2" style="padding: 40px 0 30px 0;">
				<h3 style="color: white;">Has recibido un correo de SmartPMCE+</h3>
			 
				<img src="img/envelope.png" alt="Correo de Smart PMCE" width="15%" style="display: block;" />
			 
			</td>
 		</tr>
<!-- TERMINA CABECERA --> 





<!-- CUERPO DEL CORREO -->
		<tr>		 
			<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">			 
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
				 

					<tr>					 
						<td>	 
							<h2>Hola</h2>
						</td>					 
					</tr>
 


					<tr>					 
						<td style="padding: 20px 0 30px 0;">
						<h2>Tienes una notificación para autorizar la cotización: </h2> <h1>{{$noCotizacion}} v. {{$version}}</h1>  <h2>del cliente: </h2> <h1>{{$cliente}}</h1> <h2> en la fecha: </h2> <h1>{{$fecha}}</h1> <br>
						<h2>Con un total para materiales de: $ {{$totalMateriales}}</h2><br>
						<h2>y Un total para servicios de: $ {{$totalServicios}}</h2>
							 						
						</td>					 
					</tr>



					<tr>					 
						<td align="center">

							<!--
							<table border="0" cellpadding="0" cellspacing="0" width="100%">	 
								<tr>
								 



									<td width="260" valign="top">
									 
									 
										<table border="1" cellpadding="0" cellspacing="0" width="100%">
										 
											<tr>											 
												<td>
												 
												<img src="img/left.gif" alt="" width="100%" height="140" style="display: block;" />
												 
												</td>												 
											</tr>
												


											<tr>												 
												<td style="padding: 25px 0 0 0;">
												 
												    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
												 
												</td>											 
											</tr>
										 
										</table>									 
									</td>
									 


									<td style="font-size: 0; line-height: 0;" width="20">									 
										&nbsp;									 
									</td>
									 


									<td width="260" valign="top">									 
										<table border="1" cellpadding="0" cellspacing="0" width="100%">
										 
											<tr>											 
											    <td>
											 
											    	<img src="img/right.gif" alt="" width="100%" height="140" style="display: block;" />
											 
											    </td>											 
											</tr>
											 



											<tr>											 
											   	<td style="padding: 25px 0 0 0;">
											 
											    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In tempus adipiscing felis, sit amet blandit ipsum volutpat sed. Morbi porttitor, eget accumsan dictum, nisi libero ultricies ipsum, in posuere mauris neque at erat.
											 
											   </td>											 
											</tr>
										 

										</table>									 
									</td>
								 



								</tr>							 
							</table>
						-->

						
                          <a href="http://smartpmce.com/crm/cotizaciones/autorizarCotizacion/{{$cotizacion}}">Autorizar Cotizacion</a>
                          <a href="http://smartpmce.com/crm/cotizaciones/autorizarCotizacion/{{$cotizacion}}">http://smartpmce.com/crm/cotizaciones/autorizarCotizacion/{{$cotizacion}}</a>

						</td>					 
					</tr>


				 
				</table>							
			</td>		 
		</tr>
<!-- TERMINA CUERPO DEL CORREO -->			 



<!-- PIE DE PÁGINA -->		 
		<tr>		 
			<td bgcolor="#000000" style="padding: 10px 30px 10px 30px;">
			 
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
 
					<tr>
					 
						<td width="75%">
						 
							<h4 style="color: white;">&reg; Software Lab, 2019</h4> <br/>
	 
	 						
						 
						</td>
					 
						<td align="right">
							 
							<table border="0" cellpadding="0" cellspacing="0">	 
								<tr>
								 


									<td>									 
										<a href="http://www.twitter.com/">										 
											<img src="img/tw.png" alt="Twitter" width="38" height="38" style="display: block;" border="0" />										 
										</a>									 
									</td>
								 


								 	<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
								 


									<td>									 
									  	<a href="http://www.facebook.com/">
									 
									   		<img src="img/fb.png" alt="Facebook" width="38" height="38" style="display: block;" border="0" />
									 
									  	</a>									 
									</td>
								 


								</tr>								 
							</table>
						 
						</td>
					 
					</tr>
				 
				</table>
				 
			</td>		 
		</tr>
<!-- TERMINA PIE DE PÁGINA -->	

	</table>
 
</body>


</html>