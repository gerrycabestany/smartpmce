@extends('layouts.template')

@section('title', 'Materiales')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Materiales</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('materiales')}}"><strong>Materiales</strong></a>
     </li>
</ol>
@endsection

@section('menuSKU') 
<li class="active">
    <a href="#"><i class="fa fa-clipboard"></i> <span class="nav-label">SKU's</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="active"><a href="{{route('materiales')}}">Materiales</a></li>
        <li><a href="{{route('servicios')}}">Servicios</a></li>
    </ul>
</li>
@endsection

@section('buscador')
    {{Form::open(['route'=>'materiales','method'=>'GET','class'=>'navbar-form-custom'])}}  
        <div class="form-group">
            {{Form::text('texto', old('texto') , ['class'=>'form-control', 'placeholder'=>'Buscar material', 'id'=>'top-search'])}}
        </div>    
    {{Form::close()}}
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    @if (Session::get('perfil')=='admin' || Session::get('id')=='gerente' || Session::get('id')=='ventas' || Session::get('id')=='compras')
    <div class="row">
        <div class="col-lg-1">
            <div class="text-center">
                <button class="btn btn-primary dim btn-large-dim" type="button" onclick="location.href='{{route('nuevoMaterial')}}'"><i class="fa">+</i></button>
            </div>
        </div>
    </div>
    @endif
    
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Materiales</h5>
                        <!--
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>-->
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Código</th>
                                    <th>Nombre </th>
                                    <th>Marca</th>
                                    <th>Línea</th>
                                    <th>Costo</th>
                                    <th>Moneda</th>
                                    <th>Unidad</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($materiales as $material)
                                <tr>
                                    <td>
                                            <div class="d-flex">
                                                    @if (Session::get('perfil')=='admin' || Session::get('id')=='gerente' || Session::get('id')=='ventas' || Session::get('id')=='compras')
                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarMaterial', ['material'=>$material->id])}}'"><i class="fa fa-edit"></i></button>
                                                    &nbsp;  
                                                    <form action="{{route('borrarMaterial', ['material'=>$material->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        {{method_field('DELETE')}}
                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                    </form>
                                                    @endif
                                                </div>
                                    </td>
                                    <td>{{$material->codigo}}</td>
                                    <td>{{$material->nombre}}</td>
                                    <td>{{$material->marcas->marca}}</td>
                                    <td>{{$material->lineas->linea}}</td>
                                    <td>{{$material->costo}}</td>
                                    <td>{{$material->monedas->moneda}}</td>
                                    <td>{{$material->unidades->unidad}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$materiales->render()}}
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection