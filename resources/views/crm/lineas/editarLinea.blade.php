@extends('layouts.template')

@section('title', 'Editar Línea')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>líneas</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('lineas')}}">Líneas</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Editar línea</strong></b>
    </li>
</ol>
    
@endsection

@section('menuCatalogo') 
<li class="active">
    <a href="#"><i class="fa fa-wrench"></i> <span class="nav-label">Catálogos</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('marcas')}}">Marcas</a></li>
        <li class="active"><a href="{{route('lineas')}}">Líneas</a></li>
        <li><a href="{{route('unidades')}}">Unidades</a></li>
        <li><a href="{{route('industrias')}}">Industrias</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Editar línea de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('actualizarLinea', ['linea'=>$linea->id])}}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Línea:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="linea" value="{{old('linea', $linea->linea)}}"></div>
                    </div>
                    <div class="d-flex flex-row-reverse">
                        <button type="submit" class="btn btn-w-m btn-primary">Editar línea</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('lineas')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection