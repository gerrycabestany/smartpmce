@extends('layouts.template')

@section('title', 'Industrias')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Industrias</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('industrias')}}"><strong>Industrias</strong></a>
     </li>
</ol>
    
@endsection

@section('menuCatalogo') 
<li class="active">
    <a href="#"><i class="fa fa-wrench"></i> <span class="nav-label">Catálogos</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('marcas')}}">Marcas</a></li>
        <li><a href="{{route('lineas')}}">Líneas</a></li>
        <li><a href="{{route('unidades')}}">Unidades</a></li>
        <li class="active"><a href="{{route('industrias')}}">Industrias</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-1">
            <div class="text-center">
                <button class="btn btn-primary dim btn-large-dim" type="button" onclick="location.href='{{route('nuevaIndustria')}}'"><i class="fa">+</i></button>
            </div>
        </div>
    </div>
    <div class="row">
        @foreach ($industrias as $industria)
            <div class="col-lg-3">
                <div class="ibox">
                    <div class="ibox-content">
                        <h2 class="text-navy">
                            {{$industria->industria}}
                        </h2>
                        <small>
                            {{$industria->descripcion}}
                        </small>
                        <div class="d-flex flex-row-reverse">
                            <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarIndustria', ['industria'=>$industria->id])}}'"><i class="fa fa-edit"></i></button>
                            &nbsp;  
                            <form action="{{route('borrarIndustria', ['industria'=>$industria->id])}}" method="POST">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div> 
        @endforeach
    </div>
</div>

@endsection