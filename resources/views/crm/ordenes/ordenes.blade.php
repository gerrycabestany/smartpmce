@extends('layouts.template')

@section('title', 'Ordenes')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Ordenes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('ordenes')}}"><strong>Ordenes</strong></a>
     </li>
</ol>
    
@endsection


@section('menuOrdenes') 
<li  class="active">
    <a href="{{route('ordenes')}}"><i class="fa fa-book"></i> <span class="nav-label">Ordenes de compra</span></a>
</li>
@endsection
@section('menuOportunidades') @parent @endsection


@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a class="nav-link active show" data-toggle="tab" href="#tab-1">Cotizaciones</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-2">Ordenes Procesadas</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active show">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Cotización</th>
                                                <th>Fecha de Autorizacion</th>
                                                <th>Cliente</th>
                                                <th>Elaboro</th>
                                                <th>Autorizo</th>
                                                <th>Materiales</th>
                                                <th>Servicios</th>
                                            </tr>
                                        </thead>
                                        @foreach ($cotizacionesVenta as $cotizacion)
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="{{route('pdfCotizacion', ['cotizacion'=>$cotizacion->id])}}" class="btn btn-w-m btn-primary" target="_blank">Mostar Cotización</a> 
                                                    @if (($cotizacion->tieneServ)!=0)
                                                        <a href="{{route('ordenesServiciosCompra', ['cotizacion'=>$cotizacion->id])}}" class="btn btn-w-m btn-success">Generar orden de Servicios</a>
                                                    @endif
                                                    @if (($cotizacion->tieneMat)!=0)
                                                    <a href="{{route('ordenesMaterialesCompra', ['cotizacion'=>$cotizacion->id])}}" class="btn btn-w-m btn-success">Generar orden de Materiales</a>
                                                @endif
                                                </td>
                                                <td class="desc">
                                                    <h3>
                                                        <a href="#" class="text-navy">
                                                            {{$cotizacion->cotizacion}} v. {{$cotizacion->version}}
                                                        </a>
                                                    </h3>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->fechaAutorizacion}}
                                                    </h4> 
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->clientes->cliente}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->elaboro->nombre}} {{$cotizacion->elaboro->paterno}} {{$cotizacion->elaboro->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->autorizo->nombre}} {{$cotizacion->autorizo->paterno}} {{$cotizacion->autorizo->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        $ {{number_format($cotizacion->totalMateriales, 2, '.', ',')}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        $ {{number_format($cotizacion->totalServicios, 2, '.', ',')}}
                                                    </h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Tipo de orden</th>
                                                <th>Orden</th>
                                                <th>Total</th>
                                                <th>Proveedor</th>
                                            </tr>
                                        </thead>
                                        @foreach ($ordenes as $orden)
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if ($orden->material==1)
                                                        <a href="{{route('pdfOrdenMaterial', ['orden'=>$orden->id])}}" class="btn btn-w-m btn-primary" target="_blank">Materiales</a>
                                                        @else
                                                        <a href="{{route('pdfOrdenServicio', ['orden'=>$orden->id])}}" class="btn btn-w-m btn-success" target="_blank">Servicios</a>
                                                        @endif
                                                    </td>
                                                    <td class="desc">
                                                        <h3>
                                                            <a href="#" class="text-navy">
                                                                {{$orden->orden}}
                                                            </a>
                                                        </h3>
                                                    </td>
                                                    <td>
                                                        <h4>
                                                            $ {{number_format(($orden->total)*1.16, 2, '.', ',')}}
                                                        </h4>
                                                    </td>
                                                    <td>
                                                        <h4>
                                                            {{$orden->proveedor}}
                                                        </h4>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

@endsection