@extends('layouts.template')

@section('title', 'Ordenes de compra')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Ordenes de compra</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('ordenes')}}"><strong>Ordenes de compra</strong></a>
     </li>
     <li class="breadcrumb-item active">
            <b><strong>Nueva Orden de compra</strong></b>
        </li>
</ol>
    
@endsection

@section('menuOrdenes') 
<li  class="active">
    <a href="{{route('ordenes')}}"><i class="fa fa-book"></i> <span class="nav-label">Ordenes de compra</span></a>
</li>
@endsection
@section('menuOportunidades') @parent @endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12 ">
            <h5 id="titulo">Orden de Servicios</h5>
        </div>         
                
        <div class="col-md-9" style="margin-top:10px;">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Servicios de cotización</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre </th>
                                    <th>Cantidad</th>
                                    <th>Restantes</th>
                                    <th>Unidad</th>
                                    <th>Proveedor</th>
                                    <th>Moneda</th>
                                    <th>Costo</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($valoresCotizaciones as $valorCotizacion)
                                    @if ((($valorCotizacion->servicio)==1)&&(($valorCotizacion->restanteOrdenes)!=0))
                                        <tr>
                                            <form action="{{route('subirServicioOrdenes', ['valorCotizacion'=>$valorCotizacion->id])}}"  method="POST" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <td>{{$valorCotizacion->codigo}}</td>
                                                <td>{{$valorCotizacion->nombre}}</td>
                                                <td><input type="text" class="form-control" placeholder="Cantidad" name="cantidad"></td>
                                                <td>{{$valorCotizacion->restanteOrdenes}}</td>
                                                <td>{{$valorCotizacion->unidades->unidad}}</td>
                                                <td>
                                                   <select class="form-control m-b" name="proveedor">
                                                        @foreach (($valorCotizacion->proveedor) as $proveedor)        
                                                            <option value="{{$proveedor['id']}}" 
                                                                @if((old('proveedor')==($proveedor['id'])))
                                                                selected
                                                                @endif>{{$proveedor['proveedor']}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td>{{$valorCotizacion->monedas->moneda}}</td>
                                                <td>$ {{$valorCotizacion->costo}}</td>
                                                <td>
                                                    <div class="d-flex">
                                                        <button class="btn btn-success btn-circle" type="submit"><i class="fa fa-upload"></i></button>
                                                    </div>
                                                </td>
                                            </form>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Servicios de la orden</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre </th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                    <th>Proveedor</th>
                                    <th>Moneda</th>
                                    <th>Costo</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($valoresOrdenes as $valorOrden)
                                    @if (($valorOrden->servicio)==1)
                                        <tr>
                                           <td>{{$valorOrden->codigo}}</td>
                                            <td>{{$valorOrden->nombre}}</td>
                                            <td>{{$valorOrden->cantidad}}</td>
                                            <td>{{$valorOrden->unidades->unidad}}</td>
                                            <td>{{$valorOrden->proveedor}}</td>
                                            <td>{{$valorOrden->monedas->moneda}}</td>
                                            <td>$ {{$valorOrden->costo}}</td>
                                            <td>$ {{($valorOrden->costo)*($valorOrden->cantidad)}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>    
        </div>

        <div class="col-md-3" style="margin-top:10px;">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Ordenes de compra</h5>
                </div>
                <div class="ibox-content">
                    <a href="{{route('generarServiciosOrdenes', ['cotizacion'=>$valorCotizacion->cotizacion])}}" class="btn btn-w-m btn-success">Generar ordenes de compra individuales</a>
                </div>
            </div>
            <div class="ibox">
                    <div class="ibox-title">
                        <h5>Comentarios</h5>
                    </div>
                    <div class="ibox-content">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                {{$message}}
                            </div>
                        @endif
                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger">
                                {{$message}}
                            </div>
                        @endif
                    </div>
                </div>
        </div>
    </div>  
</div>

@endsection
