@extends('layouts.template')

@section('title', 'Contactos proveedores')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Contactos proveedores</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('contactosProveedores')}}"><strong>Contactos proveedores</strong></a>
     </li>
</ol>
    
@endsection

@section('menuProveedores') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Proveedores</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('proveedores')}}">Proveedores</a></li>
        <li class="active"><a href="{{route('contactosProveedores')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevoContactoProveedor')}}'">Agregar Contacto</button>
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevaDireccionProveedor')}}'">Agregar Direccion</button>
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevoTelefonoProveedor')}}'">Agregar Telefono</button>
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevoEmailProveedor')}}'">Agregar Email</button>
            </div>
        </div>
    </div>
    
        <div class="row"style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Contactos proveedores</h5>
                        <!--
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>-->
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Logo</th>
                                    <th>Proveedor </th>
                                    <th>Contacto </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($proveedores as $proveedor)
                                <tr>
                                    <td><img style="width:25px; height:auto;" src="{{asset('logos_proveedores/'.$proveedor->logo)}}" ></td>
                                    <td>{{$proveedor->proveedor}}</td>
                                    <td>
                                    <table>
                                            <tr>
                                                <th>Nombre Proveedor</th>
                                                <th>Direcciones </th>
                                                <th>Teléfonos </th>
                                                <th>Emails </th>
                                            </tr>
                                        @foreach ($contactosProveedores as $contactoProveedor) 
                                            @if (($contactoProveedor->proveedor_id)==($proveedor->id))
                                            <tr>
                                                <td>
                                                    <div class="d-flex">
                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarContactoProveedor', ['contactoProveedor'=>$contactoProveedor->id])}}'"><i class="fa fa-edit"></i></button>
                                                    &nbsp;  
                                                    <form action="{{route('borrarContactoProveedor', ['contactoProveedor'=>$contactoProveedor->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        {{method_field('DELETE')}}
                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                    </form>
                                                    &nbsp; {{$contactoProveedor->puesto}}: {{$contactoProveedor->nombre}} {{$contactoProveedor->paterno}} {{$contactoProveedor->materno}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($direccionesProveedores as $direccionProveedor)
                                                        @if (($direccionProveedor->contacto_id)==($contactoProveedor->id))
                                                            <li>
                                                                <div class="d-flex">
                                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarDireccionProveedor', ['direccionProveedor'=>$direccionProveedor->id])}}'"><i class="fa fa-edit"></i></button>
                                                                    &nbsp;  
                                                                    <form action="{{route('borrarDireccionProveedor', ['direccionProveedor'=>$direccionProveedor->id])}}" method="POST">
                                                                        {{csrf_field()}}
                                                                        {{method_field('DELETE')}}
                                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                    &nbsp; <p>{{$direccionProveedor->tipoDireccion}}: {{$direccionProveedor->calle}} ext:{{$direccionProveedor->ext}} int: {{$direccionProveedor->int}}, Col: {{$direccionProveedor->colonia}} Mun/Del: {{$direccionProveedor->municipio}}, Edo: {{$direccionProveedor->estado}}, CP: {{$direccionProveedor->cp}}</p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($telefonosProveedores as $telefonoProveedor)
                                                        @if (($telefonoProveedor->contacto_id)==($contactoProveedor->id))
                                                            <li>
                                                                <div class="d-flex">
                                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarTelefonoProveedor', ['telefonoProveedor'=>$telefonoProveedor->id])}}'"><i class="fa fa-edit"></i></button>
                                                                    &nbsp;  
                                                                    <form action="{{route('borrarTelefonoProveedor', ['telefonoProveedor'=>$telefonoProveedor->id])}}" method="POST">
                                                                        {{csrf_field()}}
                                                                        {{method_field('DELETE')}}
                                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                    &nbsp; <p>{{$telefonoProveedor->tipoTelefono}}: Pais:{{$telefonoProveedor->pais}} Área:{{$telefonoProveedor->area}} Teléfono:{{$telefonoProveedor->telefono}} </p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($emailsProveedores as $emailProveedor)
                                                        @if (($emailProveedor->contacto_id)==($contactoProveedor->id))
                                                            <li>
                                                                <div class="d-flex">
                                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarEmailProveedor', ['emailProveedor'=>$emailProveedor->id])}}'"><i class="fa fa-edit"></i></button>
                                                                    &nbsp;  
                                                                    <form action="{{route('borrarEmailProveedor', ['emailProveedor'=>$emailProveedor->id])}}" method="POST">
                                                                        {{csrf_field()}}
                                                                        {{method_field('DELETE')}}
                                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                    &nbsp; <p>{{$emailProveedor->tipoEmail}}: {{$emailProveedor->email}}</p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>

@endsection