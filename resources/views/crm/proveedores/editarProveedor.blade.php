@extends('layouts.template')

@section('title', 'Editar Proveedor')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Proveedores</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('proveedores')}}">Proveedores</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Editar Proveedor</strong></b>
    </li>
</ol>
    
@endsection

@section('menuProveedores') 
<li class="active">
    <a href="#"><i class="fa fa-book"></i> <span class="nav-label">Proveedores</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="active"><a href="{{route('proveedores')}}">Proveedores</a></li>
        <li><a href="{{route('contactosProveedores')}}">Contactos</a></li>
    </ul>
</li>
@endsection


@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Editar Proveedor de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('actualizarProveedor', ['proveedor'=>$proveedor->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Proveedor:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="proveedor" value="{{old('proveedor', $proveedor->proveedor)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">RFC:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="rfc" value="{{old('rfc', $proveedor->rfc)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Razón Social:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="razonSocial" value="{{old('razonSocial', $proveedor->razonSocial)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Banco:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="banco" value="{{old('banco', $proveedor->banco)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Clabe:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="clabe" value="{{old('clabe', $proveedor->clabe)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Crédito:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="credito_id">
                                @foreach ($creditos as $credito)
                                <option value="{{$credito->id}}" 
                                    @if((old('credito_id')==null) && (($proveedor->credito_id)==($credito->id))) 
                                    selected
                                    @elseif((old('credito_id')==($credito->id)))
                                    selected
                                    @endif>{{$credito->tiempo}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="custom-file">
                        <input id="logo" type="file" class="custom-file-input" name="logo" value="{{old('logo')}}">
                    <label for="logo" class="custom-file-label">{{$proveedor->logo}}</label>
                    </div> 
                    
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Editar</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('proveedores')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection