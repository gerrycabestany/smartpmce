@extends('layouts.template')

@section('title', 'Editar Contacto')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Proveedores</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('proveedores')}}">Proveedores</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Editar contacto proveedor</strong></b>
    </li>
</ol>
    
@endsection


@section('menuProveedores') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Proveedores</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('proveedores')}}">Proveedores</a></li>
        <li class="active"><a href="{{route('contactosProveedores')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Editar contacto en proveedores de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('actualizarContactoProveedor', ['contactoProveedor'=>$contactoProveedor->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Nombre:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="nombre" value="{{old('nombre', $contactoProveedor->nombre)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Paterno:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="paterno" value="{{old('paterno', $contactoProveedor->paterno)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Materno:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="materno" value="{{old('materno', $contactoProveedor->materno)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Puesto:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="puesto" value="{{old('puesto', $contactoProveedor->puesto)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Proveedores:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="proveedor_id">
                                    @foreach ($proveedores as $proveedor)
                                    <option value="{{$proveedor->id}}" 
                                        @if((old('proveedor_id')==null) && (($contactoProveedor->proveedor_id)==($proveedor->id))) 
                                        selected
                                        @elseif((old('proveedor_id')==($proveedor->id)))
                                        selected
                                        @endif>{{$proveedor->proveedor}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Editar</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('contactosProveedores')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection