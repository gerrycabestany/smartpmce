@extends('layouts.template')

@section('title', 'Editar Email')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Proveedores</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('proveedores')}}">Proveedores</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Editar email proveedor</strong></b>
    </li>
</ol>
    
@endsection

@section('menuProveedores') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Proveedores</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('proveedores')}}">Proveedores</a></li>
        <li class="active"><a href="{{route('contactosProveedores')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Editar email en proveedores de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('actualizarEmailProveedor', ['emailProveedor'=>$emailProveedor->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Tipo de Email:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tipoEmail" value="{{old('tipoEmail', $emailProveedor->tipoEmail)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Email:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="email" value="{{old('email', $emailProveedor->email)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contacto:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="contacto_id">
                                    @foreach ($contactosProveedores as $contacto)
                                    <option value="{{$contacto->id}}" 
                                        @if((old('contacto_id')==null) && (($emailProveedor->contacto_id)==($contacto->id))) 
                                        selected
                                        @elseif((old('contacto_id')==($contacto->id)))
                                        selected
                                        @endif>{{$contacto->nombre}} {{$contacto->paterno}} {{$contacto->materno}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Editar</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('contactosProveedores')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection