@extends('layouts.template')

@section('title', 'Nueva Direccion')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Clientes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('clientes')}}">Clientes</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Nueva direccion cliente</strong></b>
    </li>
</ol>
    
@endsection

@section('menuClientes') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('clientes')}}">Clientes</a></li>
        <li class="active"><a href="{{route('contactosClientes')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Nueva direccion en clientes de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('crearDireccionCliente')}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Tipo de Dirección:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tipoDireccion" value="{{old('tipoDireccion')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Calle:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="calle" value="{{old('calle')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Exterior:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="ext" value="{{old('ext')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Interior:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="int" value="{{old('int')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Colonia:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="colonia" value="{{old('colonia')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Municipio:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="municipio" value="{{old('municipio')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Estado:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="estado" value="{{old('estado')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Pais:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="pais" value="{{old('pais')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Código postal:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="cp" value="{{old('cp')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contacto:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="contacto_id">
                                    @foreach ($contactosClientes as $contacto)
                                        <option value="{{$contacto->id}}" @if((old('contacto_id')==($contacto->id))) selected @endif>{{$contacto->nombre}} {{$contacto->paterno}} {{$contacto->materno}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Crear</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('contactosClientes')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection