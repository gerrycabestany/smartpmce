@extends('layouts.template')

@section('title', 'Contactos clientes')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Contactos clientes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('contactosClientes')}}"><strong>Contactos clientes</strong></a>
     </li>
</ol>
    
@endsection

@section('menuClientes') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('clientes')}}">Clientes</a></li>
        <li class="active"><a href="{{route('contactosClientes')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevoContactoCliente')}}'">Agregar Contacto</button>
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevaDireccionCliente')}}'">Agregar Direccion</button>
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevoTelefonoCliente')}}'">Agregar Telefono</button>
                <button class="btn btn-w-m btn-primary" type="button" onclick="location.href='{{route('nuevoEmailCliente')}}'">Agregar Email</button>
            </div>
        </div>
    </div>
    
        <div class="row"style="margin-top: 15px;">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Contactos clientes</h5>
                        <!--
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>-->
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Logo</th>
                                    <th>Cliente </th>
                                    <th>Contacto </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($clientes as $cliente)
                                <tr>
                                    <td><img style="width:25px; height:auto;" src="{{asset('logos_clientes/'.$cliente->logo)}}" ></td>
                                    <td>{{$cliente->cliente}}</td>
                                    <td>
                                    <table>
                                            <tr>
                                                <th>Nombre Cliente</th>
                                                <th>Direcciones </th>
                                                <th>Teléfonos </th>
                                                <th>Emails </th>
                                            </tr>
                                        @foreach ($contactosClientes as $contactoCliente) 
                                            @if (($contactoCliente->cliente_id)==($cliente->id))
                                            <tr>
                                                <td>
                                                    <div class="d-flex">
                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarContactoCliente', ['contactoCliente'=>$contactoCliente->id])}}'"><i class="fa fa-edit"></i></button>
                                                    &nbsp;  
                                                    <form action="{{route('borrarContactoCliente', ['contactoCliente'=>$contactoCliente->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        {{method_field('DELETE')}}
                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                    </form>
                                                    &nbsp; {{$contactoCliente->puesto}}: {{$contactoCliente->nombre}} {{$contactoCliente->paterno}} {{$contactoCliente->materno}}
                                                    </div>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($direccionesClientes as $direccionCliente)
                                                        @if (($direccionCliente->contacto_id)==($contactoCliente->id))
                                                            <li>
                                                                <div class="d-flex">
                                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarDireccionCliente', ['direccionCliente'=>$direccionCliente->id])}}'"><i class="fa fa-edit"></i></button>
                                                                    &nbsp;  
                                                                    <form action="{{route('borrarDireccionCliente', ['direccionCliente'=>$direccionCliente->id])}}" method="POST">
                                                                        {{csrf_field()}}
                                                                        {{method_field('DELETE')}}
                                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                    &nbsp; <p>{{$direccionCliente->tipoDireccion}}: {{$direccionCliente->calle}} ext:{{$direccionCliente->ext}} int: {{$direccionCliente->int}}, Col: {{$direccionCliente->colonia}} Mun/Del: {{$direccionCliente->municipio}}, Edo: {{$direccionCliente->estado}}, CP: {{$direccionCliente->cp}}</p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($telefonosClientes as $telefonoCliente)
                                                        @if (($telefonoCliente->contacto_id)==($contactoCliente->id))
                                                            <li>
                                                                <div class="d-flex">
                                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarTelefonoCliente', ['telefonoCliente'=>$telefonoCliente->id])}}'"><i class="fa fa-edit"></i></button>
                                                                    &nbsp;  
                                                                    <form action="{{route('borrarTelefonoCliente', ['telefonoCliente'=>$telefonoCliente->id])}}" method="POST">
                                                                        {{csrf_field()}}
                                                                        {{method_field('DELETE')}}
                                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                    &nbsp; <p>{{$telefonoCliente->tipoTelefono}}: Pais:{{$telefonoCliente->pais}} Área:{{$telefonoCliente->area}} Teléfono:{{$telefonoCliente->telefono}} </p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                                <td>
                                                    <ul>
                                                        @foreach ($emailsClientes as $emailCliente)
                                                        @if (($emailCliente->contacto_id)==($contactoCliente->id))
                                                            <li>
                                                                <div class="d-flex">
                                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarEmailCliente', ['emailCliente'=>$emailCliente->id])}}'"><i class="fa fa-edit"></i></button>
                                                                    &nbsp;  
                                                                    <form action="{{route('borrarEmailCliente', ['emailCliente'=>$emailCliente->id])}}" method="POST">
                                                                        {{csrf_field()}}
                                                                        {{method_field('DELETE')}}
                                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                                    </form>
                                                                    &nbsp; <p>{{$emailCliente->tipoEmail}}: {{$emailCliente->email}}</p>
                                                                </div>
                                                            </li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    </table>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>

@endsection