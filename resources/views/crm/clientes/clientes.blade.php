@extends('layouts.template')

@section('title', 'Clientes')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Clientes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('clientes')}}"><strong>Clientes</strong></a>
     </li>
</ol>
    
@endsection

@section('menuClientes') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li class="active"><a href="{{route('clientes')}}">Clientes</a></li>
        <li><a href="{{route('contactosClientes')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-1">
            <div class="text-center">
                <button class="btn btn-primary dim btn-large-dim" type="button" onclick="location.href='{{route('nuevoCliente')}}'"><i class="fa">+</i></button>
            </div>
        </div>
    </div>
    
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Todos los clientes</h5>
                        <!--
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>-->
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Logo</th>
                                    <th>Cliente </th>
                                    <th>Razón social </th>
                                    <th>RFC</th>
                                    <th>Industria</th>
                                    <th>Crédito</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($clientes as $cliente)
                                <tr>
                                    <td>
                                            <div class="d-flex">
                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarCliente', ['cliente'=>$cliente->id])}}'"><i class="fa fa-edit"></i></button>
                                                    &nbsp;  
                                                    <form action="{{route('borrarCliente', ['cliente'=>$cliente->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        {{method_field('DELETE')}}
                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                    </form>
                                                </div>
                                    </td>
                                    <td><img style="width:25px; height:auto;" src="{{asset('logos_clientes/'.$cliente->logo)}}" ></td>
                                    <td>{{$cliente->cliente}}</td>
                                    <td>{{$cliente->razonSocial}}</td>
                                    <td>{{$cliente->rfc}}</td>
                                    <td>{{$cliente->industrias->industria}}</td>
                                    <td>{{$cliente->creditos->tiempo}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</div>

@endsection