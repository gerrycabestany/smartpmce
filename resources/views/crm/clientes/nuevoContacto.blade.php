@extends('layouts.template')

@section('title', 'Nuevo Contacto Cliente')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Clientes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('clientes')}}">Clientes</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>nuevo contacto cliente</strong></b>
    </li>
</ol>
    
@endsection

@section('menuClientes') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('clientes')}}">Clientes</a></li>
        <li class="active"><a href="{{route('contactosClientes')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Nuevo contacto en clientes de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('crearContactoCliente')}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Nombre:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="nombre" value="{{old('nombre')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Paterno:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="paterno" value="{{old('paterno')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Materno:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="materno" value="{{old('materno')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Puesto:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="puesto" value="{{old('puesto')}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Clientes:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="cliente_id">
                                @foreach ($clientes as $cliente)
                                    <option value="{{$cliente->id}}" @if(old('cliente_id') == ($cliente->id)) selected @endif>{{$cliente->cliente}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Crear</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('contactosClientes')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection