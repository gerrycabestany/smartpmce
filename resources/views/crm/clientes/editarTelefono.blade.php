@extends('layouts.template')

@section('title', 'Editar Teléfono')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Clientes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('clientes')}}">Clientes</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Editar telefono cliente</strong></b>
    </li>
</ol>
    
@endsection

@section('menuClientes') 
<li class="active">
    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
    <ul class="nav nav-second-level collapse">
        <li><a href="{{route('clientes')}}">Clientes</a></li>
        <li class="active"><a href="{{route('contactosClientes')}}">Contactos</a></li>
    </ul>
</li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Editar teléfono en clientes de la base de datos</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('actualizarTelefonoCliente', ['telefonoCliente'=>$telefonoCliente->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Tipo de Teléfono:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="tipoTelefono" value="{{old('tipoTelefono', $telefonoCliente->tipoTelefono)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Código Pais:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="pais" value="{{old('pais', $telefonoCliente->pais)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Código Área:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="area" value="{{old('area', $telefonoCliente->area)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Teléfono:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="telefono" value="{{old('telefono', $telefonoCliente->telefono)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contacto:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="contacto_id">
                                    @foreach ($contactosClientes as $contacto)
                                    <option value="{{$contacto->id}}" 
                                        @if((old('contacto_id')==null) && (($telefonoCliente->contacto_id)==($contacto->id))) 
                                        selected
                                        @elseif((old('contacto_id')==($contacto->id)))
                                        selected
                                        @endif>{{$contacto->nombre}} {{$contacto->paterno}} {{$contacto->materno}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Editar</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('contactosClientes')}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection