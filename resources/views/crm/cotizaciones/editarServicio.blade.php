@extends('layouts.template')

@section('title', 'Editar Servicio Cotizacion')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Cotizaciones</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('cotizaciones')}}">Cotizaciones</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Editar Servicio cotizacion</strong></b>
    </li>
</ol>
    
@endsection

@section('menuCotizaciones') 
    <li class="active">
        <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
    </li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Editar servicio de la cotizacion</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('actualizarServicioCotizacion', ['valorCotizacion'=>$valorCotizacion->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Nombre:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="nombre" value="{{old('nombre', $valorCotizacion->nombre)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Codigo:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="codigo" value="{{old('codigo', $valorCotizacion->codigo)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Marca:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="marca_id">
                                @foreach ($marcas as $marca)
                                    <option value="{{$marca->id}}" 
                                        @if((old('marca_id')==null) && (($valorCotizacion->marca_id)==($marca->id))) 
                                        selected
                                        @elseif((old('marca_id')==($marca->id)))
                                        selected
                                        @endif>{{$marca->marca}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Línea:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="linea_id">
                                @foreach ($lineas as $linea)
                                <option value="{{$linea->id}}" 
                                    @if((old('linea_id')==null) && (($valorCotizacion->linea_id)==($linea->id))) 
                                    selected
                                    @elseif((old('linea_id')==($linea->id)))
                                    selected
                                    @endif>{{$linea->linea}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Costo:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="costo" value="{{old('costo', $valorCotizacion->costo)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Cantidad:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="cantidad" value="{{old('cantidad',$valorCotizacion->cantidad)}}"></div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Unidad:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="unidad_id">
                                @foreach ($unidades as $unidad)
                                <option value="{{$unidad->id}}" 
                                    @if((old('unidad_id')==null) && (($valorCotizacion->unidad_id)==($unidad->id))) 
                                    selected
                                    @elseif((old('unidad_id')==($unidad->id)))
                                    selected
                                    @endif>{{$unidad->unidad}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Proveedores:</label>
                        <input type="hidden" class="form-control" name="proveedor" id="proveedores" value="">
                        <div>
                            @foreach ($proveedores as $proveedor)
                                <div class="form-control">
                                    @if (!in_array(($proveedor->id),$numeroProveedores))
                                        <p>{{$proveedor->proveedor}}
                                        <button class="btn btn-primary btn-circle" type="button" id="cond{{$proveedor->id}}" onclick="agregarProveedor({{$proveedor->id}})">+</button></p>
                                    @else
                                        <p>{{$proveedor->proveedor}}
                                        <button class="btn btn-danger btn-circle" type="button" id="cond{{$proveedor->id}}" onclick="quitarProveedor({{$proveedor->id}})">-</button></p>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Moneda:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="moneda_id">
                                @foreach ($monedas as $moneda)
                                <option value="{{$moneda->id}}" 
                                    @if((old('moneda_id')==null) && (($valorCotizacion->moneda_id)==($moneda->id))) 
                                    selected
                                    @elseif((old('moneda_id')==($moneda->id)))
                                    selected
                                    @endif>{{$moneda->moneda}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-primary">Editar</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('regresarEditarCotizacionActual', ['cotizacion'=>Session::get('noCotizacion')])}}'">Cancelar</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection


@section('script')
    <!--Solo para condiciones comerciales-->
<script>
    //prov son las condiciones
    var prov = {{json_encode($numeroProveedores)}};
    $(function() {
        $('#proveedores').attr('value', prov);
    });
    function agregarProveedor(val){
        prov.push(val);
        id="#cond"+val;
        $(id).attr('class', 'btn btn-danger btn-circle');
        $(id).attr('onclick', 'quitarProveedor('+val+')');
        $(id).html('-');     
        $('#proveedores').attr('value', prov);
    }
    function quitarProveedor(val){
        for( var i = 0; i <= prov.length-1; i++){ 
            if ( prov[i] === val) {
                prov.splice(i, 1); 
           }
        }
        id="#cond"+val;
        $(id).attr('class', 'btn btn-success btn-circle');
        $(id).attr('onclick', 'agregarProveedor('+val+')');
        $(id).html('+');
        $('#proveedores').attr('value', prov);
    }
    </script>
@endsection