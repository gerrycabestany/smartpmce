@extends('layouts.template')

@section('title', 'Generar venta de cotizacion')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Cotizacion</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('cotizaciones')}}">Cotizaciones</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Generar venta de cotización</strong></b>
    </li>
</ol>
    
@endsection

@section('menuCotizaciones') 
    <li class="active">
        <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
    </li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Generación de la venta</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('finalizarVentaCotizacion', ['cotizacion'=>$cotizacion->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Contacto:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="contactoCliente_id" id="contactoCliente_id">
                                @foreach ($contactosClientes as $contactoCliente)
                                    <option value="{{$contactoCliente->id}}" @if(old('contactoCliente_id') == ($contactoCliente->id)) selected @endif>{{$contactoCliente->nombre}} {{$contactoCliente->paterno}} {{$contactoCliente->materno}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Dirección:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="direccionCliente_id" id="direccionCliente_id">
                                @foreach ($direccionesClientes as $direccionCliente)
                                    <option value="{{$direccionCliente->id}}" @if(old('direccionCliente_id') == ($direccionCliente->id)) selected @endif>{{$direccionCliente->tipodireccion}}: {{$direccionCliente->calle}} {{$direccionCliente->ext}}, {{$direccionCliente->int}}, Col: {{$direccionCliente->colonia}} Mun/Del: {{$direccionCliente->municipio}} - {{$direccionCliente->estado}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Email:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="emailCliente_id" id="emailCliente_id">
                                @foreach ($emailsClientes as $emailCliente)
                                    <option value="{{$emailCliente->id}}" @if(old('emailCliente_id') == ($emailCliente->id)) selected @endif>{{$emailCliente->tipoEmail}}: {{$emailCliente->email}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Teléfono:</label>
                        <div class="col-sm-10">
                            <select class="form-control m-b" name="telefonoCliente_id" id="telefonoCliente_id">
                                @foreach ($telefonosClientes as $telefonoCliente)
                                    <option value="{{$telefonoCliente->id}}" @if(old('telefonoCliente_id') == ($telefonoCliente->id)) selected @endif>{{$telefonoCliente->tipoTelefono}}: {{$telefonoCliente->pais}} {{$telefonoCliente->area}} {{$telefonoCliente->telefono}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                    <button type="submit" class="btn btn-w-m btn-primary">Generar venta</button>
                    &nbsp;
                    <button type="button" class="btn btn-w-m btn-success" onclick="location.href='{{route('cotizaciones')}}'">< Regresar</button>
                </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection

@section('script')
<script>
$("#contactoCliente_id").change(function(){
    var datos = {
        contacto_id: $("#contactoCliente_id").val()
    }
    $.ajax({
		type:'POST',
        url: '{{route('datosVenta')}}',  
        dataType: 'json',  
        contentType: 'application/json',              
        data:JSON.stringify(datos),
		success:function(data){
            var noEmails=data.emails.length;
            var noDirecciones=data.direcciones.length;
            var noTelefonos=data.telefonos.length;
            var email="";
            var direccion="";
            var telefono="";
            if (noEmails===0){
                email+="<option value='ninguno' >Este contacto no tiene emails</option>";
                $("#emailCliente_id").html(email);
            }else{
                for (i = 0; i < noEmails; i++) {
                    email+="<option value="+data.emails[i].id+" >"+data.emails[i].tipoEmail+": "+data.emails[i].email+"</option>";
                }
                $("#emailCliente_id").html(email);
            }

            if (noDirecciones===0){
                direccion+="<option value='ninguno' >Este contacto no tiene direcciones</option>";
                $("#direccionCliente_id").html(direccion);
            }else{
                for (i = 0; i < noDirecciones; i++) {
                    direccion+="<option value="+data.direcciones[i].id+" >"+data.direcciones[i].tipoDireccion+": "+data.direcciones[i].calle+" "+data.direcciones[i].ext+", "+data.direcciones[i].int+", Col: "+data.direcciones[i].colonia+" Mun/Del: "+data.direcciones[i].municipio+" - "+data.direcciones[i].estado+"</option>";
                }
                $("#direccionCliente_id").html(direccion);
            }

            if (noTelefonos===0){
                telefono+="<option value='ninguno' >Este contacto no tiene telefonos</option>";
                $("#telefonoCliente_id").html(telefono);
            }else{
                for (i = 0; i < noTelefonos; i++) {
                    telefono+="<option value="+data.telefonos[i].id+" >"+data.telefonos[i].tipoTelefono+": "+data.telefonos[i].pais+" "+data.telefonos[i].area+" "+data.telefonos[i].telefono+"</option>";
                }
                $("#telefonoCliente_id").html(telefono);
            }
         }
    });
});
</script>

@endsection