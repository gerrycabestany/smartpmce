@extends('layouts.template')

@section('title', 'Venta cotizacion')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Clientes</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('cotizaciones')}}">Cotizaciones</a>
        </li>
    <li class="breadcrumb-item active">
        <b><strong>Generación de venta</strong></b>
    </li>
</ol>
    
@endsection

@section('menuCotizaciones') 
    <li class="active">
        <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
    </li>
@endsection

@section('content')
<div class="row" style="margin-top:15px;">
    <div class="col-lg-8">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Generación de venta para cotización</h5>
            </div>
            <div class="ibox-content">
                <form action="{{route('finalizarCancelacionCotizacion', ['cotizacion'=>$cotizacion->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group  row"><label class="col-sm-2 col-form-label">Motivo Cancelación:</label>
                        <div class="col-sm-10"><input type="text" class="form-control" name="motivo" value="{{old('motivo')}}"></div>
                    </div>
                    
                    <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                        <button type="submit" class="btn btn-w-m btn-danger">Cancelar cotizacion</button>
                        &nbsp;
                        <button type="button" class="btn btn-w-m btn-success" onclick="location.href='{{route('cotizaciones')}}'">< Ir a cotizaciones</button>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>    

@endsection