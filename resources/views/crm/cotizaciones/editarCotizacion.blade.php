@extends('layouts.template')

@section('title', 'Cotizaciones')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Cotizaciones</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('cotizaciones')}}"><strong>Cotizaciones</strong></a>
     </li>
     <li class="breadcrumb-item active">
            <b><strong>Nueva Cotización</strong></b>
        </li>
</ol>
    
@endsection

@section('menuCotizaciones') 
    <li class="active">
        <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
    </li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
         <div class="col-md-9 ">
            <button type="button" class="btn btn-primary btn-lg" onclick="location.href='{{route('agregarMaterialCotizacion')}}'">Agregar materiales</button>
            <button type="button" class="btn btn-primary btn-lg" onclick="location.href='{{route('agregarServicioCotizacion')}}'">Agregar servicios</button>
        </div>       
                
        <div class="col-md-9" style="margin-top:10px;">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Materiales</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Código</th>
                                    <th>Nombre </th>
                                    <th>Marca</th>
                                    <th>Moneda</th>
                                    <th>Costo</th>
                                    <th>Margen de utilidad</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($valoresCotizaciones as $valorCotizacion)
                                    @if (($valorCotizacion->material)==1)
                                        <tr>
                                            <td>
                                                <div class="d-flex flex-row">
                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarMaterialCotizacion', ['valorCotizacion'=>$valorCotizacion->id])}}'"><i class="fa fa-edit"></i></button>
                                                    &nbsp;  
                                                    <form action="{{route('borrarMaterialCotizacion', ['valorCotizacion'=>$valorCotizacion->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        {{method_field('DELETE')}}
                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                            <td>
                                                {{$valorCotizacion->codigo}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->nombre}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->marcas->marca}}   
                                            </td>
                                            <td>
                                                {{$valorCotizacion->monedas->moneda}}
                                            </td>
                                            <td>
                                                $ {{number_format($valorCotizacion->costo, 2, '.', ',')}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->margen}} %
                                            </td>
                                            <td>
                                                $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100), 2, '.', ',')}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->cantidad}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->unidades->unidad}}
                                            </td>
                                            <td>
                                                <h4>
                                                    $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100)*($valorCotizacion->cantidad), 2, '.', ',')}}
                                            </h4>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Servicios</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Código</th>
                                    <th>Nombre </th>
                                    <th>Marca</th>
                                    <th>Moneda</th>
                                    <th>Costo</th>
                                    <th>Margen de utilidad</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                    <th>Unidad</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($valoresCotizaciones as $valorCotizacion)
                                    @if (($valorCotizacion->servicio)==1)
                                        <tr>
                                            <td>
                                                <div class="d-flex flex-row">
                                                    <button class="btn btn-success btn-circle" type="button" onclick="location.href='{{route('editarServicioCotizacion', ['valorCotizacion'=>$valorCotizacion->id])}}'"><i class="fa fa-edit"></i></button>
                                                    &nbsp;  
                                                    <form action="{{route('borrarServicioCotizacion', ['valorCotizacion'=>$valorCotizacion->id])}}" method="POST">
                                                        {{csrf_field()}}
                                                        {{method_field('DELETE')}}
                                                        <button class="btn btn-danger btn-circle" type="submit" ><i class="fa fa-times"></i></button>
                                                    </form>
                                                </div>
                                            </td>
                                            <td>
                                                {{$valorCotizacion->codigo}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->nombre}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->marcas->marca}}   
                                            </td>
                                            <td>
                                                {{$valorCotizacion->monedas->moneda}}
                                            </td>
                                            <td>
                                                $ {{number_format($valorCotizacion->costo, 2, '.', ',')}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->margen}} %
                                            </td>
                                            <td>
                                                $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100), 2, '.', ',')}}
                                            <td>
                                                {{$valorCotizacion->cantidad}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->unidades->unidad}}
                                            </td>
                                            <td>
                                                <h4>
                                                    $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100)*($valorCotizacion->cantidad), 2, '.', ',')}}
                                                </h4>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>    
        </div>

        <div class="col-md-3" style="margin-top:10px;">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Cotización</h5>
                </div>
                <div class="ibox-content">
                    <h2 class="font-bold">
                        {{$noCotizacion}}
                    </h2>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Materiales</h5>
                </div>
                <div class="ibox-content">
                    <h2 class="font-bold">
                        Total: <br>{!!$totalMateriales!!}
                    </h2>
                </div>
            </div>
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Servicios</h5>
                </div>
                <div class="ibox-content">
                    <h2 class="font-bold">
                            Total: <br>{!!$totalServicios!!}
                    </h2>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top:15px;">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">    
                    <h5>Formulario</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <form action="{{route('actualizarCotizacion', ['cotizacion'=>$cotizacion->id])}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                            
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Cliente:</label>
                             <div class="col-sm-10">
                                <select class="form-control m-b" name="cliente_id">
                                    @foreach ($clientes as $cliente)
                                        <option value="{{$cliente->id}}" 
                                            @if((old('cliente_id')==null) && (($cotizacion->cliente_id)==($cliente->id))) 
                                            selected
                                            @elseif((old('cliente_id')==($cliente->id)))
                                            selected
                                            @endif>{{$cliente->cliente}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Elaboro:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="elaboro_id">
                                    @foreach ($admins as $admin)
                                        <option value="{{$admin->id}}" 
                                            @if((old('elaboro_id')==null) && (($cotizacion->elaboro_id)==($admin->id))) 
                                            selected
                                            @elseif((old('elaboro_id')==($admin->id)))
                                            selected
                                            @endif>{{$admin->nombre}} {{$admin->paterno}} {{$admin->materno}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Autorizara:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="autorizo_id">
                                    @foreach ($admins as $admin)
                                    <option value="{{$admin->id}}" 
                                        @if((old('autorizo_id')==null) && (($cotizacion->autorizo_id)==($admin->id))) 
                                        selected
                                        @elseif((old('autorizo_id')==($admin->id)))
                                        selected
                                        @endif>{{$admin->nombre}} {{$admin->paterno}} {{$admin->materno}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Condiciones comerciales:</label>
                            <input type="hidden" class="form-control" name="condicionesComerciales" id="condicionesComerciales" value="">
                            <div class="col-sm-10">
                                @foreach ($condicionesComerciales as $condicionComercial)
                                    <div class="form-control">
                                        @if (!in_array(($condicionComercial->orden),$condiciones))
                                            <p>{{$condicionComercial->texto}}</p><br>
                                            <button class="btn btn-primary btn-circle" type="button" id="cond{{$condicionComercial->orden}}" onclick="agregarCondicionComercial({{$condicionComercial->orden}})">+</button>
                                        @else
                                            <p>{{$condicionComercial->texto}}</p><br>
                                            <button class="btn btn-danger btn-circle" type="button" id="cond{{$condicionComercial->orden}}" onclick="quitarCondicionComercial({{$condicionComercial->orden}})">-</button>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Condiciones de pago:</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="condicionesPagos" value="{{old('condicionesPagos', $cotizacion->condicionesPagos)}}"></div>
                        </div>
                        <div class="form-group  row"><label class="col-sm-2 col-form-label">Comentarios:</label>
                            <div class="col-sm-10"><input type="text" class="form-control" name="comentarios" value="{{old('comentarios', $cotizacion->comentarios)}}"></div>
                        </div>   
                        <div class="d-flex flex-row-reverse" style="margin-top:15px; ">
                            <button type="submit" class="btn btn-w-m btn-primary">Agregar</button>
                            &nbsp;
                            <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('cancelarCotizacion', $cotizacion->id)}}'">Cancelar cotización</button>
                        </div>
                    </form>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>    
</div>

@endsection


@section('script')
    <!--Solo para condiciones comerciales-->
<script>
    //prov son las condiciones
    var prov = {{json_encode($condiciones)}};
    $(function() {
        $('#condicionesComerciales').attr('value', prov);
    });
    function agregarCondicionComercial(val){
        prov.push(val);
        id="#cond"+val;
        $(id).attr('class', 'btn btn-danger btn-circle');
        $(id).attr('onclick', 'quitarCondicionComercial('+val+')');
        $(id).html('-');     
        $('#condicionesComerciales').attr('value', prov);
    }
    function quitarCondicionComercial(val){
        for( var i = 0; i <= prov.length-1; i++){ 
            if ( prov[i] === val) {
                prov.splice(i, 1); 
           }
        }
        id="#cond"+val;
        $(id).attr('class', 'btn btn-success btn-circle');
        $(id).attr('onclick', 'agregarCondicionComercial('+val+')');
        $(id).html('+');
        $('#condicionesComerciales').attr('value', prov);
    }
    </script>
@endsection