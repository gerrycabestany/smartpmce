@extends('layouts.template')

@section('title', 'Agregar Material a cotizacion')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Cotizaciones</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item">
            <a href="{{route('cotizaciones')}}"><strong>Cotizaciones</strong></a>
     </li>
     <li class="breadcrumb-item active">
         <strong>Agregar material a cotizacion</strong>
     </li>
</ol>
@endsection


@section('menuCotizaciones') 
    <li class="active">
        <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
    </li>
@endsection

@section('buscador')
    {{Form::open(['route'=>'agregarMaterialCotizacion','method'=>'GET','class'=>'navbar-form-custom'])}}  
        <div class="form-group">
            {{Form::text('texto', old('texto') , ['class'=>'form-control', 'placeholder'=>'Buscar material', 'id'=>'top-search'])}}
        </div>    
    {{Form::close()}}
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <button type="button" class="btn btn-w-m btn-success" onclick="location.href='{{route('regresarEditarCotizacionActual', ['cotizacion'=>Session::get('noCotizacion')])}}'">< Regresar</button>
            <button type="button" class="btn btn-w-m btn-primary" onclick="location.href='{{route('nuevoMaterial')}}'">+ Agergar material a catalogo</button>
            <button type="button" class="btn btn-w-m btn-primary" onclick="location.href='{{route('materialFueraCatalogo')}}'">+ Agergar material fuera de catalogo</button>
        </div>
    </div>
        <div class="row" style="margin-top:15px;">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Materiales</h5>
                        <!--
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>-->
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Margen de utilidad</th>
                                    <th>Tiempo de entrega</th>
                                    <th>Código</th>
                                    <th>Nombre </th>
                                    <th>Marca</th>
                                    <th>Línea</th>
                                    <th>Costo</th>
                                    <th>Moneda</th>
                                    <th>Unidad</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($materiales as $material)
                                <tr>
                                <form action="{{route('subirMaterialCotizacion', ['material'=>$material->id])}}"  method="POST" enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <td><input type="text" class="form-control" placeholder="Cantidad" name="cantidad"></td>
                                    <td><input type="text" class="form-control" placeholder="Margen" name="margen" value="22"></td>
                                    <td><input type="text" class="form-control"  ></td>
                                    <td>{{$material->codigo}}</td>
                                    <td>{{$material->nombre}}</td>
                                    <td>{{$material->marcas->marca}}</td>
                                    <td>{{$material->lineas->linea}}</td>
                                    <td>{{$material->costo}}</td>
                                    <td>{{$material->monedas->moneda}}</td>
                                    <td>{{$material->unidades->unidad}}</td>
                                    <td>
                                        <div class="d-flex">
                                            <button class="btn btn-success btn-circle" type="submit"><i class="fa fa-upload"></i></button>
                                        </div>
                                    </td>
                                </form>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$materiales->render()}}
                    </div>
                </div>
            </div>
        </div>
</div>

@endsection