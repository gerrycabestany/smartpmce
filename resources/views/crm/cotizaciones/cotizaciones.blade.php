@extends('layouts.template')

@section('title', 'Cotizaciones')

@section('user', 'Ulisses')

@section('breadcrumb')
<h2>Cotizaciones</h2>
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="{{route('inicio')}}">Inicio</a>
    </li>
    <li class="breadcrumb-item active">
            <a href="{{route('cotizaciones')}}"><strong>Cotizaciones</strong></a>
     </li>
</ol>
    
@endsection

@section('menuCotizaciones') 
    <li class="active">
        <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
    </li>
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    @if (Session::get('perfil')=='admin' || Session::get('id')=='gerente' || Session::get('id')=='ventas')
    <div class="row">
        <div class="col-lg-1">
            <div class="text-center">
                <button class="btn btn-primary dim btn-large-dim" type="button" onclick="location.href='{{route('nuevaCotizacion')}}'"><i class="fa">+</i></button>
            </div>
        </div>
    </div> 
    @endif
    
    <div class="col-lg-12">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a class="nav-link active show" data-toggle="tab" href="#tab-1">Cotizaciones pendientes</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-2">Cotizaciones en proceso</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-3">Autorizadas</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-4">Venta</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-5">Cancelaciones</a></li>
                        <li><a class="nav-link" data-toggle="tab" href="#tab-6">Versiones Anteriores</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane  active show">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Cotización</th>
                                                <th>Fecha de Elaboración</th>
                                                <th>Elaboro</th>
                                                <th>Materiales</th>
                                                <th>Servicios</th>
                                            </tr>
                                        </thead>
                                        @foreach ($cotizacionesPendientes as $cotizacion)
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if (Session::get('perfil')=='admin' || Session::get('id')=='gerente' || Session::get('id')=='ventas')
                                                    <button type="button" class="btn btn-w-m btn-info" onclick="location.href='{{route('editarCotizacion', ['cotizacion'=>$cotizacion->id])}}'">Finalizar</button>
                                                    <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('cancelarCotizacion', $cotizacion->id)}}'">Cancelar cotización</button>
                                                    @endif
                                                </td>
                                                <td class="desc">
                                                    <h3>
                                                        <a href="#" class="text-navy">
                                                            {{$cotizacion->cotizacion}}
                                                        </a>
                                                    </h3>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->fechaElaboracion}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->elaboro->nombre}} {{$cotizacion->elaboro->paterno}} {{$cotizacion->elaboro->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {!!$cotizacion->totalMateriales!!}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {!!$cotizacion->totalServicios!!}
                                                    </h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Cotización</th>
                                                        <th>Fecha de Elaboración</th>
                                                        <th>Cliente</th>
                                                        <th>Elaboro</th>
                                                        <th>Materiales</th>
                                                        <th>Servicios</th>
                                                    </tr>
                                                </thead>
                                                @foreach ($cotizacionesProceso as $cotizacion)
                                                <tbody>
                                                    <tr>
                                                        <td>@if (Session::get('perfil')=='admin' || Session::get('id')=='gerente' || Session::get('id')=='ventas')
                                                            <a href="{{route('pdfCotizacion', ['cotizacion'=>$cotizacion->id])}}" class="btn btn-w-m btn-primary" target="_blank">Mostrar Cotización</a>
                                                            @if ((Session::get('id'))==($cotizacion->autorizo_id))
                                                                <button type="button" class="btn btn-w-m btn-primary" onclick="location.href='{{route('autorizarCotizacion', ['cotizacion'=>$cotizacion->id])}}'">Autorizar</button> 
                                                            @endif
                                                            <button type="button" class="btn btn-w-m btn-info" onclick="location.href='{{route('editarCotizacion', ['cotizacion'=>$cotizacion->id])}}'">Editar Cotizacion</button>
                                                            <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('cancelarCotizacion', $cotizacion->id)}}'">Cancelar cotización</button>
                                                            @endif
                                                        </td>
                                                        <td class="desc">
                                                            <h3>
                                                                <a href="#" class="text-navy">
                                                                    {{$cotizacion->cotizacion}} v. {{$cotizacion->version}}
                                                                </a>
                                                            </h3>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->fechaElaboracion}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->clientes->cliente}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->elaboro->nombre}} {{$cotizacion->elaboro->paterno}} {{$cotizacion->elaboro->materno}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {!!$cotizacion->totalMateriales!!}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {!!$cotizacion->totalServicios!!}
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                @endforeach
                                            </table>
                                        </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Cotización</th>
                                                <th>Fecha de Autorizacion</th>
                                                <th>Cliente</th>
                                                <th>Elaboro</th>
                                                <th>Autorizo</th>
                                                <th>Materiales</th>
                                                <th>Servicios</th>
                                            </tr>
                                        </thead>
                                        @foreach ($cotizacionesAutorizadas as $cotizacion)
                                        <tbody>
                                            <tr>
                                                <td>
                                                    @if (Session::get('perfil')=='admin' || Session::get('id')=='gerente' || Session::get('id')=='ventas')
                                                    <a href="{{route('pdfCotizacion', ['cotizacion'=>$cotizacion->id])}}" class="btn btn-w-m btn-primary" target="_blank">Mostar Cotización</a>
                                                    <button type="button" class="btn btn-w-m btn-primary" onclick="location.href='{{route('procesarVentaCotizacion', ['cotizacion'=>$cotizacion->id])}}'">Generar Venta</button>
                                                    <button type="button" class="btn btn-w-m btn-danger" onclick="location.href='{{route('cancelarCotizacion', $cotizacion->id)}}'">Cancelar cotización</button>
                                                    @endif
                                                </td>
                                                <td class="desc">
                                                    <h3>
                                                        <a href="#" class="text-navy">
                                                            {{$cotizacion->cotizacion}} v. {{$cotizacion->version}}
                                                        </a>
                                                    </h3>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->fechaAutorizacion}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->clientes->cliente}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->elaboro->nombre}} {{$cotizacion->elaboro->paterno}} {{$cotizacion->elaboro->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->autorizo->nombre}} {{$cotizacion->autorizo->paterno}} {{$cotizacion->autorizo->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {!!$cotizacion->totalMateriales!!}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {!!$cotizacion->totalServicios!!}
                                                    </h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Cotización</th>
                                                <th>Fecha de Autorizacion</th>
                                                <th>Cliente</th>
                                                <th>Elaboro</th>
                                                <th>Autorizo</th>
                                                <th>Materiales</th>
                                                <th>Servicios</th>
                                            </tr>
                                        </thead>
                                        @foreach ($cotizacionesVenta as $cotizacion)
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="{{route('pdfCotizacion', ['cotizacion'=>$cotizacion->id])}}" class="btn btn-w-m btn-primary" target="_blank">Mostar Cotización</a> 
                                                </td>
                                                <td class="desc">
                                                    <h3>
                                                        <a href="#" class="text-navy">
                                                            {{$cotizacion->cotizacion}} v. {{$cotizacion->version}}
                                                        </a>
                                                    </h3>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->fechaAutorizacion}}
                                                    </h4> 
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->clientes->cliente}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->elaboro->nombre}} {{$cotizacion->elaboro->paterno}} {{$cotizacion->elaboro->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {{$cotizacion->autorizo->nombre}} {{$cotizacion->autorizo->paterno}} {{$cotizacion->autorizo->materno}}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {!!$cotizacion->totalMateriales!!}
                                                    </h4>
                                                </td>
                                                <td>
                                                    <h4>
                                                        {!!$cotizacion->totalServicios!!}
                                                    </h4>
                                                </td>
                                            </tr>
                                        </tbody>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Cotización</th>
                                                        <th>Fecha de Cancelación</th>
                                                        <th>Cliente</th>
                                                        <th>Cancelo</th>
                                                        <th>Motivo</th>
                                                        <th>Materiales</th>
                                                        <th>Servicios</th>
                                                    </tr>
                                                </thead>
                                                @foreach ($cotizacionesCanceladas as $cotizacion)
                                                <tbody>
                                                    <tr>
                                                        <td class="desc">
                                                            <h3>
                                                                <a href="#" class="text-navy">
                                                                    {{$cotizacion->cotizacion}} v. {{$cotizacion->version}}
                                                                </a>
                                                            </h3>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->fechaCancelacion}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->clientes->cliente}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->cancelo->nombre}} {{$cotizacion->cancelo->paterno}} {{$cotizacion->cancelo->materno}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {{$cotizacion->motivoCancelacion}}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {!!$cotizacion->totalMateriales!!}
                                                            </h4>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                {!!$cotizacion->totalServicios!!}
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                @endforeach
                                            </table>
                                        </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-6" class="tab-pane">
                            <div class="panel-body">
                                    <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Cotización</th>
                                                        <th>Versiones</th>
                                                    </tr>
                                                </thead>
                                                @foreach ($cotizacionesVersionadas as $cotizacion)
                                                <tbody>
                                                    <tr>
                                                        <td class="desc">
                                                            <h3>
                                                                <a href="#" class="text-navy">
                                                                    {{$cotizacion->cotizacion}}
                                                                </a>
                                                            </h3>
                                                        </td>
                                                        <td>
                                                            <h4>
                                                                @for ($i = 1; $i <= ($cotizacion->version); $i++)
                                                                    <a href="{{route('pdfVersionCotizacion', ['cotizacion'=>$cotizacion->id, 'version'=>$i])}}" class="btn btn-w-m btn-primary" target="_blank">V. {{$i}}</a>
                                                                @endfor
                                                            </h4>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                @endforeach
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

@endsection