<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title')</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/cropper/cropper.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/switchery/switchery.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/nouslider/jquery.nouislider.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/ionRangeSlider/ion.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/clockpicker/clockpicker.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/select2/select2.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet">

    <link href="{{asset('css/plugins/dualListbox/bootstrap-duallistbox.min.css')}}" rel="stylesheet">


    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>

<body>

<div id="wrapper">

<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element">
                            <img alt="image" class="rounded-circle" style="max-width:55px; height:auto;" src="{{asset('logos_empresas/'.Session::get('logo'))}}"/>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="block m-t-xs font-bold">{{Session::get('razonSocial')}}</span>
                                <span class="text-muted text-xs block">{{Session::get('rfc')}}<b class="caret"></b></span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a class="dropdown-item" href="profile.html">Perfil</a></li>
                                <li><a class="dropdown-item" href="contacts.html">Contactos</a></li>
                                <li class="dropdown-divider"></li>
                                <li><a class="dropdown-item" href="{{route('logoutCrm')}}">Cerrar Sesión</a></li>
                            </ul>
                        </div>
                        <div class="logo-element">
                            sPMCE
                        </div>
                    </li>
                @section('menuInicio')
                <li>
                    <a href="{{route('inicio')}}"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
                </li>
                @show
                @section('menuCatalogo')
                <li>
                    <a href="#"><i class="fa fa-wrench"></i> <span class="nav-label">Catálogos</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('marcas')}}">Marcas</a></li>
                        <li><a href="{{route('lineas')}}">Líneas</a></li>
                        <li><a href="{{route('unidades')}}">Unidades</a></li>
                        <li><a href="{{route('industrias')}}">Industrias</a></li>
                    </ul>
                </li>
                @show
                @section('menuClientes')
                <li>
                    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Clientes</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('clientes')}}">Clientes</a></li>
                        <li><a href="{{route('contactosClientes')}}">Contactos</a></li>
                    </ul>
                </li>
                @show
                @section('menuProveedores')
                <li>
                    <a href="#"><i class="fa fa-book"></i> <span class="nav-label">Proveedores</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('proveedores')}}">Proveedores</a></li>
                        <li><a href="{{route('contactosProveedores')}}">Contactos</a></li>
                    </ul>
                </li>
                @show
                @section('menuSKU')
                <li>
                    <a href="#"><i class="fa fa-clipboard"></i> <span class="nav-label">SKU's</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{route('materiales')}}">Materiales</a></li>
                        <li><a href="{{route('servicios')}}">Servicios</a></li>
                    </ul>
                </li>
                @show
                @if (Session::get('perfil')=='admin' || Session::get('perfil')=='ventas' || Session::get('perfil')=='gerente') 
                @section('menuCotizaciones')
                <li>
                    <a href="{{route('cotizaciones')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Cotizaciones</span></a>
                </li>
                @show
                @endif
                @if (Session::get('perfil')=='admin' || Session::get('perfil')=='compras' || Session::get('perfil')=='finanzas') 
                @section('menuOrdenes')
                <li>
                    <a href="{{route('ordenes')}}"><i class="fa fa-book"></i> <span class="nav-label">Ordenes de compra</span></a>
                </li>
                @show
                @endif
                @if (Session::get('perfil')=='admin' || Session::get('perfil')=='ventas' || Session::get('perfil')=='gerente') 
                @section('menuOportunidades')
                <li>
                    <a href="{{route('oportunidades')}}"><i class="fa fa-address-card"></i> <span class="nav-label">Oportunidades</span></a>
                </li>
                @show   
                @endif     
        </ul>
    </div>
</nav>

<div id="page-wrapper" class="gray-bg">
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            @yield('buscador')
                <!--
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>-->
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <!--
            <li>
                <span class="m-r-sm text-muted welcome-message">Bienvenido @yield('user')</span>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                </a>
                <ul class="dropdown-menu dropdown-messages">
                    <li>
                        <div class="dropdown-messages-box">
                            <a class="dropdown-item float-left" href="profile.html">
                                <img alt="image" class="rounded-circle" src="img/a7.jpg">
                            </a>
                            <div class="media-body">
                                <small class="float-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <div class="dropdown-messages-box">
                            <a class="dropdown-item float-left" href="profile.html">
                                <img alt="image" class="rounded-circle" src="img/a4.jpg">
                            </a>
                            <div class="media-body ">
                                <small class="float-right text-navy">5h ago</small>
                                <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <div class="dropdown-messages-box">
                            <a class="dropdown-item float-left" href="profile.html">
                                <img alt="image" class="rounded-circle" src="img/profile.jpg">
                            </a>
                            <div class="media-body ">
                                <small class="float-right">23h ago</small>
                                <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="mailbox.html" class="dropdown-item">
                                <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                </a>
                <ul class="dropdown-menu dropdown-alerts">
                    <li>
                        <a href="mailbox.html">
                            <div>
                                <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                <span class="float-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <a href="profile.html">
                            <div>
                                <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                <span class="float-right text-muted small">12 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <a href="grid_options.html">
                            <div>
                                <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                <span class="float-right text-muted small">4 minutes ago</span>
                            </div>
                        </a>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="notifications.html">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>-->


            <li>
                <a href="{{route('logoutCrm')}}">
                    <i class="fa fa-sign-out"></i> Salir
                </a>
            </li>
        </ul>

    </nav>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        @section('breadcrumb')
        <h2>Inicio</h2>
        @show 
    </div>
</div>

@yield('content')

<div class="footer" >
    <div class="float-right">
        To fit in it
    </div>
    <div>
        <strong>Copyright</strong> SoftwareLab &copy; 2019
    </div>
</div>

</div>
</div>



<!-- Mainly scripts -->
<script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('js/inspinia.js')}}"></script>
<script src="{{asset('js/plugins/pace/pace.min.js')}}"></script>
<script src="{{asset('js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>


<!--Subir imagenes -->
<script>
$('.custom-file-input').on('change', function() {
    let fileName = $(this).val().split('\\').pop();
    $(this).next('.custom-file-label').addClass("selected").html(fileName);
}); 
</script>

<!-- Chosen -->
<script src="{{asset('js/plugins/chosen/chosen.jquery.js')}}"></script>

<!-- JSKnob -->
<script src="{{asset('js/plugins/jsKnob/jquery.knob.js')}}"></script>

<!-- Input Mask-->
 <script src="{{asset('js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>

<!-- Data picker -->
<script src="{{asset('js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

<!-- NouSlider -->
<script src="{{asset('js/plugins/nouslider/jquery.nouislider.min.js')}}"></script>

<!-- Switchery -->
<script src="{{asset('js/plugins/switchery/switchery.js')}}"></script>

 <!-- IonRangeSlider -->
 <script src="{{asset('js/plugins/ionRangeSlider/ion.rangeSlider.min.js')}}"></script>

 <!-- iCheck -->
 <script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>

 <!-- MENU -->
 <script src="{{asset('js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>

 <!-- Color picker -->
 <script src="{{asset('js/plugins/colorpicker/bootstrap-colorpicker.min.js')}}"></script>

 <!-- Clock picker -->
 <script src="{{asset('js/plugins/clockpicker/clockpicker.js')}}"></script>

 <!-- Image cropper -->
 <script src="{{asset('js/plugins/cropper/cropper.min.js')}}"></script>

 <!-- Date range use moment.js same as full calendar plugin -->
 <script src="{{asset('js/plugins/fullcalendar/moment.min.js')}}"></script>

 <!-- Date range picker -->
 <script src="{{asset('js/plugins/daterangepicker/daterangepicker.js')}}"></script>

 <!-- Select2 -->
 <script src="{{asset('js/plugins/select2/select2.full.min.js')}}"></script>

 <!-- TouchSpin -->
 <script src="{{asset('js/plugins/touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

 <!-- Tags Input -->
 <script src="{{asset('js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>

 <!-- Dual Listbox -->
 <script src="{{asset('js/plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}"></script>

 <script>
     $(document).ready(function(){

         $('.tagsinput').tagsinput({
             tagClass: 'label label-primary'
         });

         var $image = $(".image-crop > img")
         $($image).cropper({
             aspectRatio: 1.618,
             preview: ".img-preview",
             done: function(data) {
                 // Output the result data for cropping image.
             }
         });

         var $inputImage = $("#inputImage");
         if (window.FileReader) {
             $inputImage.change(function() {
                 var fileReader = new FileReader(),
                         files = this.files,
                         file;

                 if (!files.length) {
                     return;
                 }

                 file = files[0];

                 if (/^image\/\w+$/.test(file.type)) {
                     fileReader.readAsDataURL(file);
                     fileReader.onload = function () {
                         $inputImage.val("");
                         $image.cropper("reset", true).cropper("replace", this.result);
                     };
                 } else {
                     showMessage("Please choose an image file.");
                 }
             });
         } else {
             $inputImage.addClass("hide");
         }

         $("#download").click(function() {
             window.open($image.cropper("getDataURL"));
         });

         $("#zoomIn").click(function() {
             $image.cropper("zoom", 0.1);
         });

         $("#zoomOut").click(function() {
             $image.cropper("zoom", -0.1);
         });

         $("#rotateLeft").click(function() {
             $image.cropper("rotate", 45);
         });

         $("#rotateRight").click(function() {
             $image.cropper("rotate", -45);
         });

         $("#setDrag").click(function() {
             $image.cropper("setDragMode", "crop");
         });

         var mem = $('#data_1 .input-group.date').datepicker({
             todayBtn: "linked",
             keyboardNavigation: false,
             forceParse: false,
             calendarWeeks: true,
             autoclose: true
         });

         var yearsAgo = new Date();
         yearsAgo.setFullYear(yearsAgo.getFullYear() - 20);

         $('#selector').datepicker('setDate', yearsAgo );


         $('#data_2 .input-group.date').datepicker({
             startView: 1,
             todayBtn: "linked",
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true,
             format: "dd/mm/yyyy"
         });

         $('#data_3 .input-group.date').datepicker({
             startView: 2,
             todayBtn: "linked",
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });

         $('#data_4 .input-group.date').datepicker({
             minViewMode: 1,
             keyboardNavigation: false,
             forceParse: false,
             forceParse: false,
             autoclose: true,
             todayHighlight: true
         });

         $('#data_5 .input-daterange').datepicker({
             keyboardNavigation: false,
             forceParse: false,
             autoclose: true
         });

         var elem = document.querySelector('.js-switch');
         var switchery = new Switchery(elem, { color: '#1AB394' });

         var elem_2 = document.querySelector('.js-switch_2');
         var switchery_2 = new Switchery(elem_2, { color: '#ED5565' });

         var elem_3 = document.querySelector('.js-switch_3');
         var switchery_3 = new Switchery(elem_3, { color: '#1AB394' });

         var elem_4 = document.querySelector('.js-switch_4');
         var switchery_4 = new Switchery(elem_4, { color: '#f8ac59' });
             switchery_4.disable();

         $('.i-checks').iCheck({
             checkboxClass: 'icheckbox_square-green',
             radioClass: 'iradio_square-green'
         });

         $('.demo1').colorpicker();

         var divStyle = $('.back-change')[0].style;
         $('#demo_apidemo').colorpicker({
             color: divStyle.backgroundColor
         }).on('changeColor', function(ev) {
                     divStyle.backgroundColor = ev.color.toHex();
                 });

         $('.clockpicker').clockpicker();

         $('input[name="daterange"]').daterangepicker();

         $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

         $('#reportrange').daterangepicker({
             format: 'MM/DD/YYYY',
             startDate: moment().subtract(29, 'days'),
             endDate: moment(),
             minDate: '01/01/2012',
             maxDate: '12/31/2015',
             dateLimit: { days: 60 },
             showDropdowns: true,
             showWeekNumbers: true,
             timePicker: false,
             timePickerIncrement: 1,
             timePicker12Hour: true,
             ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                 'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
             },
             opens: 'right',
             drops: 'down',
             buttonClasses: ['btn', 'btn-sm'],
             applyClass: 'btn-primary',
             cancelClass: 'btn-default',
             separator: ' to ',
             locale: {
                 applyLabel: 'Submit',
                 cancelLabel: 'Cancel',
                 fromLabel: 'From',
                 toLabel: 'To',
                 customRangeLabel: 'Custom',
                 daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
                 monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                 firstDay: 1
             }
         }, function(start, end, label) {
             console.log(start.toISOString(), end.toISOString(), label);
             $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
         });

         $(".select2_demo_1").select2();
         $(".select2_demo_2").select2();
         $(".select2_demo_3").select2({
             placeholder: "Select a state",
             allowClear: true
         });


         $(".touchspin1").TouchSpin({
             buttondown_class: 'btn btn-white',
             buttonup_class: 'btn btn-white'
         });

         $(".touchspin2").TouchSpin({
             min: 0,
             max: 100,
             step: 0.1,
             decimals: 2,
             boostat: 5,
             maxboostedstep: 10,
             postfix: '%',
             buttondown_class: 'btn btn-white',
             buttonup_class: 'btn btn-white'
         });

         $(".touchspin3").TouchSpin({
             verticalbuttons: true,
             buttondown_class: 'btn btn-white',
             buttonup_class: 'btn btn-white'
         });

         $('.dual_select').bootstrapDualListbox({
             selectorMinimalHeight: 160
         });


     });

     $('.chosen-select').chosen({width: "100%"});

     $("#ionrange_1").ionRangeSlider({
         min: 0,
         max: 5000,
         type: 'double',
         prefix: "$",
         maxPostfix: "+",
         prettify: false,
         hasGrid: true
     });

     $("#ionrange_2").ionRangeSlider({
         min: 0,
         max: 10,
         type: 'single',
         step: 0.1,
         postfix: " carats",
         prettify: false,
         hasGrid: true
     });

     $("#ionrange_3").ionRangeSlider({
         min: -50,
         max: 50,
         from: 0,
         postfix: "°",
         prettify: false,
         hasGrid: true
     });

     $("#ionrange_4").ionRangeSlider({
         values: [
             "January", "February", "March",
             "April", "May", "June",
             "July", "August", "September",
             "October", "November", "December"
         ],
         type: 'single',
         hasGrid: true
     });

     $("#ionrange_5").ionRangeSlider({
         min: 10000,
         max: 100000,
         step: 100,
         postfix: " km",
         from: 55000,
         hideMinMax: true,
         hideFromTo: false
     });

     $(".dial").knob();

     var basic_slider = document.getElementById('basic_slider');

     noUiSlider.create(basic_slider, {
         start: 40,
         behaviour: 'tap',
         connect: 'upper',
         range: {
             'min':  20,
             'max':  80
         }
     });

     var range_slider = document.getElementById('range_slider');

     noUiSlider.create(range_slider, {
         start: [ 40, 60 ],
         behaviour: 'drag',
         connect: true,
         range: {
             'min':  20,
             'max':  80
         }
     });

     var drag_fixed = document.getElementById('drag-fixed');

     noUiSlider.create(drag_fixed, {
         start: [ 40, 60 ],
         behaviour: 'drag-fixed',
         connect: true,
         range: {
             'min':  20,
             'max':  80
         }
     });


 </script>

 @yield('script')




</body>

</html>
