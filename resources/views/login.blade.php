<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h1 class="logo-name">SPMCE+</h1>
            <h3>Bienvenido a Smart Project Management CRM & ERP</h3>
            <p>Diseño a la medida para tu empresa
            </p>
            <p>Iniciar Sesión</p>
            <form class="m-t" role="form" action="{{route('authLoginCrm')}}" method="POST">
                {{csrf_field()}}
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" name="password" value="{{old('password')}}">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b" name="login">Iniciar Sesión</button>

                <a href="#"><small>¿Olvidaste tu contraseña?</small></a>
                
            </form>

        
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <p class="m-t"> <small>CRM & ERP SoftwareLab @2019</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="{{asset('js/jquery-3.1.1.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>

</body>

</html>
