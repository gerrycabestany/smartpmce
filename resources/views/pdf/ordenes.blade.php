<style type="text/css">

    * { 
        margin: 0; 
        padding: 0; }

    body { 
        /*font: 14px/1.4 Georgia, serif; */
        margin: 5mm 30mm 5mm 5mm;
    }

    #page-wrap { 
        width: 700px;  
        margin: 10 auto; 
    }

    table { border-collapse: collapse; }

    table td, table th { border: 1px solid white; padding: 2px; }

    #header { 
        height: 35px; 
        width: 107%; 
        margin: 10px 0px 0px 0px;
        background: #085; 
        text-align: right; 
        color: white; 
        font: bold 14px Helvetica, Sans-Serif; 
        letter-spacing: 4px; 
        padding: 10px 0px; 
        border: 1px solid #085;
    }

    #address { 
        width: 50%; 
        height: 169px; 
        float: left;         
        margin-top: 20px;
        margin-left: 20px;
    }
        
    #logo { 
        text-align: right; 
        float: right; 
        position: relative; 
        margin-top: 25px; 
        border: 1px solid #fff; 
        max-width: 540px; 
        max-height: 100px; 
        overflow: hidden; 
    }

    #logo:hover, #logo.edit { border: 1px solid #000; margin-top: 0px; max-height: 125px; }
    #logo:hover, #logo.edit { 
    display: block; 
    text-align: right; line-height: 25px; background: #eee; padding: 0 0px; }

    .items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid white; }
    
    .colElemento{
        width: 290px;
    }
    
    .colMarca, .colTiempo, .colPrecio, .colMonto{
        width: 75px;
    }

    .colCantidad, .colUnidad{
        width: 60px;
    }
    
    th{
        height: 40px; 
		text-align: center; 
		background: #94B6CF;  
		color: white; 
		border: 1px solid #666666 !important;
    }
	td, tr{
        border: 1px solid black !important;;
  		border-collapse: collapse;
    }
    
    .cabezaTabla{
        text-align: right;
        margin-bottom: 10px;
    }
    
    #terms { 
        text-align: justify; 
        margin: 0 20 20 25;
        font-size: 10px;
    }
    
    .total{
        text-align: right;
        font-size: 16px;
        margin-bottom: 8px;
        margin-top: 8px;
        margin-right: 4px;
    }
    
    #cabecera{
        /*position: fixed;*/
        text-align: right;
        position: relative; 
        margin-top: 25px; 
        margin-bottom: -5px;
        border: 1px solid #fff;        
    }

</style> 




<div id="page-wrap">
		
    <div id="header" style="width:110%;">
        <p>ORDEN DE COMPRA . {{$orden}} </p>
        <p> {{ $proveedor }} </p>
    </div>

    <div id="identity">
    
        <div id="address">
            <p><strong>Razón Social: </strong></p>
            <br>    
            <p>No: </p>
            <p>Fecha de envío al proveedor: </p>
            <p>Tiempo de entrega: _____________________</p>
            <br>    
            <br>    
            <p>Representante: </p>
            <p>Tel: </p>
            <br>
        </div>     

        <div id="logo">
            <img id="image" src="../views/modules/img/BIT_logo.png" alt="logo" width="50%"/>
        </div>
    
    </div>
    <br>
    <br>
        


	<!--MATERIALES EN LA PO -->
    <div class="items">    
        <h1 class="cabezaTabla" style="margin-left25px;">Orden de Materiales</h1>
        <table style="width:110%;">

            <thead style="font-size:15px; ">
                <tr>                            
                    <th>Código</th>
                    <th>Nombre</th>
                    <th>Marca</th>
                    <th>Cantidad</th>
                    <th>Unidad</th>
                    <th>Precio Unitario</th>
                    <th>Importe</th>                            
                </tr>
            </thead>
            
            <tbody>
                {{$valoresOrdenes}}
                @foreach ($valoresOrdenes as $valorOrden)
                    <tr style="font-size:12px;"> 
                        <td align="center">{{ $valorOrden->codigo }}</td>
                        <td align="left">{{ $valorOrden->nombre }}</td>
                        <td align="center">{{ $valorOrden->marcas->marca }}</td>
                        <td align="center">{{ $valorOrden->cantidad}}</td>
                        <td align="center">{{ $valorOrden->unidades->unidad }}</td>
                        <td align="right">{{ $valorOrden->costo }}</td>
                        <td align="right">{{ ($valorOrden->costo)*1.16 }}</td>
                    </tr>
                @endforeach
            </tbody>

        </table>

    </div>

    <div>
        <h2 class="total">
            MONEDA: MXN
        </h2>
    </div>

    <div>
        <h2 class="total">
            SUBTOTAL: $ {{ number_format(($orden->total), 2, '.', ',') }}
        </h2>
    </div>

    <div>
        <h2 class="total">
            IVA: $ {{ number_format(($orden->total)*0.16, 2, '.', '') }}
        </h2>
    </div>

    <div>
        <h2 class="total">
            TOTAL: $ {{ number_format(($orden->total) * 1.16, 2, '.', ',') }}
        </h2>
    </div>

    <div>
        <h3 class="total">
            IMPORTE CON LETRA: 
        </h3>
    </div>

    <div> 
        <h3>DATOS DE FACTURACION:</h3><br>
        <h4>R.F.C: </h4><br>
        <h4>RAZÓN SOCIAL: </h4><br>
        <br>
    </div>

    <div> 
        <h3>DOMICILIO:</h3><br>
        <h4>ENTREGAR EN:</h4><br><br><br>
        </td>
        <h4>FECHA DE RECEPCIÓN:</h4><br><br><br>            
    </div>

    <div style="font-size:9px;"> 
        <h4>NOMBRE Y FIRMA DE QUIEN RECIBIÓ LOS MATERIALES</h4>
        <h4>______________________________________________________________________</h4>
        <br>
        <h4>______________________________________________________________________</h4>
    </div>

    <div style="font-size:9px; "> 
        <h4>NOMBRE Y FIRMA DE QUIEN LO SOLICITA:</h4>
        <h4>______________________________________________________________________</h4>
        <br>
        <h4>______________________________________________________________________</h4>

        <h4>NOMBRE Y FIRMA DE QUIEN LO APRUEBA:</h4>
        <h4>______________________________________________________________________</h4>
        <br>
        <h4>______________________________________________________________________</h4>

        <h4>NOMBRE Y FIRMA DE QUIEN LO PROVEE:</h4>
        <h4>______________________________________________________________________</h4>
        <br>
        <h4>______________________________________________________________________</h4>
    </div>
                        
                                
        
</div>