<style type="text/css">

    * { 
        margin: 0; 
        padding: 0; }

    body { 
        /*font: 14px/1.4 Georgia, serif; */
        margin: 5mm 30mm 5mm 5mm;
    }

    #page-wrap { 
        width: 700px;  
        margin: 10 auto; 
    }

    table { border-collapse: collapse; }

    table td, table th { border: 1px solid white; padding: 2px; }

    #header { 
        height: 35px; 
        width: 107%; 
        margin: 10px 0px 0px 0px;
        background: #666666; 
        text-align: right; 
        color: white; 
        font: bold 14px Helvetica, Sans-Serif; 
        letter-spacing: 4px; 
        padding: 10px 0px; 
        border: 1px solid #666666;
    }

    #address { 
        width: 50%; 
        height: 169px; 
        float: left;         
        margin-top: 20px;
        margin-left: 20px;
    }
        
    #logo { 
        text-align: right; 
        float: right; 
        position: relative; 
        margin-top: 25px; 
        border: 1px solid #fff; 
        max-width: 540px; 
        max-height: 100px; 
        overflow: hidden; 
    }

    #logo:hover, #logo.edit { border: 1px solid #000; margin-top: 0px; max-height: 125px; }
    #logo:hover, #logo.edit { 
    display: block; 
    text-align: right; line-height: 25px; background: #eee; padding: 0 0px; }

    .items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid white; }
    
    .colElemento{
        width: 290px;
    }
    
    .colMarca, .colTiempo, .colPrecio, .colMonto{
        width: 75px;
    }

    .colCantidad, .colUnidad{
        width: 50px;
    }
    
    th{
        height: 40px;
        text-align: center;
        background: #94B6CF; 
        color: white;
        border: 1px solid #666666 !important;
    }
    
    .cabezaTabla{
        text-align: right;
        margin-bottom: 10px;
    }
    
    .terms { 
        text-align: justify; 
        margin: 0 20 20 25;
        font-size: 10px;
    }
    
    .total{
        text-align: right;
        font-size: 16px;
        margin-bottom: 8px;
        margin-top: 8px;
        margin-right: 4px;
    }
    
    #cabecera{
        /*position: fixed;*/
        text-align: right;
        position: relative; 
        margin-top: 25px; 
        margin-bottom: -5px;
        border: 1px solid #fff;        
    }

    #pie{
        text-align: right;
        font-size: 10px;
    }

</style> 



<div id="page-wrap">   

    <div id="header">
    <h3>Cotización No: {{ $noCotizacion}} v. {{$version}}</h3>
        <h4>Fecha: {{$cotizacion->fechaElaboracion}}</h4>
    </div>


    <div id="identity">
		
        <div id="address">
            <p><strong>Cliente: {{$cotizacion->clientes->cliente}}</strong></p>
            <br> 
            @if (($cotizacion->direccionCliente_id)!=null)
            <p>Calle y No. {{$cotizacion->direcciones->calle}} {{$cotizacion->direcciones->ext}} {{$cotizacion->direcciones->int}}</p>
            <p>Col. {{$cotizacion->direcciones->colonia}}</p>
            <p>Municipio {{$cotizacion->direcciones->municipio}}</p>
            <p>Estado, C.P. {{$cotizacion->direcciones->estado}} {{$cotizacion->direcciones->cp}}</p>    
            @endif   
            @if (($cotizacion->telefonoCliente_id)!=null)
            <br>    
            <p>Tel: {{$cotizacion->telefonos->pais}}  {{$cotizacion->telefonos->area}} {{$cotizacion->telefonos->telefono}}</p> 
            @endif   
            @if (($cotizacion->emailCliente_id)!=null)
            <br>    
            <p>Email: {{$cotizacion->emails->email}}</p> 
            @endif  
            
            @if (($cotizacion->contactoCliente_id)!=null)
            <br>    
            <p>At´n: {{$cotizacion->contactos->nombre}} {{$cotizacion->contactos->paterno}} {{$cotizacion->contactos->materno}}</p>
            @endif  
            
        </div>     

        <div id="logo" >
            <img id="image" src="{{asset('logos_empresas/'.Session::get('logo'))}}" width="50%"/>
        </div>

    </div>
    <br>
    <br>

    



    
            <div class="items">
                    <h1 class="cabezaTabla">Materiales</h1>
                       
                        
                        <table class="table table-striped">

                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Material</th>
                                    <th>Marca</th>
                                    <th>Precio</th>
                                    <th>Moneda</th>
                                    <th>Cant.</th>
                                    <th>Unidad</th>
                                    <th>Monto</th>
                                    <th>T. Entrega</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($valoresCotizaciones as $valorCotizacion)
                                    @if (($valorCotizacion->material)==1)
                                        <tr>
                                            <td>{{$valorCotizacion->codigo}}</td>
                                            <td>{{$valorCotizacion->nombre}}</td>
                                            <td>{{$valorCotizacion->marcas->marca}}</td>
                                            <td>$ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100), 2, '.', ',')}}
                                            </td>
                                            <td>{{$valorCotizacion->monedas->moneda}}</td>
                                            <td>
                                                {{$valorCotizacion->cantidad}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->unidades->unidad}}
                                            </td>
                                            <td>
                                                $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100)*($valorCotizacion->cantidad), 2, '.', ',')}}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>

                        </table>
                       

                <div>
                    <h2 class="total">
                        Total de materiales: $ {{number_format($totalMateriales, 2, '.', ',')}}
                    </h2>
                </div>

            </div>




            <div class="items">
                    <h1 class="cabezaTabla">Servicios</h1>

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Servicio </th>
                                    <th>Marca</th>
                                    <th>Precio</th>
                                    <th>Moneda</th>
                                    <th>Cant.</th>
                                    <th>Unidad</th>
                                    <th>Monto</th>
                                    <th>T. Entrega</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($valoresCotizaciones as $valorCotizacion)
                                    @if (($valorCotizacion->servicio)==1)
                                        <tr>
                                            <td>
                                                {{$valorCotizacion->codigo}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->nombre}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->marcas->marca}}   
                                            </td>
                                            <td>
                                                $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100), 2, '.', ',')}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->monedas->moneda}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->cantidad}}
                                            </td>
                                            <td>
                                                {{$valorCotizacion->unidades->unidad}}
                                            </td>
                                            <td>
                                                $ {{number_format(($valorCotizacion->costo)/(1-($valorCotizacion->margen)/100)*($valorCotizacion->cantidad), 2, '.', ',')}}
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>

                        </table>

                <div>
                    <h2 class="total">
                            Total de servicios: $ {{number_format($totalServicios, 2, '.', ',')}}
                    </h2>
                </div>

            </div>  




    <div style="page-break-after:always;"></div>
    <br>
    <br>

    <div class="row" style="margin-top:15px;">
        <div class="col-lg-12">
            <div class="ibox ">


                <div class="ibox-content">
                        <div class="form-group  row">
                            <label>Condiciones comerciales:</label>
                            <input type="hidden" class="form-control" name="condicionesComerciales" id="condicionesComerciales" value="">
                            <div class="col-sm-10">
                                @foreach ($condicionesComerciales as $condicionComercial)
                                    <div class="form-control">
                                        @if (in_array(($condicionComercial->orden),$condiciones))
                                            <p class="terms">{{$condicionComercial->texto}}</p>
                                            <br>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="form-group  row">
                            <label>Condiciones de pago: </label>
                                <p class="terms">{{$cotizacion->condicionesPagos}}</p>
                                <br>
                        </div>

                        <div class="form-group  row">
                            <label>Comentarios: </label>
                                <p class="terms">{{$cotizacion->comentarios}}</p>
                                <br>
                        </div>

                </div>

            </div>
            
        </div>
    </div>  


    <div id="pie">
        <p>Elaboró: {{$cotizacion->elaboro->nombre}} {{$cotizacion->elaboro->paterno}} {{$cotizacion->elaboro->materno}}</p>                            
        <p>Autorizó: {{$cotizacion->autorizo->nombre}} {{$cotizacion->autorizo->paterno}} {{$cotizacion->autorizo->materno}}</p>                            
    </div>


</div>