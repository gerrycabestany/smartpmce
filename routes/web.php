<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Login
Route::get('crm/login', 'LoginController@login')->name("loginCrm");
Route::get('/crm', 'LoginController@login');
Route::get('/', 'LoginController@login');
Route::post('/crm/login/auth', 'LoginController@authLogin')->name('authLoginCrm');
Route::get('/crm/logout', 'LoginController@logout')->name('logoutCrm');

//Rutas privadas solo para usuarios registrados y que iniciaron sesión
Route::middleware('session')->group(function(){

    //Inicio
    Route::get('crm/inicio', 'LoginController@inicio')->name("inicio");

    //Marcas
    Route::get('crm/marcas', 'MarcasController@marcas')->name("marcas");
    Route::get('crm/marcas/nueva', 'MarcasController@nuevaMarca')->name("nuevaMarca");
    Route::post('crm/marcas/crear', 'MarcasController@crearMarca')->name("crearMarca");
    Route::get('crm/marcas/{marca}', 'MarcasController@editarMarca')->where('marca', '[0-9]+')->name("editarMarca");
    Route::post('crm/marcas/{marca}', 'MarcasController@actualizarMarca')->name("actualizarMarca");
    Route::delete('crm/marcas/{marca}', 'MarcasController@borrarMarca')->name("borrarMarca");

    //Lineas
    Route::get('crm/lineas', 'LineasController@lineas')->name("lineas");
    Route::get('crm/lineas/nueva', 'LineasController@nuevaLinea')->name("nuevaLinea");
    Route::post('crm/lineas/crear', 'LineasController@crearLinea')->name("crearLinea");
    Route::get('crm/lineas/{linea}', 'LineasController@editarLinea')->where('linea', '[0-9]+')->name("editarLinea");
    Route::post('crm/lineas/{linea}', 'LineasController@actualizarLinea')->name("actualizarLinea");
    Route::delete('crm/lineas/{linea}', 'LineasController@borrarLinea')->name("borrarLinea");

    //Unidades
    Route::get('crm/unidades', 'UnidadesController@unidades')->name("unidades");
    Route::get('crm/unidades/nueva', 'UnidadesController@nuevaUnidad')->name("nuevaUnidad");
    Route::post('crm/unidades/crear', 'UnidadesController@crearUnidad')->name("crearUnidad");
    Route::get('crm/unidades/{unidad}', 'UnidadesController@editarUnidad')->where('unidad', '[0-9]+')->name("editarUnidad");
    Route::post('crm/unidades/{unidad}', 'UnidadesController@actualizarUnidad')->name("actualizarUnidad");
    Route::delete('crm/unidades/{unidad}', 'UnidadesController@borrarUnidad')->name("borrarUnidad");

    //Industrias
    Route::get('crm/industrias', 'IndustriasController@industrias')->name("industrias");
    Route::get('crm/industrias/nueva', 'IndustriasController@nuevaIndustria')->name("nuevaIndustria");
    Route::post('crm/industrias/crear', 'IndustriasController@crearIndustria')->name("crearIndustria");
    Route::get('crm/industrias/{industria}', 'IndustriasController@editarIndustria')->where('industria', '[0-9]+')->name("editarIndustria");
    Route::post('crm/industrias/{industria}', 'IndustriasController@actualizarIndustria')->name("actualizarIndustria");
    Route::delete('crm/industrias/{industria}', 'IndustriasController@borrarIndustria')->name("borrarIndustria");

    //Clientes
    Route::get('crm/clientes', 'ClientesController@clientes')->name("clientes");
    Route::get('crm/clientes/nuevo', 'ClientesController@nuevoCliente')->name("nuevoCliente");
    Route::post('crm/clientes/crear', 'ClientesController@crearCliente')->name("crearCliente");
    Route::get('crm/clientes/{cliente}', 'ClientesController@editarCliente')->where('cliente', '[0-9]+')->name("editarCliente");
    Route::post('crm/clientes/{cliente}', 'ClientesController@actualizarCliente')->name("actualizarCliente");
    Route::delete('crm/clientes/{cliente}', 'ClientesController@borrarCliente')->name("borrarCliente");

    Route::get('crm/contactosClientes', 'ClientesController@contactosClientes')->name("contactosClientes");
    Route::get('crm/contactosClientes/{contactoCliente}', 'ClientesController@editarContactoCliente')->where('contactoCliente', '[0-9]+')->name("editarContactoCliente");
    Route::delete('crm/contactosClientes/{contactoCliente}', 'ClientesController@borrarContactoCliente')->name("borrarContactoCliente");
    Route::get('crm/contactosClientes/nuevo', 'ClientesController@nuevoContactoCliente')->name("nuevoContactoCliente");
    Route::post('crm/contactosClientes/crear', 'ClientesController@crearContactoCliente')->name("crearContactoCliente");
    Route::post('crm/contactosClientes/{contactoCliente}', 'ClientesController@actualizarContactoCliente')->name("actualizarContactoCliente");

    Route::get('crm/direccionesClientes/{direccionCliente}', 'ClientesController@editarDireccionCliente')->where('direccionCliente', '[0-9]+')->name("editarDireccionCliente");
    Route::delete('crm/direccionesClientes/{direccionCliente}', 'ClientesController@borrarDireccionCliente')->name("borrarDireccionCliente");
    Route::get('crm/direccionesClientes/nuevo', 'ClientesController@nuevaDireccionCliente')->name("nuevaDireccionCliente");
    Route::post('crm/direccionesClientes/crear', 'ClientesController@crearDireccionCliente')->name("crearDireccionCliente");
    Route::post('crm/direccionesClientes/{direccionCliente}', 'ClientesController@actualizarDireccionCliente')->name("actualizarDireccionCliente");

    Route::get('crm/emailsClientes/{emailCliente}', 'ClientesController@editarEmailCliente')->where('emailCliente', '[0-9]+')->name("editarEmailCliente");
    Route::delete('crm/emailsClientes/{emailCliente}', 'ClientesController@borrarEmailCliente')->name("borrarEmailCliente");
    Route::get('crm/emailsClientes/nuevo', 'ClientesController@nuevoEmailCliente')->name("nuevoEmailCliente");
    Route::post('crm/emailsClientes/crear', 'ClientesController@crearEmailCliente')->name("crearEmailCliente");
    Route::post('crm/emailsClientes/{emailCliente}', 'ClientesController@actualizarEmailCliente')->name("actualizarEmailCliente");

    Route::get('crm/telefonosClientes/{telefonoCliente}', 'ClientesController@editarTelefonoCliente')->where('telefonoCliente', '[0-9]+')->name("editarTelefonoCliente");
    Route::delete('crm/telefonosClientes/{telefonoCliente}', 'ClientesController@borrarTelefonoCliente')->name("borrarTelefonoCliente");
    Route::get('crm/telefonosClientes/nuevo', 'ClientesController@nuevoTelefonoCliente')->name("nuevoTelefonoCliente");
    Route::post('crm/telefonosClientes/crear', 'ClientesController@crearTelefonoCliente')->name("crearTelefonoCliente");
    Route::post('crm/telefonosClientes/{telefonoCliente}', 'ClientesController@actualizarTelefonoCliente')->name("actualizarTelefonoCliente");

    //Proveedores
    Route::get('crm/proveedores', 'ProveedoresController@proveedores')->name("proveedores");
    Route::get('crm/proveedores/nuevo', 'ProveedoresController@nuevoProveedor')->name("nuevoProveedor");
    Route::post('crm/proveedores/crear', 'ProveedoresController@crearProveedor')->name("crearProveedor");
    Route::get('crm/proveedores/{proveedor}', 'ProveedoresController@editarProveedor')->where('proveedor', '[0-9]+')->name("editarProveedor");
    Route::post('crm/proveedores/{proveedor}', 'ProveedoresController@actualizarProveedor')->name("actualizarProveedor");
    Route::delete('crm/proveedores/{proveedor}', 'ProveedoresController@borrarProveedor')->name("borrarProveedor");
    
    Route::get('crm/contactosProveedores', 'ProveedoresController@contactosProveedores')->name("contactosProveedores");
    Route::get('crm/contactosProveedores/{contactoProveedor}', 'ProveedoresController@editarContactoProveedor')->where('contactoProveedor', '[0-9]+')->name("editarContactoProveedor");
    Route::delete('crm/contactosProveedores/{contactoProveedor}', 'ProveedoresController@borrarContactoProveedor')->name("borrarContactoProveedor");
    Route::get('crm/contactosProveedores/nuevo', 'ProveedoresController@nuevoContactoProveedor')->name("nuevoContactoProveedor");
    Route::post('crm/contactosProveedores/crear', 'ProveedoresController@crearContactoProveedor')->name("crearContactoProveedor");
    Route::post('crm/contactosProveedores/{contactoProveedor}', 'ProveedoresController@actualizarContactoProveedor')->name("actualizarContactoProveedor");

    Route::get('crm/direccionesProveedores/{direccionProveedor}', 'ProveedoresController@editarDireccionProveedor')->where('direccionProveedor', '[0-9]+')->name("editarDireccionProveedor");
    Route::delete('crm/direccionesProveedores/{direccionProveedor}', 'ProveedoresController@borrarDireccionProveedor')->name("borrarDireccionProveedor");
    Route::get('crm/direccionesProveedores/nuevo', 'ProveedoresController@nuevaDireccionProveedor')->name("nuevaDireccionProveedor");
    Route::post('crm/direccionesProveedores/crear', 'ProveedoresController@crearDireccionProveedor')->name("crearDireccionProveedor");
    Route::post('crm/direccionesProveedores/{direccionProveedor}', 'ProveedoresController@actualizarDireccionProveedor')->name("actualizarDireccionProveedor");

    Route::get('crm/emailsProveedores/{emailProveedor}', 'ProveedoresController@editarEmailProveedor')->where('emailProveedor', '[0-9]+')->name("editarEmailProveedor");
    Route::delete('crm/emailsProveedores/{emailProveedor}', 'ProveedoresController@borrarEmailProveedor')->name("borrarEmailProveedor");
    Route::get('crm/emailsProveedores/nuevo', 'ProveedoresController@nuevoEmailProveedor')->name("nuevoEmailProveedor");
    Route::post('crm/emailsProveedores/crear', 'ProveedoresController@crearEmailProveedor')->name("crearEmailProveedor");
    Route::post('crm/emailsProveedores/{emailProveedor}', 'ProveedoresController@actualizarEmailProveedor')->name("actualizarEmailProveedor");

    Route::get('crm/telefonosProveedores/{telefonoProveedor}', 'ProveedoresController@editarTelefonoProveedor')->where('telefonoProveedor', '[0-9]+')->name("editarTelefonoProveedor");
    Route::delete('crm/telefonosProveedores/{telefonoProveedor}', 'ProveedoresController@borrarTelefonoProveedor')->name("borrarTelefonoProveedor");
    Route::get('crm/telefonosProveedores/nuevo', 'ProveedoresController@nuevoTelefonoProveedor')->name("nuevoTelefonoProveedor");
    Route::post('crm/telefonosProveedores/crear', 'ProveedoresController@crearTelefonoProveedor')->name("crearTelefonoProveedor");
    Route::post('crm/telefonosProveedores/{telefonoProveedor}', 'ProveedoresController@actualizarTelefonoProveedor')->name("actualizarTelefonoProveedor");

    //Materiales
    Route::get('crm/materiales', 'MaterialesController@materiales')->name("materiales");
    Route::get('crm/materiales/nuevo', 'MaterialesController@nuevoMaterial')->name("nuevoMaterial");
    Route::post('crm/materiales/crear', 'MaterialesController@crearMaterial')->name("crearMaterial");
    Route::get('crm/materiales/{material}', 'MaterialesController@editarMaterial')->where('material', '[0-9]+')->name("editarMaterial");
    Route::post('crm/materiales/{material}', 'MaterialesController@actualizarMaterial')->name("actualizarMaterial");
    Route::delete('crm/materiales/{material}', 'MaterialesController@borrarMaterial')->name("borrarMaterial");

    //Servicios
    Route::get('crm/servicios', 'ServiciosController@servicios')->name("servicios");
    Route::get('crm/servicios/nuevo', 'ServiciosController@nuevoServicio')->name("nuevoServicio");
    Route::post('crm/servicios/crear', 'ServiciosController@crearServicio')->name("crearServicio");
    Route::get('crm/servicios/{servicio}', 'ServiciosController@editarServicio')->where('servicio', '[0-9]+')->name("editarServicio");
    Route::post('crm/servicios/{servicio}', 'ServiciosController@actualizarServicio')->name("actualizarServicio");
    Route::delete('crm/servicios/{servicio}', 'ServiciosController@borrarServicio')->name("borrarServicio");

    //Cotizaciones
    Route::get('crm/cotizaciones', 'CotizacionesController@cotizaciones')->name("cotizaciones");
    Route::get('crm/cotizaciones/nueva', 'CotizacionesController@nuevaCotizacion')->name("nuevaCotizacion");
    Route::get('crm/cotizaciones/{cotizacion}', 'CotizacionesController@editarCotizacion')->where('cotizacion', '[0-9]+')->name("editarCotizacion");
    Route::get('crm/cotizaciones/regresar/{cotizacion}', 'CotizacionesController@regresarEditarCotizacionActual')->name("regresarEditarCotizacionActual");
    Route::get('crm/cotizaciones/cancelar/{cotizacion}', 'CotizacionesController@cancelarCotizacion')->name("cancelarCotizacion");
    Route::post('crm/cotizaciones/finalizarCancelacion/{cotizacion}', 'CotizacionesController@finalizarCancelacionCotizacion')->name("finalizarCancelacionCotizacion");
    Route::post('crm/cotizaciones/actualizarCotizacion/{cotizacion}', 'CotizacionesController@actualizarCotizacion')->name("actualizarCotizacion");
    Route::post('crm/cotizaciones/procesarCotizacion/{cotizacion}', 'CotizacionesController@procesarCotizacion')->name("procesarCotizacion");
    Route::get('crm/cotizaciones/autorizarCotizacion/{cotizacion}', 'CotizacionesController@autorizarCotizacion')->name("autorizarCotizacion");
    Route::get('crm/cotizaciones/ventaCotizacion/{cotizacion}', 'CotizacionesController@ventaCotizacion')->name("ventaCotizacion");
    Route::get('crm/cotizaciones/procesarVentaCotizacion/{cotizacion}', 'CotizacionesController@procesarVentaCotizacion')->name("procesarVentaCotizacion");
    Route::post('crm/cotizaciones/datosVenta', 'CotizacionesController@datosVenta')->name("datosVenta");
    Route::post('crm/cotizaciones/finalizarVentaCotizacion/{cotizacion}', 'CotizacionesController@finalizarVentaCotizacion')->name("finalizarVentaCotizacion");
    
    //Pdf cotizacion
    Route::get('crm/cotizaciones/pdf/{cotizacion}', 'CotizacionesController@pdfCotizacion')->name("pdfCotizacion");
    Route::get('crm/cotizaciones/pdfVersionCotizacion/{cotizacion}/{version}', 'CotizacionesController@pdfVersionCotizacion')->name("pdfVersionCotizacion");
    
        //Materiales Cotizaciones
        Route::get('crm/cotizaciones/agregarMaterialCotizacion', 'CotizacionesController@agregarMaterialCotizacion')->name("agregarMaterialCotizacion");
        Route::post('crm/cotizaciones/material/{material}', 'CotizacionesController@subirMaterialCotizacion')->name("subirMaterialCotizacion");
        Route::get('crm/cotizaciones/materialFueraCatalogo', 'CotizacionesController@materialFueraCatalogo')->name("materialFueraCatalogo");
        Route::post('crm/cotizaciones/crearMaterialFueraCatalogo', 'CotizacionesController@crearMaterialFueraCatalogo')->name("crearMaterialFueraCatalogo");
        Route::get('crm/cotizaciones/material/editar/{valorCotizacion}', 'CotizacionesController@editarMaterial')->where('valorCotizacion', '[0-9]+')->name("editarMaterialCotizacion");
        Route::post('crm/cotizaciones/material/editar/{valorCotizacion}', 'CotizacionesController@actualizarMaterial')->name("actualizarMaterialCotizacion");
        Route::delete('crm/cotizaciones/material/editar/{valorCotizacion}', 'CotizacionesController@borrarMaterial')->name("borrarMaterialCotizacion");

        //Servicios Cotizacion
        Route::get('crm/cotizaciones/agregarServicioCotizacion', 'CotizacionesController@agregarServicioCotizacion')->name("agregarServicioCotizacion");
        Route::post('crm/cotizaciones/servicio/{servicio}', 'CotizacionesController@subirServicioCotizacion')->name("subirServicioCotizacion");
        Route::get('crm/cotizaciones/servicioFueraCatalogo', 'CotizacionesController@servicioFueraCatalogo')->name("servicioFueraCatalogo");
        Route::post('crm/cotizaciones/crearServicioFueraCatalogo', 'CotizacionesController@crearServicioFueraCatalogo')->name("crearServicioFueraCatalogo");
        Route::get('crm/cotizaciones/servicio/editar/{valorCotizacion}', 'CotizacionesController@editarServicio')->where('valorCotizacion', '[0-9]+')->name("editarServicioCotizacion");
        Route::post('crm/cotizaciones/servicio/editar/{valorCotizacion}', 'CotizacionesController@actualizarServicio')->name("actualizarServicioCotizacion");
        Route::delete('crm/cotizaciones/servicio/editar/{valorCotizacion}', 'CotizacionesController@borrarServicio')->name("borrarServicioCotizacion");

    //Ordenes de compra
    Route::get('crm/ordenes', 'OrdenesController@ordenes')->name("ordenes");
    Route::get('crm/ordenesMaterialesCompra/{cotizacion}', 'OrdenesController@ordenesMaterialesCompra')->name("ordenesMaterialesCompra");
    Route::get('crm/ordenesServiciosCompra/{cotizacion}', 'OrdenesController@ordenesServiciosCompra')->name("ordenesServiciosCompra");
    Route::get('crm/generarMaterialesOrdenes/{cotizacion}', 'OrdenesController@generarMaterialesOrdenes')->name("generarMaterialesOrdenes");
    Route::get('crm/generarServiciosOrdenes/{cotizacion}', 'OrdenesController@generarServiciosOrdenes')->name("generarServiciosOrdenes");
    Route::get('crm/pdfOrdenMaterial/{orden}', 'OrdenesController@pdfOrdenMaterial')->name("pdfOrdenMaterial");
    Route::get('crm/pdfOrdenServicio/{orden}', 'OrdenesController@pdfOrdenServicio')->name("pdfOrdenServicio");

        //Materiales
        Route::post('crm/subirMaterialOrdenes/{valorCotizacion}', 'OrdenesController@subirMaterialOrdenes')->name("subirMaterialOrdenes");

        //Servicios
        Route::post('crm/subirServicioOrdenes/{valorCotizacion}', 'OrdenesController@subirServicioOrdenes')->name("subirServicioOrdenes");

    //oportunidades
    Route::get('crm/oportunidades', 'OportunidadesController@oportunidades')->name("oportunidades");
    Route::get('crm/oportunidades/nuevo', 'OportunidadesController@nuevaOportunidad')->name("nuevaOportunidad");
    Route::post('crm/oportunidades/crear', 'OportunidadesController@crearOportunidad')->name("crearOportunidad");
    Route::get('crm/oportunidades/{oportunidad}', 'OportunidadesController@editarOportunidad')->where('oportunidad', '[0-9]+')->name("editarOportunidad");
    Route::post('crm/oportunidades/{oportunidad}', 'OportunidadesController@actualizarOportunidad')->name("actualizarOportunidad");
    Route::delete('crm/oportunidades/{oportunidad}', 'OportunidadesController@borrarOportunidad')->name("borrarOportunidad");

    Route::get('crm/facturacion', 'FacturacionController@facturacion')->name("facturacion");

});


